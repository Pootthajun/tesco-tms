﻿Imports System.IO
Imports System.Net
Imports System.Text
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.Data.OleDb
Imports System.Threading
Imports TESCO_TMS_CheckUpdate.Org.Mentalis.Files

Public Class frmCheckUpdate

    Public dbFile As String = "System.dll"
    Public TempPath As String = GetTempPath() & "TMS"
    Public ConnStr As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & TempPath & "\" & dbFile & ";Persist Security Info=False;"

    Private WithEvents _Downloader As WebFileDownloader
    Dim FileUpdatePath As String = ""
    'Dim ClientPath As String = "C:\LEARNING CENTER\TESCO-TMS.exe"
    Public WebServiceURL As String = GetWebServiceURL(TempPath & "\TempPath.ini")
    Public ExitIncompleted As Boolean = False
    Function GetWebServiceURL(TempINIFile As String) As String
        'Return "http://tescolotuslc.com/learningcenterstaging/"
        'Return "https://tescolotuslc.com/learningcenterpreproduction/"
        Return "https://tescolotuslc.com/learningcenter/"  'Production

    End Function

    Function GetTempPath() As String
        Dim ini As New IniReader(Application.StartupPath & "\TempPath.ini")
        ini.Section = "SETTING"
        If ini.ReadString("TempPath") = "" Then
            ini.Write("ProgramPath", Application.StartupPath & "\")
            ini.Write("TempPath", "D:\")
        End If
        Return ini.ReadString("TempPath")

    End Function

    Private Function GetFileVersionInfo(ByVal filename As String) As Version
        Return Version.Parse(FileVersionInfo.GetVersionInfo(filename).FileVersion)
    End Function

    Private Sub frmCheckUpdate_FormClosed(sender As Object, e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        ExitIncompleted = True
    End Sub

    Private Sub frmCheckUpdate_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        'Me.TopMost = True
        Dim ClientID As String = ""
        Dim ClientVersion As String = getMyVersion()
        Dim UserID As String = ""
        Dim UserName As String = ""
        TempPath = GetTempPath() & "TMS"

        If (Not Directory.Exists(TempPath)) Then
            Try
                Directory.CreateDirectory(TempPath)
            Catch ex As Exception
                MessageBox.Show("Could not fine Drive " & GetTempPath(), "Attention", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Application.Exit()
                Exit Sub
            End Try
        End If

        If IO.File.Exists(TempPath & "\TESCO_TMS_CheckUpdate.exe") = False Then
            IO.File.Copy(Application.StartupPath & "\TESCO_TMS_CheckUpdate.exe", TempPath & "\TESCO_TMS_CheckUpdate.exe", True)
            IO.File.Copy(Application.StartupPath & "\Newtonsoft.Json.dll", TempPath & "\Newtonsoft.Json.dll", True)
            IO.File.Copy(Application.StartupPath & "\System.dll", TempPath & "\System.dll", True)
        End If

        Dim iniStartupPath As New IniReader(Application.StartupPath & "\TempPath.ini")
        iniStartupPath.Section = "SETTING"
        Dim ProgramPath As String = iniStartupPath.ReadString("ProgramPath") & "TESCO-TMS.exe"
        Dim UpdateTempPath As String = iniStartupPath.ReadString("TempPath")
        Dim iniTempPath As New IniReader(TempPath & "\TempPath.ini")
        iniTempPath.Section = "SETTING"
        iniTempPath.Write("TempPath", UpdateTempPath)

        Dim version As System.Version = GetFileVersionInfo(TempPath & "\TESCO_TMS_CheckUpdate.exe")
        Dim FileUpdateVersion As String = version.Major & "." & version.Minor & "." & version.Build
        If ClientVersion <> FileUpdateVersion Then
            IO.File.Copy(Application.StartupPath & "\TESCO_TMS_CheckUpdate.exe", TempPath & "\TESCO_TMS_CheckUpdate.exe", True)
        End If

        If Application.StartupPath <> TempPath Then
            Process.Start(TempPath & "\TESCO_TMS_CheckUpdate.exe")
            Application.Exit()
            Exit Sub
        End If


        pbCheckUpdate.Maximum = 3
        pbCheckUpdate.Value = 1
        Application.DoEvents()

        Dim sql As String = ""
        sql = "select client_id,client_version from Activate"
        Dim dt As DataTable = GetSqlDataTable(sql)
        If dt.Rows.Count > 0 Then
            ClientID = dt.Rows(0)("client_id")
        End If
        dt.Dispose()
        pbCheckUpdate.Value = 2

        Dim UpdateVersion As Boolean = False
        Dim UpdateCompleted As Boolean = False
        Try
            Dim info As String = ""
            info = GetStringDataFromURL(WebServiceURL & "api/client_update/get", "client_id=" & ClientID & "&client_version=" & ClientVersion)
            pbCheckUpdate.Value = 3
            If info.Trim <> "" Then
                Dim json As String = info
                Dim ser As JObject = JObject.Parse(json)
                Dim data As List(Of JToken) = ser.Children().ToList
                Dim output As String = ""

                If DirectCast(data(0), JProperty).Value.ToString.ToLower = "true" Then
                    Dim IsUpdaet As Boolean = False
                    Dim vVersion As String = DirectCast(data(2), JProperty).Value.ToString.ToLower
                    Dim SVersion() As String = vVersion.Split(".")
                    Dim CVersion() As String = ClientVersion.Split(".")
                    If CInt(SVersion(0)) > CInt(CVersion(0)) Then
                        IsUpdaet = True
                    ElseIf CInt(SVersion(1)) > CInt(CVersion(1)) Then
                        IsUpdaet = True
                    ElseIf CInt(SVersion(2)) > CInt(CVersion(2)) Then
                        IsUpdaet = True
                    End If
                    If IsUpdaet = True Then
                        'ถ้ามี Version ใหม่กว่า
                        Dim vURL As String = DirectCast(data(3), JProperty).Value.ToString
                        UpdateVersion = True
                        If vURL.Trim <> "" Then
                            'Download
                            Panel1.Visible = True
                            lblURL.Text = vURL
                            lblVersion.Text = vVersion
                            Application.DoEvents()
                            UpdateCompleted = DownloadFile()
                            'If DownloadFile() = True Then
                            '    UpdateVersion = True
                            'End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception

        End Try

        Me.Top = False

        If UpdateVersion And Not UpdateCompleted Then 'ต้อง update แต่ download ไม่เสร็จ
            'MsgBox("พบซอฟท์แวร์เวอร์ชั่นใหม่" & vbLf & "แต่การดาวน์โหลดไม่สำเร็จ" & vbLf & vbLf & "กรุณาตรวจสอบการเชื่อมต่อเครือข่าย")
            'If MsgBox("การเปิดโปรแกรมเวอร์ชั่นเก่าอาจทำให้โปรแกรมทำงานไม่สมบูรณ์" & vbLf & vbLf & "คุณยังต้องการเปิดโปรแกรมเวอร์ชั่นเก่าหรือไม่?", MsgBoxStyle.YesNo) <> MsgBoxResult.Yes Then End
            Application.Exit()
        ElseIf UpdateVersion Then '------ ต้อง update และ download เสร็จ 
            Dim P As Process = Process.Start(FileUpdatePath)
            P.WaitForExit()
            Process.Start(ProgramPath)
            Application.Exit()
        Else
            Process.Start(ProgramPath)
            Application.Exit()
        End If


    End Sub


    Function GetStringDataFromURL(ByVal URL As String, Optional ByVal Parameter As String = "") As String
        Try
            If CheckInternetConnection(WebServiceURL & "api/welcome") = True Then
                Dim ret As String = ""
                Dim request As WebRequest
                request = WebRequest.Create(URL)
                Dim response As WebResponse
                Dim data As Byte() = Encoding.UTF8.GetBytes(Parameter)

                request.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials
                request.Method = "POST"
                request.ContentType = "application/x-www-form-urlencoded"
                request.ContentLength = data.Length

                Dim stream As Stream = request.GetRequestStream()
                stream.Write(data, 0, data.Length)
                stream.Close()

                response = request.GetResponse()
                Dim sr As New StreamReader(response.GetResponseStream())

                Return sr.ReadToEnd()
            Else
                MessageBox.Show("Unable to connect Back-End server " & vbCrLf & vbCrLf & "Please check network connection !!", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Catch ex As Exception
        End Try
        Return ""
    End Function

    Private Function CheckInternetConnection(URL As String) As Boolean
        Dim ret = False
        Dim req As WebRequest = WebRequest.Create(URL)
        Dim resp As WebResponse
        Try
            req.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials
            req.Timeout = 5000
            resp = req.GetResponse
            resp.Close()
            req = Nothing
            ret = True
        Catch ex As Exception
            ret = False
        End Try
        Return ret
    End Function



#Region "Database Query"
    Public Function GetSqlDataTable(sql As String) As DataTable
        Dim dt As New DataTable
        Try
            Dim da As New OleDbDataAdapter(sql, ConnStr)
            da.Fill(dt)
        Catch ex As Exception
            dt = New DataTable
        End Try

        Return dt
    End Function
    Public Function ExecuteSqlNoneQuery(sql As String) As Boolean
        Dim ret As Boolean = False
        Try
            Dim Command As New OleDbCommand
            Dim Conn As New OleDbConnection(ConnStr)
            Conn.Open()

            With Command
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = sql
                ret = (.ExecuteNonQuery() > -1)
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()

        Catch ex As Exception
            ret = False
        End Try

        Return ret
    End Function
#End Region

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Application.Exit()
    End Sub

    Private Sub btnUpdate_Click(sender As System.Object, e As System.EventArgs) Handles btnUpdate.Click
        DownloadFile()
    End Sub

    

#Region "Download Event"
    Private Sub _Downloader_FileDownloadSizeObtained(ByVal iFileSize As Long) Handles _Downloader.FileDownloadSizeObtained
        pbDownload.Value = 0
        pbDownload.Maximum = Convert.ToInt32(iFileSize)
    End Sub

    'FIRES WHEN DOWNLOAD IS COMPLETE
    Private Sub _Downloader_FileDownloadComplete() Handles _Downloader.FileDownloadComplete
        'เมื่อดาวน์โหลดไฟล์สำเร็จให้เริ่มทำการ Update Program ได้เลย

        pbDownload.Value = pbDownload.Maximum
        Me.DialogResult = Windows.Forms.DialogResult.OK

        If File.Exists(FileUpdatePath) = True Then
            Try
                'Kill Process เดิมก่อน
                Dim pProcess() As Process = Process.GetProcessesByName("TESCO-TMS")
                For Each p As Process In pProcess
                    p.Kill()
                Next
                Threading.Thread.Sleep(5000)

                ''ลบไฟล์เดิมทิ้งไป
                'Dim fInfo As New FileInfo(FileUpdatePath)
                'If File.Exists(Application.StartupPath & "\" & fInfo.Name) = True Then
                '    Try
                '        File.SetAttributes(Application.StartupPath & "\" & fInfo.Name, FileAttributes.Normal)
                '        File.Delete(Application.StartupPath & "\" & fInfo.Name)
                '    Catch ex As Exception

                '    End Try
                'End If

                ''เอาไฟล์ใหม่ที่เพิ่งดาวน์โหลดได้มาวางทับ
                'File.Copy(fInfo.FullName, Application.StartupPath & "\" & fInfo.Name)

                Dim sql As String = "update activate "
                sql += " set client_version='" & lblVersion.Text & "'"
                ExecuteSqlNoneQuery(sql)

                'Start Program

            Catch ex As Exception

            End Try
        End If
    End Sub

    'FIRES WHEN DOWNLOAD FAILES. PASSES IN EXCEPTION INFO
    Private Sub _Downloader_FileDownloadFailed(ByVal ex As System.Exception) Handles _Downloader.FileDownloadFailed
        'MessageBox.Show("An error has occured during download: " & ex.Message)
        Application.Exit()
    End Sub

    'FIRES WHEN MORE OF THE FILE HAS BEEN DOWNLOADED
    Private Sub _Downloader_AmountDownloadedChanged(ByVal iNewProgress As Long) Handles _Downloader.AmountDownloadedChanged
        pbDownload.Value = Convert.ToInt32(iNewProgress)
        lblProgress.Text = WebFileDownloader.FormatFileSize(iNewProgress) & " of " & WebFileDownloader.FormatFileSize(pbDownload.Maximum) & " downloaded"
        Application.DoEvents()
    End Sub

    Private Function DownloadFile() As Boolean
        Dim tmpFile() As String = Split(lblURL.Text, "/")
        If tmpFile.Length > 0 Then
            Dim PathDownloadTo As String = TempPath & "\Update\" & lblVersion.Text
            If Directory.Exists(PathDownloadTo) = False Then
                Directory.CreateDirectory(PathDownloadTo)
            End If

            PathDownloadTo = PathDownloadTo & "\" & tmpFile(tmpFile.Length - 1)
            If System.IO.File.Exists(PathDownloadTo) = True Then
                System.IO.File.Delete(PathDownloadTo)
            End If

            Try
                FileUpdatePath = PathDownloadTo
                _Downloader = New WebFileDownloader
                _Downloader.DownloadFileWithProgress(lblURL.Text, PathDownloadTo)
                If ExitIncompleted Then
                    Return False
                Else
                    Return True
                End If

            Catch ex As Exception
                'Application.Exit()
                Return False
            End Try
        Else
            Return False
        End If
    End Function
#End Region


    Public Function getMyVersion() As String
        Dim version As System.Version = System.Reflection.Assembly.GetExecutingAssembly.GetName().Version
        Return version.Major & "." & version.Minor & "." & version.Build '& "." & version.Revision
    End Function
End Class
