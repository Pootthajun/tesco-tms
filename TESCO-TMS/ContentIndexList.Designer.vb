﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ContentIndexList
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblDocumentTitle = New System.Windows.Forms.Label()
        Me.pbLock = New System.Windows.Forms.PictureBox()
        Me.pbSeq = New System.Windows.Forms.PictureBox()
        Me.lblSeq = New System.Windows.Forms.Label()
        CType(Me.pbLock, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbSeq, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblDocumentTitle
        '
        Me.lblDocumentTitle.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblDocumentTitle.BackColor = System.Drawing.Color.White
        Me.lblDocumentTitle.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblDocumentTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(113, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(118, Byte), Integer))
        Me.lblDocumentTitle.Location = New System.Drawing.Point(76, 35)
        Me.lblDocumentTitle.Name = "lblDocumentTitle"
        Me.lblDocumentTitle.Size = New System.Drawing.Size(208, 38)
        Me.lblDocumentTitle.TabIndex = 21
        Me.lblDocumentTitle.Text = "LIVE TRAINING บทเรียนพิเศษ"
        '
        'pbLock
        '
        Me.pbLock.Image = Global.TESCO_TMS.My.Resources.Resources.locker_locked
        Me.pbLock.Location = New System.Drawing.Point(78, 8)
        Me.pbLock.Name = "pbLock"
        Me.pbLock.Size = New System.Drawing.Size(24, 24)
        Me.pbLock.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbLock.TabIndex = 22
        Me.pbLock.TabStop = False
        '
        'pbSeq
        '
        Me.pbSeq.Image = Global.TESCO_TMS.My.Resources.Resources.index_number_dark_bg
        Me.pbSeq.Location = New System.Drawing.Point(8, 9)
        Me.pbSeq.Name = "pbSeq"
        Me.pbSeq.Size = New System.Drawing.Size(62, 62)
        Me.pbSeq.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbSeq.TabIndex = 20
        Me.pbSeq.TabStop = False
        '
        'lblSeq
        '
        Me.lblSeq.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSeq.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblSeq.Font = New System.Drawing.Font("Tahoma", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblSeq.ForeColor = System.Drawing.Color.White
        Me.lblSeq.Location = New System.Drawing.Point(8, 20)
        Me.lblSeq.Name = "lblSeq"
        Me.lblSeq.Size = New System.Drawing.Size(62, 39)
        Me.lblSeq.TabIndex = 53
        Me.lblSeq.Text = "8"
        Me.lblSeq.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ContentIndexList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.Controls.Add(Me.lblSeq)
        Me.Controls.Add(Me.pbLock)
        Me.Controls.Add(Me.lblDocumentTitle)
        Me.Controls.Add(Me.pbSeq)
        Me.Name = "ContentIndexList"
        Me.Size = New System.Drawing.Size(284, 79)
        CType(Me.pbLock, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbSeq, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pbSeq As System.Windows.Forms.PictureBox
    Friend WithEvents lblDocumentTitle As System.Windows.Forms.Label
    Friend WithEvents pbLock As System.Windows.Forms.PictureBox
    Friend WithEvents lblSeq As System.Windows.Forms.Label

End Class
