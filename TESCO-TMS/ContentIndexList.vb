﻿Public Class ContentIndexList

    Public Event ChangeDocument()


    Dim IsDocumentLock As Boolean = True
    Dim DocumentID As Integer = 0
    'Dim DocumentTitle As String = ""
    'Dim TxtSeq As Integer = 0

    Public Sub Display(LockStatus As Boolean, DocID As Integer, DocumentTitle As String, TxtSeq As String)
        If LockStatus = False Then
            pbLock.Image = My.Resources.locker_unlocked
            pbSeq.Image = My.Resources.index_number_green_bg

            lblSeq.BackColor = Color.FromArgb(3, 148, 119)
            lblDocumentTitle.Cursor = Cursors.Hand
        End If

        lblDocumentTitle.Text = DocumentTitle
        lblSeq.Text = TxtSeq
        DocumentID = DocID
        IsDocumentLock = LockStatus
    End Sub

    'Private Sub ContentIndexList_Load(sender As Object, e As System.EventArgs) Handles Me.Load
    '    'lblDocumentTitle.Font = GetFont(FontName.TLCorperateRegular, 10)
    'End Sub

    Private Sub lblDocumentTitle_Click(sender As Object, e As System.EventArgs) Handles lblDocumentTitle.Click
        If IsDocumentLock = False Then
            frmWait.Show()
            Application.DoEvents()

            RaiseEvent ChangeDocument()
            OpenByDocument(DocumentID)
            frmWait.Close()
        End If
    End Sub
End Class
