﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CourseBox
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CourseBox))
        Me.lblDescription = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Logo = New System.Windows.Forms.PictureBox()
        Me.txtTitle = New System.Windows.Forms.TextBox()
        Me.btnOpen = New System.Windows.Forms.PictureBox()
        Me.Line = New System.Windows.Forms.PictureBox()
        Me.pb1 = New System.Windows.Forms.ProgressBar()
        Me.Panel1.SuspendLayout()
        CType(Me.Logo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnOpen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Line, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblDescription
        '
        Me.lblDescription.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDescription.BackColor = System.Drawing.Color.White
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(167, Byte), Integer), CType(CType(147, Byte), Integer))
        Me.lblDescription.Location = New System.Drawing.Point(137, 73)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(350, 145)
        Me.lblDescription.TabIndex = 19
        Me.lblDescription.Text = "บทนี้ท่านจะได้ทำความเข้าใจเกี่ยวกับเครื่อง" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "แคชเชียร์ เรียนรู้ส่วนต่างๆ เพื่อให้ท" & _
    "่านมี" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "ความรู้ความเข้าใจและสามารถปฎิบัติงานได้จริง"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.Panel1.Controls.Add(Me.Logo)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(128, 280)
        Me.Panel1.TabIndex = 21
        '
        'Logo
        '
        Me.Logo.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.Logo.Image = CType(resources.GetObject("Logo.Image"), System.Drawing.Image)
        Me.Logo.Location = New System.Drawing.Point(14, 12)
        Me.Logo.Name = "Logo"
        Me.Logo.Size = New System.Drawing.Size(100, 100)
        Me.Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Logo.TabIndex = 16
        Me.Logo.TabStop = False
        '
        'txtTitle
        '
        Me.txtTitle.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtTitle.Font = New System.Drawing.Font("Tahoma", 16.0!)
        Me.txtTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.txtTitle.Location = New System.Drawing.Point(141, 3)
        Me.txtTitle.Multiline = True
        Me.txtTitle.Name = "txtTitle"
        Me.txtTitle.Size = New System.Drawing.Size(337, 52)
        Me.txtTitle.TabIndex = 23
        Me.txtTitle.Text = "เครื่องแคชเชียร์เครื่องแคชเชียร์เครื่องแคชเชียร์เครื่องแคชเชียร์" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'btnOpen
        '
        Me.btnOpen.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnOpen.Image = Global.TESCO_TMS.My.Resources.Resources.btnOpen
        Me.btnOpen.Location = New System.Drawing.Point(141, 230)
        Me.btnOpen.Name = "btnOpen"
        Me.btnOpen.Size = New System.Drawing.Size(153, 35)
        Me.btnOpen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnOpen.TabIndex = 22
        Me.btnOpen.TabStop = False
        '
        'Line
        '
        Me.Line.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Line.BackColor = System.Drawing.Color.White
        Me.Line.Image = Global.TESCO_TMS.My.Resources.Resources.Line
        Me.Line.Location = New System.Drawing.Point(137, 61)
        Me.Line.Name = "Line"
        Me.Line.Size = New System.Drawing.Size(350, 1)
        Me.Line.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Line.TabIndex = 20
        Me.Line.TabStop = False
        '
        'pb1
        '
        Me.pb1.Location = New System.Drawing.Point(301, 237)
        Me.pb1.Name = "pb1"
        Me.pb1.Size = New System.Drawing.Size(186, 23)
        Me.pb1.TabIndex = 24
        Me.pb1.Visible = False
        '
        'CourseBox
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.Controls.Add(Me.pb1)
        Me.Controls.Add(Me.txtTitle)
        Me.Controls.Add(Me.btnOpen)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Line)
        Me.Controls.Add(Me.lblDescription)
        Me.Margin = New System.Windows.Forms.Padding(3, 3, 40, 40)
        Me.Name = "CourseBox"
        Me.Size = New System.Drawing.Size(500, 280)
        Me.Panel1.ResumeLayout(False)
        CType(Me.Logo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnOpen, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Line, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Logo As System.Windows.Forms.PictureBox
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents Line As System.Windows.Forms.PictureBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnOpen As System.Windows.Forms.PictureBox
    Friend WithEvents txtTitle As System.Windows.Forms.TextBox
    Friend WithEvents pb1 As System.Windows.Forms.ProgressBar

End Class
