﻿
Public Class CourseBox

    Public CourseID As Int32
    Public CourseName As String
    Sub Display(ByVal Title As String, Description As String, ByVal iconFile As String)
        txtTitle.Text = Title
        lblDescription.Text = Description
        'Dim Image As Image = GetImageFromURL(iconURL)
        If iconFile.Trim <> "" Then
            Dim Image As Image = Image.FromFile(iconFile)
            If Not Image Is Nothing Then
                Logo.Image = Image
            End If
        End If
    End Sub

    Private Sub btnOpen_Click(sender As System.Object, e As System.EventArgs) Handles btnOpen.Click
        btnOpen.Enabled = False
        pb1.Visible = True
        BindDocumentData(CourseID, pb1)
        Dim nform As New frmLessonDialog
        nform.CourseID = CourseID
        nform.lblCourseFilter1.Text = CourseName
        Plexiglass(nform, Me.ParentForm)
        btnOpen.Enabled = True
        pb1.Visible = False
    End Sub

    
End Class
