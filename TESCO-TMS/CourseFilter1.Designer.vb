﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CourseFilter1
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CourseFilter1))
        Me.txtTitle = New System.Windows.Forms.TextBox()
        Me.lblDescription = New System.Windows.Forms.Label()
        Me.btnOpen = New System.Windows.Forms.PictureBox()
        Me.Line = New System.Windows.Forms.PictureBox()
        Me.Logo = New System.Windows.Forms.PictureBox()
        CType(Me.btnOpen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Line, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Logo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtTitle
        '
        Me.txtTitle.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtTitle.Font = New System.Drawing.Font("Tahoma", 16.0!)
        Me.txtTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.txtTitle.Location = New System.Drawing.Point(122, 5)
        Me.txtTitle.Multiline = True
        Me.txtTitle.Name = "txtTitle"
        Me.txtTitle.Size = New System.Drawing.Size(337, 30)
        Me.txtTitle.TabIndex = 29
        Me.txtTitle.Text = "MEAL SOLUTION"
        '
        'lblDescription
        '
        Me.lblDescription.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDescription.BackColor = System.Drawing.Color.White
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(167, Byte), Integer), CType(CType(147, Byte), Integer))
        Me.lblDescription.Location = New System.Drawing.Point(122, 29)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(506, 76)
        Me.lblDescription.TabIndex = 25
        Me.lblDescription.Text = "คำแนะนำหลักสูตร : xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" & _
    "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'btnOpen
        '
        Me.btnOpen.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnOpen.Image = Global.TESCO_TMS.My.Resources.Resources.btnOpen
        Me.btnOpen.Location = New System.Drawing.Point(670, 61)
        Me.btnOpen.Name = "btnOpen"
        Me.btnOpen.Size = New System.Drawing.Size(125, 27)
        Me.btnOpen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnOpen.TabIndex = 28
        Me.btnOpen.TabStop = False
        '
        'Line
        '
        Me.Line.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Line.BackColor = System.Drawing.Color.White
        Me.Line.Image = Global.TESCO_TMS.My.Resources.Resources.Line
        Me.Line.Location = New System.Drawing.Point(15, 116)
        Me.Line.Name = "Line"
        Me.Line.Size = New System.Drawing.Size(781, 1)
        Me.Line.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Line.TabIndex = 26
        Me.Line.TabStop = False
        '
        'Logo
        '
        Me.Logo.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.Logo.Image = CType(resources.GetObject("Logo.Image"), System.Drawing.Image)
        Me.Logo.Location = New System.Drawing.Point(16, 5)
        Me.Logo.Name = "Logo"
        Me.Logo.Size = New System.Drawing.Size(100, 100)
        Me.Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Logo.TabIndex = 30
        Me.Logo.TabStop = False
        '
        'CourseFilter1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.Controls.Add(Me.Logo)
        Me.Controls.Add(Me.txtTitle)
        Me.Controls.Add(Me.btnOpen)
        Me.Controls.Add(Me.Line)
        Me.Controls.Add(Me.lblDescription)
        Me.Name = "CourseFilter1"
        Me.Size = New System.Drawing.Size(798, 122)
        CType(Me.btnOpen, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Line, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Logo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtTitle As System.Windows.Forms.TextBox
    Friend WithEvents btnOpen As System.Windows.Forms.PictureBox
    Friend WithEvents Line As System.Windows.Forms.PictureBox
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents Logo As System.Windows.Forms.PictureBox

End Class
