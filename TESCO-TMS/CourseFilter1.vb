﻿Public Class CourseFilter1
    Public CourseID As Int32

    Sub Display(ByVal Title As String, Description As String, ByVal iconFile As String)
        txtTitle.Text = Title
        lblDescription.Text = Description
        If iconFile.Trim <> "" Then
            Dim Image As Image = Image.FromFile(iconFile)
            If Not Image Is Nothing Then
                Logo.Image = Image
            End If
        End If
    End Sub

End Class
