﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Document
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.imgIcon = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.lblDescription = New System.Windows.Forms.Label()
        Me.btnOpen = New System.Windows.Forms.PictureBox()
        CType(Me.imgIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnOpen, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'imgIcon
        '
        Me.imgIcon.Image = Global.TESCO_TMS.My.Resources.Resources.DocumentIcon1
        Me.imgIcon.Location = New System.Drawing.Point(32, 0)
        Me.imgIcon.Name = "imgIcon"
        Me.imgIcon.Size = New System.Drawing.Size(90, 90)
        Me.imgIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgIcon.TabIndex = 1
        Me.imgIcon.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.TESCO_TMS.My.Resources.Resources.DocumentList
        Me.PictureBox1.Location = New System.Drawing.Point(0, 45)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(155, 145)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'lblDescription
        '
        Me.lblDescription.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblDescription.BackColor = System.Drawing.Color.White
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.ForeColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(66, Byte), Integer))
        Me.lblDescription.Location = New System.Drawing.Point(3, 99)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(149, 52)
        Me.lblDescription.TabIndex = 32
        Me.lblDescription.Text = "Description"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'btnOpen
        '
        Me.btnOpen.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnOpen.Image = Global.TESCO_TMS.My.Resources.Resources.btnOpen
        Me.btnOpen.Location = New System.Drawing.Point(20, 148)
        Me.btnOpen.Name = "btnOpen"
        Me.btnOpen.Size = New System.Drawing.Size(114, 30)
        Me.btnOpen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnOpen.TabIndex = 33
        Me.btnOpen.TabStop = False
        '
        'Document
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.Controls.Add(Me.btnOpen)
        Me.Controls.Add(Me.lblDescription)
        Me.Controls.Add(Me.imgIcon)
        Me.Controls.Add(Me.PictureBox1)
        Me.Name = "Document"
        Me.Size = New System.Drawing.Size(155, 200)
        CType(Me.imgIcon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnOpen, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents imgIcon As System.Windows.Forms.PictureBox
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents btnOpen As System.Windows.Forms.PictureBox

End Class
