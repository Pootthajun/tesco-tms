﻿Imports System.Data

Public Class Document

    Public CourseID As Int32
    Public DocumentID As Int32

    'Private Sub Document_Load(sender As Object, e As System.EventArgs) Handles Me.Load
    '    lblDescription.Font = GetFont(FontName.TLCorperateRegular, 21)
    'End Sub

    Sub Display(Description As String, ByVal iconID As String)
        lblDescription.Text = Description
        Dim Image As Image = Global.TESCO_TMS.My.Resources.Resources.WelcomBoxMessage
        Select Case iconID
            Case 1
                Image = Global.TESCO_TMS.My.Resources.Resources.DocumentIcon1
            Case 2
                Image = Global.TESCO_TMS.My.Resources.Resources.DocumentIcon2
            Case 3
                Image = Global.TESCO_TMS.My.Resources.Resources.DocumentIcon3
            Case 4
                Image = Global.TESCO_TMS.My.Resources.Resources.DocumentIcon4
            Case Else
        End Select
        imgIcon.Image = Image
    End Sub

    Private Sub btnOpen_Click(sender As System.Object, e As System.EventArgs) Handles btnOpen.Click
        OpenByDocument(DocumentID)
    End Sub

End Class
