﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormatBox
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblTitleFormat = New System.Windows.Forms.Label()
        Me.pbFormatIcon = New System.Windows.Forms.PictureBox()
        CType(Me.pbFormatIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblTitleFormat
        '
        Me.lblTitleFormat.AutoSize = True
        Me.lblTitleFormat.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblTitleFormat.Location = New System.Drawing.Point(12, 168)
        Me.lblTitleFormat.Name = "lblTitleFormat"
        Me.lblTitleFormat.Size = New System.Drawing.Size(161, 24)
        Me.lblTitleFormat.TabIndex = 1
        Me.lblTitleFormat.Text = "EXTRA & HYPER"
        Me.lblTitleFormat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pbFormatIcon
        '
        Me.pbFormatIcon.Location = New System.Drawing.Point(4, 4)
        Me.pbFormatIcon.Name = "pbFormatIcon"
        Me.pbFormatIcon.Size = New System.Drawing.Size(175, 154)
        Me.pbFormatIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbFormatIcon.TabIndex = 0
        Me.pbFormatIcon.TabStop = False
        '
        'FormatBox
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.Controls.Add(Me.lblTitleFormat)
        Me.Controls.Add(Me.pbFormatIcon)
        Me.Name = "FormatBox"
        Me.Size = New System.Drawing.Size(182, 203)
        CType(Me.pbFormatIcon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pbFormatIcon As System.Windows.Forms.PictureBox
    Friend WithEvents lblTitleFormat As System.Windows.Forms.Label

End Class
