﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ListUser
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblUserID = New System.Windows.Forms.Label()
        Me.lblName = New System.Windows.Forms.Label()
        Me.lblSername = New System.Windows.Forms.Label()
        Me.Line = New System.Windows.Forms.PictureBox()
        Me.pbRemove = New System.Windows.Forms.PictureBox()
        Me.lblRowIndex = New System.Windows.Forms.Label()
        CType(Me.Line, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbRemove, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblUserID
        '
        Me.lblUserID.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblUserID.Location = New System.Drawing.Point(30, 21)
        Me.lblUserID.Name = "lblUserID"
        Me.lblUserID.Size = New System.Drawing.Size(151, 23)
        Me.lblUserID.TabIndex = 3
        Me.lblUserID.Text = "OOOOOOOOOO"
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblName.Location = New System.Drawing.Point(184, 21)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(153, 25)
        Me.lblName.TabIndex = 4
        Me.lblName.Text = "XXXXXXXXXXXXX"
        '
        'lblSername
        '
        Me.lblSername.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblSername.Location = New System.Drawing.Point(342, 21)
        Me.lblSername.Name = "lblSername"
        Me.lblSername.Size = New System.Drawing.Size(200, 25)
        Me.lblSername.TabIndex = 5
        Me.lblSername.Text = "XXXXXXXXXXXXXXXXXX"
        '
        'Line
        '
        Me.Line.Image = Global.TESCO_TMS.My.Resources.Resources.Line
        Me.Line.Location = New System.Drawing.Point(0, 65)
        Me.Line.Name = "Line"
        Me.Line.Size = New System.Drawing.Size(630, 5)
        Me.Line.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Line.TabIndex = 0
        Me.Line.TabStop = False
        '
        'pbRemove
        '
        Me.pbRemove.BackgroundImage = Global.TESCO_TMS.My.Resources.Resources.btn_remove
        Me.pbRemove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.pbRemove.Location = New System.Drawing.Point(564, 9)
        Me.pbRemove.Name = "pbRemove"
        Me.pbRemove.Size = New System.Drawing.Size(52, 50)
        Me.pbRemove.TabIndex = 6
        Me.pbRemove.TabStop = False
        '
        'lblRowIndex
        '
        Me.lblRowIndex.AutoSize = True
        Me.lblRowIndex.Location = New System.Drawing.Point(31, 46)
        Me.lblRowIndex.Name = "lblRowIndex"
        Me.lblRowIndex.Size = New System.Drawing.Size(0, 13)
        Me.lblRowIndex.TabIndex = 7
        Me.lblRowIndex.Visible = False
        '
        'ListUser
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.lblRowIndex)
        Me.Controls.Add(Me.pbRemove)
        Me.Controls.Add(Me.lblSername)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.lblUserID)
        Me.Controls.Add(Me.Line)
        Me.Name = "ListUser"
        Me.Size = New System.Drawing.Size(630, 70)
        CType(Me.Line, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbRemove, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Line As System.Windows.Forms.PictureBox
    Friend WithEvents lblUserID As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents lblSername As System.Windows.Forms.Label
    Friend WithEvents pbRemove As System.Windows.Forms.PictureBox
    Friend WithEvents lblRowIndex As System.Windows.Forms.Label

End Class
