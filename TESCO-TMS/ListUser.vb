﻿Public Class ListUser

    Public Event RemoveListUser(RowIndex As Integer)


    Sub Display(ByVal UserID As String, Name As String, ByVal Sername As String, RowIndex As Int16)
        lblUserID.Text = UserID
        lblName.Text = Name
        lblSername.Text = Sername
        lblRowIndex.Text = RowIndex
    End Sub

    'Private Sub cbT_Click(sender As System.Object, e As System.EventArgs)
    '    cbT.Visible = False
    '    cbF.Visible = True
    'End Sub
    'Private Sub cbF_Click(sender As System.Object, e As System.EventArgs)
    '    cbT.Visible = True
    '    cbF.Visible = False
    'End Sub

    Private Sub pbRemove_Click(sender As System.Object, e As System.EventArgs) Handles pbRemove.Click
        RaiseEvent RemoveListUser(lblRowIndex.Text)
    End Sub
End Class
