﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class StudyCon
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pbConStudyCancel = New System.Windows.Forms.PictureBox()
        Me.pbConStudyOK = New System.Windows.Forms.PictureBox()
        Me.lblStudenList = New System.Windows.Forms.Label()
        Me.lblClassID = New System.Windows.Forms.Label()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.lblCourseID = New System.Windows.Forms.Label()
        Me.lblCourseName = New System.Windows.Forms.Label()
        CType(Me.pbConStudyCancel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbConStudyOK, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pbConStudyCancel
        '
        Me.pbConStudyCancel.BackgroundImage = Global.TESCO_TMS.My.Resources.Resources.btnStudyConCancel
        Me.pbConStudyCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.pbConStudyCancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.pbConStudyCancel.Location = New System.Drawing.Point(428, 195)
        Me.pbConStudyCancel.Name = "pbConStudyCancel"
        Me.pbConStudyCancel.Size = New System.Drawing.Size(108, 35)
        Me.pbConStudyCancel.TabIndex = 49
        Me.pbConStudyCancel.TabStop = False
        Me.pbConStudyCancel.Visible = False
        '
        'pbConStudyOK
        '
        Me.pbConStudyOK.BackgroundImage = Global.TESCO_TMS.My.Resources.Resources.btnStudyConConOK
        Me.pbConStudyOK.Cursor = System.Windows.Forms.Cursors.Hand
        Me.pbConStudyOK.Location = New System.Drawing.Point(203, 195)
        Me.pbConStudyOK.Name = "pbConStudyOK"
        Me.pbConStudyOK.Size = New System.Drawing.Size(168, 35)
        Me.pbConStudyOK.TabIndex = 48
        Me.pbConStudyOK.TabStop = False
        '
        'lblStudenList
        '
        Me.lblStudenList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblStudenList.BackColor = System.Drawing.Color.White
        Me.lblStudenList.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStudenList.ForeColor = System.Drawing.Color.FromArgb(CType(CType(113, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(118, Byte), Integer))
        Me.lblStudenList.Location = New System.Drawing.Point(47, 102)
        Me.lblStudenList.Name = "lblStudenList"
        Me.lblStudenList.Size = New System.Drawing.Size(489, 84)
        Me.lblStudenList.TabIndex = 47
        Me.lblStudenList.Text = "X"
        '
        'lblClassID
        '
        Me.lblClassID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblClassID.AutoSize = True
        Me.lblClassID.BackColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(119, Byte), Integer))
        Me.lblClassID.Font = New System.Drawing.Font("Tahoma", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblClassID.ForeColor = System.Drawing.Color.White
        Me.lblClassID.Location = New System.Drawing.Point(44, 36)
        Me.lblClassID.Name = "lblClassID"
        Me.lblClassID.Size = New System.Drawing.Size(126, 39)
        Me.lblClassID.TabIndex = 46
        Me.lblClassID.Text = "XD5656"
        '
        'PictureBox4
        '
        Me.PictureBox4.BackgroundImage = Global.TESCO_TMS.My.Resources.Resources.frmStudyConBody
        Me.PictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox4.Location = New System.Drawing.Point(1, 90)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(585, 160)
        Me.PictureBox4.TabIndex = 45
        Me.PictureBox4.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.BackgroundImage = Global.TESCO_TMS.My.Resources.Resources.frmStudyConClassID
        Me.PictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox3.Location = New System.Drawing.Point(1, 0)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(585, 90)
        Me.PictureBox3.TabIndex = 44
        Me.PictureBox3.TabStop = False
        '
        'lblCourseID
        '
        Me.lblCourseID.AutoSize = True
        Me.lblCourseID.Location = New System.Drawing.Point(304, 61)
        Me.lblCourseID.Name = "lblCourseID"
        Me.lblCourseID.Size = New System.Drawing.Size(0, 13)
        Me.lblCourseID.TabIndex = 50
        Me.lblCourseID.Visible = False
        '
        'lblCourseName
        '
        Me.lblCourseName.AutoSize = True
        Me.lblCourseName.Location = New System.Drawing.Point(328, 36)
        Me.lblCourseName.Name = "lblCourseName"
        Me.lblCourseName.Size = New System.Drawing.Size(0, 13)
        Me.lblCourseName.TabIndex = 51
        Me.lblCourseName.Visible = False
        '
        'StudyCon
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.Controls.Add(Me.lblCourseName)
        Me.Controls.Add(Me.lblCourseID)
        Me.Controls.Add(Me.pbConStudyCancel)
        Me.Controls.Add(Me.pbConStudyOK)
        Me.Controls.Add(Me.lblStudenList)
        Me.Controls.Add(Me.lblClassID)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(Me.PictureBox3)
        Me.Margin = New System.Windows.Forms.Padding(3, 3, 40, 40)
        Me.Name = "StudyCon"
        Me.Size = New System.Drawing.Size(590, 254)
        CType(Me.pbConStudyCancel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbConStudyOK, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pbConStudyCancel As System.Windows.Forms.PictureBox
    Friend WithEvents pbConStudyOK As System.Windows.Forms.PictureBox
    Friend WithEvents lblStudenList As System.Windows.Forms.Label
    Friend WithEvents lblClassID As System.Windows.Forms.Label
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents lblCourseID As System.Windows.Forms.Label
    Friend WithEvents lblCourseName As System.Windows.Forms.Label

End Class
