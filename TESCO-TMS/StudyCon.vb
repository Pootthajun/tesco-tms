﻿Imports System.Data

Public Class StudyCon
    Sub Display(ByVal ClassID As String, CourseID As String, CourseName As String, StudenList As String)
        lblClassID.Text = ClassID
        lblCourseID.Text = CourseID
        lblCourseName.Text = CourseName
        lblStudenList.Text = StudenList
    End Sub

    Private Sub pbConStudyOK_Click(sender As System.Object, e As System.EventArgs) Handles pbConStudyOK.Click
        Dim sql As String = "select usf.tb_course_document_file_id, usf.file_no, usf.file_url, usf.file_path,is_study, "
        sql += " cdf.tb_course_document_id as document_id, usf.tb_course_id"
        sql += " from tb_user_study_file usf "
        sql += " inner join tb_course_document_file cdf on usf.tb_course_document_file_id=cdf.id"
        sql += " where usf.class_id=" & lblClassID.Text
        sql += " and usf.user_id=" & myUser.UserID
        sql += " and usf.client_id=" & myUser.ClientID
        sql += " and usf.tb_course_id=" & lblCourseID.Text
        sql += " order by usf.file_no"

        DT_DOCUMENT_ALL_FILE = New DataTable
        DT_DOCUMENT_ALL_FILE.Columns.Add("id")
        DT_DOCUMENT_ALL_FILE.Columns.Add("document_id")
        DT_DOCUMENT_ALL_FILE.Columns.Add("file")    'URL ของ ไฟล์
        DT_DOCUMENT_ALL_FILE.Columns.Add("sort")
        DT_DOCUMENT_ALL_FILE.Columns.Add("file_path")
        DT_DOCUMENT_ALL_FILE.Columns.Add("tb_course_id")

        Dim stuDt As DataTable = GetSqlDataTable(sql)
        If stuDt.Rows.Count > 0 Then
            For Each stuDr As DataRow In stuDt.Rows
                Dim dr As DataRow = DT_DOCUMENT_ALL_FILE.NewRow
                dr("id") = stuDr("tb_course_document_file_id")

                dr("file") = stuDr("file_url")
                dr("file_path") = stuDr("file_path")
                dr("sort") = stuDr("file_no")
                dr("document_id") = stuDr("document_id")
                dr("tb_course_id") = stuDr("tb_course_id")
                DT_DOCUMENT_ALL_FILE.Rows.Add(dr)
            Next

            If DT_DOCUMENT_ALL_FILE.Rows.Count = 0 Then
                MessageBox.Show("File not found", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If

            stuDt.DefaultView.RowFilter = "is_study='N'"
            If stuDt.DefaultView.Count > 0 Then
                ShowFormDocumentAllFile(stuDt.DefaultView(0).Item("file_no"), lblClassID.Text)
            ElseIf stuDt.DefaultView.Count = 0 Then
                If MessageBox.Show("คุณได้เรียนจบบทเรียน" & lblCourseName.Text & "แล้ว คุณต้องการเริ่มเรียนบทเรียนนี้อีกครั้งหรือไม่?", "Attention", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    ShowFormDocumentAllFile(1, lblClassID.Text)
                End If
            End If
            stuDt.DefaultView.RowFilter = ""
        Else
            ShowFormDocumentAllFile(0, lblClassID.Text)
        End If
        stuDt.Dispose()
    End Sub
End Class
