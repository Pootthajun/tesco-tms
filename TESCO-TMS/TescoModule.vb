﻿Imports System
Imports System.Net
Imports System.Text
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports TESCO_TMS.Org.Mentalis.Files

Module TescoModule

    Public dbFile As String = "System.dll"
    'Public dbFile As String = "System.accdb"
    Public TempPath As String = GetTempPath() & "TMS"
    Public ConnStr As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & TempPath & "\" & dbFile & ";Persist Security Info=False;"
    Public CurrentFunctionID As Integer = 0
    Public CurrentDepartmentID As Integer = 0

    Public FolderCourseDocumentFile As String = TempPath & "\CourseDocumentFile"
    Public Token As String = ""
    Public DT_DOCUMENT_ALL_FILE As DataTable

    Public DT_TEST As DataTable
    Public DT_TEST_QUESTION As DataTable

    Public WebServiceURL As String = GetWebServiceURL(TempPath & "\TempPath.ini")
    Private Const Env As String = "Production"

    Public myUser As User
    Public Structure User
        Dim UserID As String
        Dim Name As String
        Dim Token As String
        Dim ClientID As Long
        Dim UserFormat As DataTable
        Dim UserDefaultFormat As String
        Dim UserFunction As DataTable
        Dim UserDepartment As DataTable
        Dim UserMessage As DataTable
        Dim IsLogIn As Boolean
        Dim CourseComplete As Integer
        Dim CourseTotal As Integer
        Dim TestingAttempt As Integer
        Dim TestingComplete As Integer
        Dim TestingTotal As Integer
    End Structure

    Public QT As Question
    Public Structure Question
        Dim TestID As Long
        Dim Title As String
        Dim Descriction As String
        Dim TestTotalScore As Double
    End Structure

    Public SessionPasscode As LogInPasscode
    Public Structure LogInPasscode
        Dim PASSCODE As String
        Dim COUSE_ID As Long
        Dim USER_LIST As String
    End Structure

    Function GetTempPath() As String
        Dim ini As New IniReader(Application.StartupPath & "\TempPath.ini")
        ini.Section = "SETTING"
        If ini.ReadString("TempPath") = "" Then
            ini.Write("ProgramPath", Application.StartupPath & "\")
            ini.Write("TempPath", "D:\")
        End If
        Return ini.ReadString("TempPath")

    End Function

    Function GetWebServiceURL(TempINIFile As String) As String
        If Env = "Staging" Then
            Return "http://tescolotuslc.com/learningcenterstaging/"
        ElseIf Env = "PreProduction" Then
            Return "http://tescolotuslc.com/learningcenterpreproduction/"
        ElseIf Env = "Production" Then
            Return "https://tescolotuslc.com/learningcenter/"
        End If
    End Function


    Private Function CheckInternetConnection(URL As String) As Boolean
        Dim ret = False
        Dim req As WebRequest = WebRequest.Create(URL)
        Dim resp As WebResponse
        Try
            req.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials
            req.Timeout = 5000
            resp = req.GetResponse
            resp.Close()
            req = Nothing
            ret = True
        Catch ex As Exception
            ret = False
        End Try
        Return ret
    End Function


    Function GetStringDataFromURL(ByVal URL As String, Optional ByVal Parameter As String = "") As String
        Try
            Dim ret As String = ""
            If CheckInternetConnection(WebServiceURL & "api/welcome") = True Then
                Dim request As WebRequest
                request = WebRequest.Create(URL)
                Dim response As WebResponse
                Dim data As Byte() = Encoding.UTF8.GetBytes(Parameter)

                request.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials
                request.Method = "POST"
                request.ContentType = "application/x-www-form-urlencoded"
                request.ContentLength = data.Length

                Dim stream As Stream = request.GetRequestStream()
                stream.Write(data, 0, data.Length)
                stream.Close()

                response = request.GetResponse()
                Dim sr As New StreamReader(response.GetResponseStream())

                Return sr.ReadToEnd()
            Else
                MessageBox.Show("Unable to connect Back-End server " & vbCrLf & vbCrLf & "Please check network connection !!", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message & vbCrLf & ex.StackTrace, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        Return ""
    End Function

    Function GetImageFromURL(ByVal URL As String) As Image
        Try
            Dim Client As WebClient = New WebClient
            Client.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim Image As Bitmap = Bitmap.FromStream(New MemoryStream(Client.DownloadData(URL)))
            Return Image
        Catch ex As Exception : End Try
        Dim im As Bitmap
        Return im
    End Function

    Public Sub CallAPIUpdateLog(token As String, ClientID As String, vAction As String, vModule As String, data As String)
        'เมื่อเรียนจบหลักสูตรให้บันทึก Log
        If myUser.IsLogIn = False Then
            vModule = "passcode"
        End If
        Dim info As String = ""
        info = GetStringDataFromURL(WebServiceURL & "api/log", token & "&client_id=" & ClientID & "&action=" & vAction & "&module=" & vModule & "&data=" & data)
    End Sub

    Public Sub CallAPIIncompleteFile(token As String, ClientID As String, vAction As String, vModule As String, data As String)
        'เมื่อกดปุ่มต่อไป หรือย้อนกลับ ให้บันทึก Log
        If myUser.IsLogIn = False Then
            vModule = "passcode"
        End If
        Dim info As String = ""
        info = GetStringDataFromURL(WebServiceURL & "api/log/incomplete/file", token & "&client_id=" & ClientID & "&action=" & vAction & "&module=" & vModule & "&data=" & data)

    End Sub

    Public Function GetClientUpdate() As String
        Dim token As String = ""
        Dim ClientID As String = ""
        Dim ClientVersion As String = ""
        Dim UserID As String = ""
        Dim UserName As String = ""


        Dim sql As String = ""
        sql = "select user_id,username, token,client_id,client_version from TB_CURRENT_LOGON_USER"
        Dim dt As DataTable = GetSqlDataTable(sql)
        If dt.Rows.Count > 0 Then
            ClientID = dt.Rows(0)("client_id")
            ClientVersion = dt.Rows(0)("client_version")
            UserID = dt.Rows(0)("user_id")
            UserName = dt.Rows(0)("username")
            token = dt.Rows(0)("token")
        End If
        dt.Dispose()

        Dim info As String = ""
        info = GetStringDataFromURL(WebServiceURL & "api/client_update/get", token & "&client_id=" & ClientID & "&client_version=" & ClientVersion)

        Dim json As String = info
        Dim ser As JObject = JObject.Parse(json)
        Dim data As List(Of JToken) = ser.Children().ToList
        Dim output As String = ""
        Return info
    End Function

    Public Function CallAPIGetLog(token As String, ClientID As String, CourseID As String) As DataTable
        Dim ret As New DataTable
        ret.Columns.Add("class_id")
        ret.Columns.Add("studen_list")
        ret.Columns.Add("is_document_lock")
        ret.Columns.Add("document_finished_list")
        Try
            Dim info As String = ""
            info = GetStringDataFromURL(WebServiceURL & "api/class/get", token & "&client_id=" & ClientID & "&course_id=" & CourseID)
            Dim json As String = info
            Dim ser As JObject = JObject.Parse(json)
            Dim data As List(Of JToken) = ser.Children().ToList
            Dim output As String = ""
            If data.Count = 3 Then
                If DirectCast(data(0), JProperty).Value.ToString.ToLower = "true" Then
                    If DirectCast(data(2), JProperty).Name.ToLower = "class" Then


                        For Each comment As JObject In DirectCast(data(2), JProperty).Value
                            Dim ClassID As Long = comment("id")


                            'Student List
                            Dim tmpStu As String = ""
                            Dim stuList_txt As String = "{""student_list_id"":" & comment("student_list_id").ToString & "}"
                            Dim stuList_ser As JObject = JObject.Parse(stuList_txt)
                            Dim stuList_data As List(Of JToken) = stuList_ser.Children().ToList
                            For Each stuList_item As JProperty In stuList_data
                                stuList_item.CreateReader()

                                For Each stuList_comment As JObject In stuList_item.Value
                                    Dim firstname As String = stuList_comment("firstname").ToString

                                    If firstname.Trim <> "" Then
                                        If tmpStu.Trim = "" Then
                                            tmpStu = firstname
                                        Else
                                            tmpStu += ", " & firstname
                                        End If
                                    End If
                                Next
                            Next


                            'IS Document Lock
                            Dim IsDocumentLock As String = comment("is_document_lock").ToString
                            Dim tmpfDoc As String = ""
                            Dim fDocList_txt As String = "{""document_finished_list"":" & comment("document_finished_list").ToString & "}"
                            Dim fDocList_ser As JObject = JObject.Parse(fDocList_txt)
                            Dim fDocList_data As List(Of JToken) = fDocList_ser.Children().ToList
                            For Each fDocList_item As JProperty In fDocList_data
                                fDocList_item.CreateReader()

                                For Each DocID As String In Split(fDocList_item.ToString.Replace("""document_finished_list"": [", "").Replace("]", "").Trim, ",")
                                    'Dim DocID As String = fDoc_comment("id").ToString

                                    If DocID.Trim <> "" Then
                                        If tmpfDoc.Trim = "" Then
                                            tmpfDoc = DocID
                                        Else
                                            tmpfDoc += ", " & DocID
                                        End If
                                    End If
                                Next
                            Next


                            Dim dr As DataRow = ret.NewRow
                            dr("class_id") = ClassID
                            dr("studen_list") = tmpStu
                            dr("is_document_lock") = IsDocumentLock
                            dr("document_finished_list") = tmpfDoc
                            ret.Rows.Add(dr)
                        Next
                    End If
                End If
            End If

        Catch ex As Exception
            ret = New DataTable
        End Try
        Return ret
    End Function

    Public Sub CallAPIOnExit(token As String, ClientID As String)
        GetStringDataFromURL(WebServiceURL & "api/exit", token & "&client_id=" & ClientID)
    End Sub

    

    Private Function GetDriveInfoByDriveLetter(ByVal DriveLetter As String) As Double
        Dim ret As Double = 0
        Dim drives As System.IO.DriveInfo() = System.IO.DriveInfo.GetDrives
        For Each dri As System.IO.DriveInfo In drives
            If dri.IsReady = True Then
                If dri.Name.StartsWith(DriveLetter) = True Then
                    ret = dri.TotalFreeSpace / (1000 ^ 2)
                    Exit For
                End If
            End If
        Next

        Return ret
    End Function

    Private Sub BuiltUserFormat(data2 As JToken)
        For Each comment As JProperty In data2
            comment.CreateReader()
            Select Case comment.Name
                Case "format"
                    For Each f As JObject In comment.Values
                        Dim dr As DataRow = myUser.UserFormat.NewRow
                        dr("format_id") = f("id").ToString
                        dr("format_title") = f("title").ToString
                        myUser.UserFormat.Rows.Add(dr)

                        Dim jProp As JObject = JObject.Parse("{""function"":" & f("function").ToString & "}")
                        BuiltUserFunction(jProp, f("id").ToString, f("title").ToString)
                    Next
            End Select
        Next
    End Sub

    Private Sub BuiltUserFunction(data_ser As JObject, FormatID As Integer, FormatTitle As String)
        Dim data As List(Of JToken) = data_ser.Children().ToList
        For Each item As JProperty In data
            For Each comment As JObject In item.Value
                comment.CreateReader()

                Dim dr As DataRow = myUser.UserFunction.NewRow
                dr("format_id") = FormatID
                dr("format_title") = FormatTitle
                dr("function_id") = comment("id").ToString
                dr("function_title") = comment("title").ToString
                dr("function_cover") = comment("cover").ToString
                dr("function_cover_color") = comment("color").ToString
                dr("function_subject") = comment("subject_type").ToString   'main subject / additional subject

                myUser.UserFunction.Rows.Add(dr)

                Dim jProp As JObject = JObject.Parse("{""department"":" & comment("department").ToString & "}")
                BuiltFuserDepartment(jProp, comment("id").ToString, FormatTitle)
            Next
        Next
    End Sub

    Private Sub BuiltFuserDepartment(data_ser As JObject, FunctionID As String, FormatTitle As String)

        Dim data As List(Of JToken) = data_ser.Children().ToList
        For Each item As JProperty In data
            For Each desc As JObject In item.Values
                desc.CreateReader()

                Dim dr As DataRow = myUser.UserDepartment.NewRow
                dr("department_id") = desc("id").ToString
                dr("department_title") = desc("title").ToString
                dr("department_cover") = desc("cover").ToString
                dr("function_id") = FunctionID
                dr("format_title") = FormatTitle

                myUser.UserDepartment.Rows.Add(dr)
                BindDatableTableFromCourse(desc.Last)
            Next
        Next

    End Sub

    Private Sub ClearCoureData()
        Dim sql As String = ""
        sql = "delete from TB_COURSE_DOCUMENT_FILE  "
        ExecuteSqlNoneQuery(sql)

        sql = " delete from TB_COURSE_DOCUMENT "
        ExecuteSqlNoneQuery(sql)

        sql = " delete from TB_COURSE "
        ExecuteSqlNoneQuery(sql)

        Dim CourseIconFolder As String = "CourseIcon"
        Dim CourseCoverFolder As String = "CourseCover"
        DeleteCourseIcon(CourseIconFolder)
        DeleteCourseCover(CourseCoverFolder)
    End Sub


    Private Sub BindDatableTableFromCourse(data As JProperty)
        Dim course_id As Int32 = 0
        Dim doc_id As Int32 = 0

        Dim sql As String = ""
        Dim CourseIconFolder As String = "CourseIcon"
        Dim CourseCoverFolder As String = "CourseCover"

        Dim item As JProperty = data
        'Dim ci As Integer = 1
        For Each comment As JObject In item.Values
            Try
                course_id = comment("id")
                sql = "insert into TB_COURSE (id, department_id,title,description,icon_url,icon_file,cover_url,cover_file,is_document_lock,document_detail,bind_document)"
                sql += " values('" & course_id & "'"
                sql += ", '" & comment("department_id").ToString & "'"
                sql += ", '" & comment("name").ToString & "'"
                sql += ", '" & comment("description").ToString & "'"
                sql += ", '" & comment("icon").ToString & "'"
                sql += ", '" & SaveCourseIcon(CourseIconFolder, comment("icon").ToString, course_id) & "'"
                sql += ", '" & comment("cover").ToString & "'"
                sql += ", '" & SaveCourseCover(CourseCoverFolder, comment("cover").ToString, course_id) & "'"
                'sql += ", '" & ci & "' "
                sql += ", '" & IIf(comment("is_document_lock").ToString.ToLower = "true", "Y", "N") & "'"
                sql += ", '" & ("{""document"":" & comment("document").ToString & "}").Replace("'", "''") & "','N')"

                If ExecuteSqlNoneQuery(sql) = False Then
                    Dim aaa As String = ""
                End If
            Catch ex As Exception

            End Try
        Next
        'End If

        'Clear Inactive File
        'ถ้ามีไฟล์ที่เคยดาวน์โหลดมาแล้วในเครื่อง และเป็นไฟล์ที่ไม่มีอยู่ในบทเรียนแล้ว ก็ให้ลบไฟล์นั้นออกไปโลด

        Dim fDt As DataTable = GetSqlDataTable("select * from TB_COURSE_DOCUMENT_FILE")
        If fDt.Rows.Count > 0 Then
            For Each f As String In Directory.GetFiles(FolderCourseDocumentFile)
                Dim fInfo As New FileInfo(f)

                Dim fFile As String = EnCripText(FolderCourseDocumentFile & "\" & DeCripText(fInfo.Name))
                fDt.DefaultView.RowFilter = "file_path='" & fFile & "'"
                If fDt.DefaultView.Count = 0 Then
                    Try
                        File.SetAttributes(f, FileAttributes.Normal)
                        File.Delete(f)
                    Catch ex As Exception

                    End Try
                End If
                fDt.DefaultView.RowFilter = ""
            Next
        End If
        fDt.Dispose()
    End Sub


    Private Sub BuiltDatableTableUserMessage(data3 As JProperty)
        myUser.UserMessage = New DataTable
        myUser.UserMessage.Columns.Add("name")
        myUser.UserMessage.Columns.Add("description")

        For Each desc As JObject In data3.Value
            Dim name As String = desc("name").ToString
            Dim description As String = desc("description").ToString

            Dim dr As DataRow = myUser.UserMessage.NewRow
            dr("name") = name
            dr("description") = description
            myUser.UserMessage.Rows.Add(dr)
        Next

    End Sub

    Function Login(ByVal Username As String, ByVal Password As String) As Boolean
        Dim ret As Boolean = False
        Try
            Dim Capacity As Double = GetDriveInfoByDriveLetter("C")

            Dim info As String = ""
            info = GetStringDataFromURL(WebServiceURL & "api/login", "username=" & Username & "&password=" & Password & "&client_id=" & myUser.ClientID & "&capacity=" & Format(Capacity, "0.00"))
            If info.Trim <> "" Then
                myUser.IsLogIn = True
                Dim json As String = info
                Dim ser As JObject = JObject.Parse(json)
                Dim data As List(Of JToken) = ser.Children().ToList
                Dim output As String = ""

                Dim FirstNameThai As String = ""
                Dim LastNameThai As String = ""
                Dim FirstNameEng As String = ""
                Dim LastNameEng As String = ""

                For Each item As JProperty In data
                    item.CreateReader()
                    Select Case item.Name
                        Case "token"
                            myUser.Token = item.First
                            Token = "token=" & myUser.Token
                            ret = True
                        Case "firstname"
                            If item.First.ToString <> "" Then FirstNameEng = item.First.ToString
                        Case "lastname"
                            If item.First.ToString <> "" Then LastNameEng = item.First.ToString
                        Case "firstname_thai"
                            If item.First.ToString <> "" Then FirstNameThai = item.First.ToString
                        Case "lastname_thai"
                            If item.First.ToString <> "" Then LastNameThai = item.First.ToString
                        Case "user_id"
                            myUser.UserID = item.First
                        Case "data"
                            myUser.UserFormat = New DataTable
                            myUser.UserFormat.Columns.Add("format_id")
                            myUser.UserFormat.Columns.Add("format_title")

                            myUser.UserFunction = New DataTable
                            myUser.UserFunction.Columns.Add("format_id")
                            myUser.UserFunction.Columns.Add("format_title")
                            myUser.UserFunction.Columns.Add("function_id")
                            myUser.UserFunction.Columns.Add("function_title")
                            myUser.UserFunction.Columns.Add("function_cover")
                            myUser.UserFunction.Columns.Add("function_cover_color")
                            myUser.UserFunction.Columns.Add("function_subject")

                            myUser.UserDepartment = New DataTable
                            myUser.UserDepartment.Columns.Add("department_id")
                            myUser.UserDepartment.Columns.Add("department_title")
                            myUser.UserDepartment.Columns.Add("department_cover")
                            myUser.UserDepartment.Columns.Add("function_id")
                            myUser.UserDepartment.Columns.Add("format_title")
                            ClearCoureData()


                            BuiltUserFormat(item.First)
                        Case "welcome"
                            BuiltDatableTableUserMessage(item)
                    End Select
                Next

                Dim FirstName As String = ""
                Dim LastName As String = ""
                If FirstNameThai.Trim = "" Then
                    FirstName = FirstNameEng
                Else
                    FirstName = FirstNameThai
                End If

                If LastNameThai.Trim = "" Then
                    LastName = LastNameEng
                Else
                    LastName = LastNameThai
                End If

                myUser.Name = FirstName & " " & LastName
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

        Return ret
    End Function

    Function LoninPasscode(ByVal Passcode As String) As Boolean
        Dim ret As Boolean = False
        Try
            'token : String,
            'client_id:  Number()
            'passcode_id: Number()

            Dim info As String = ""
            info = GetStringDataFromURL(WebServiceURL & "api/passcode/get", "client_id=" & myUser.ClientID & "&passcode_id=" & Passcode)
            If info.Trim <> "" Then
                myUser.IsLogIn = False

                Dim json As String = info
                Dim ser As JObject = JObject.Parse(json)
                Dim data As List(Of JToken) = ser.Children().ToList
                If data.Count = 6 Then
                    For Each item As JProperty In data
                        item.CreateReader()
                        Select Case item.Name
                            Case "status"
                                If item.Value = "false" Then
                                    ret = False
                                End If
                            Case "student_id_list"
                                SessionPasscode.USER_LIST = item.Value.ToString
                            Case "course"

                                Dim sql As String = ""
                                sql = "delete from TB_COURSE_DOCUMENT_FILE "
                                ExecuteSqlNoneQuery(sql)

                                sql = " delete from TB_COURSE_DOCUMENT "
                                ExecuteSqlNoneQuery(sql)

                                sql = " delete from TB_COURSE "
                                ExecuteSqlNoneQuery(sql)

                                Dim CourseIconFolder As String = "CourseIcon"
                                Dim CourseCoverFolder As String = "CourseCover"
                                DeleteCourseIcon(CourseIconFolder)
                                DeleteCourseCover(CourseCoverFolder)

                                SessionPasscode.PASSCODE = Passcode
                                Dim course_id As String = ""
                                Dim title As String = ""
                                Dim description As String = ""
                                Dim icon As String = ""
                                Dim cover As String = ""
                                Dim document As String = ""
                                Dim is_document_lock As String = ""

                                Dim course_ser As JObject = JObject.Parse(item.Value.ToString)
                                Dim course_data As List(Of JToken) = course_ser.Children().ToList
                                For Each comment As JProperty In course_data
                                    Select Case comment.Name
                                        Case "id"
                                            SessionPasscode.COUSE_ID = comment.Value
                                            course_id = comment.Value
                                        Case "title"
                                            title = comment.Value
                                        Case "description"
                                            description = comment.Value
                                        Case "icon"
                                            icon = comment.Value
                                        Case "cover"
                                            cover = comment.Value
                                        Case "document"
                                            document = comment.Value.ToString
                                        Case "is_document_lock"
                                            is_document_lock = comment.Value.ToString
                                    End Select
                                Next

                                sql = "insert into TB_COURSE (id,title,description,icon_url,icon_file,cover_url,cover_file,sort,is_document_lock,document_detail,bind_document)"
                                sql += " values('" & course_id & "'"
                                sql += ", '" & title & "'"
                                sql += ", '" & description & "'"
                                sql += ", '" & icon & "'"
                                sql += ", '" & SaveCourseIcon(CourseIconFolder, icon, course_id) & "'"
                                sql += ", '" & cover & "'"
                                sql += ", '" & SaveCourseCover(CourseCoverFolder, cover, course_id) & "'"
                                sql += ", '1'"
                                sql += ", '" & IIf(is_document_lock.ToLower = "true", "Y", "N") & "'"
                                sql += ", '" & ("{""document"":" & document & "}").Replace("'", "''") & "','N')"

                                ret = ExecuteSqlNoneQuery(sql)
                        End Select
                    Next

                    Dim fDt As DataTable = GetSqlDataTable("select * from TB_COURSE_DOCUMENT_FILE")
                    If fDt.Rows.Count > 0 Then
                        For Each f As String In Directory.GetFiles(FolderCourseDocumentFile)
                            Dim fInfo As New FileInfo(f)

                            Dim fFile As String = EnCripText(FolderCourseDocumentFile & "\" & DeCripText(fInfo.Name))
                            fDt.DefaultView.RowFilter = "file_path='" & fFile & "'"
                            If fDt.DefaultView.Count = 0 Then
                                Try
                                    File.SetAttributes(f, FileAttributes.Normal)
                                    File.Delete(f)
                                Catch ex As Exception

                                End Try
                            End If
                            fDt.DefaultView.RowFilter = ""
                        Next
                    End If
                    fDt.Dispose()

                End If
            End If
        Catch ex As Exception
            ret = False
        End Try

        Return ret
    End Function

    Function TescoCheckActivateWithClientToken(client_id As String, client_token As String) As Boolean
        Dim ret As Boolean = False
        Try
            Dim info As String = ""
            info = GetStringDataFromURL(WebServiceURL & "api/validate", "client_token=" & client_token & "&client_id=" & client_id)

            Dim json As String = info
            Dim ser As JObject = JObject.Parse(json)
            Dim data As List(Of JToken) = ser.Children().ToList
            If data.Count = 2 Then
                Dim iTemStatus As JProperty = DirectCast(data.Item(0), JProperty)
                If iTemStatus.Name = "status" Then
                    If iTemStatus.Value = "True" Then
                        ret = True
                    End If
                End If
            End If
        Catch ex As Exception
            ret = False
        End Try
        Return ret
    End Function

    Function TescoActivate(ByVal ActivateCode As String) As Boolean
        Dim ret As Boolean = False
        Dim info As String = ""
        info = GetStringDataFromURL(WebServiceURL & "api/activation", "activation_code=" & ActivateCode)

        Dim json As String = info
        Dim ser As JObject = JObject.Parse(json)
        Dim data As List(Of JToken) = ser.Children().ToList
        Dim output As String = ""

        If data.Count = 5 Then
            Dim iTemStatus As JProperty = DirectCast(data.Item(0), JProperty)
            If iTemStatus.Name = "status" Then
                If iTemStatus.Value = "True" Then
                    Dim sql As String = ""
                    sql = "select * from activate"
                    Dim dt As New DataTable
                    Dim da As New OleDbDataAdapter(sql, ConnStr)
                    da.Fill(dt)
                    If dt.Rows.Count > 0 Then
                        ExecuteSqlNoneQuery("delete from activate")
                    End If

                    myUser.ClientID = DirectCast(data.Item(2), JProperty).Value
                    Dim ClientToken As String = DirectCast(data.Item(4), JProperty).Value

                    Dim v As System.Version = System.Reflection.Assembly.GetExecutingAssembly.GetName().Version
                    Dim ClientVersion As String = v.Major & "." & v.Minor & "." & v.Build ' & "." & v.Revision

                    Dim dr As DataRow = dt.NewRow
                    dr("activate_code") = ActivateCode
                    dr("client_id") = myUser.ClientID
                    dr("client_version") = ClientVersion
                    dr("client_token") = ClientToken
                    dr("update_date") = Now
                    dt.Rows.Add(dr)
                    Dim cmd As New OleDbCommandBuilder(da)
                    da.Update(dt)

                    ret = True
                End If
            End If
        End If
       

        Return ret
    End Function

    Function AddUser(ByVal UserID As String, CourseID As String) As String()
        Dim ret(3) As String
        Dim info As String = ""
        info = GetStringDataFromURL(WebServiceURL & "api/user/get", Token & "&user_company_id=" & UserID & "&course_id=" & CourseID)
        If info.Trim <> "" Then
            Dim json As String = info
            Dim ser As JObject = JObject.Parse(json)
            Dim data As List(Of JToken) = ser.Children().ToList
            Dim output As String = ""

            For Each item As JProperty In data
                item.CreateReader()
                Select Case item.Name
                    Case "user"
                        For Each comment As JProperty In item.Values
                            Select Case comment.Name
                                Case "id"
                                    ret(0) = comment.Value.ToString
                                Case "firstname"
                                    ret(1) = comment.Value.ToString
                                Case "lastname"
                                    ret(2) = comment.Value.ToString
                                Case "company_id"
                                    ret(3) = comment.Value.ToString
                            End Select
                        Next

                End Select
            Next
        End If
        Return ret
    End Function


    Private Function SaveCourseIcon(IconPath As String, IconURL As String, CourseID As String) As String
        Dim ret As String = ""

        Dim FilePath As String = TempPath & "\" & IconPath
        If Directory.Exists(FilePath) = False Then
            Directory.CreateDirectory(FilePath)
        End If

        Dim FileExt As String = ""
        If InStr(IconURL, ".jpeg") Or InStr(IconURL, ".jpg") Then
            FileExt = ".jpg"
        ElseIf InStr(IconURL, ".png") Then
            FileExt = ".png"
        End If

        Dim FileName As String = FilePath & "\" & CourseID & FileExt
        Dim img As Image = GetImageFromURL(IconURL)
        If img IsNot Nothing Then
            If File.Exists(FileName) = True Then
                Try
                    File.SetAttributes(FileName, FileAttributes.Normal)
                    File.Delete(FileName)
                Catch ex As Exception

                End Try
            End If

            img.Save(FileName)

            ret = FileName
        End If
        'img.Dispose()

        Return ret
    End Function

    Private Function SaveCourseCover(CoverPath As String, CoverURL As String, CourseID As String) As String
        Dim ret As String = ""

        Dim FilePath As String = TempPath & "\" & CoverPath
        If Directory.Exists(FilePath) = False Then
            Directory.CreateDirectory(FilePath)
        End If

        Dim FileExt As String = ""
        If InStr(CoverURL, ".jpeg") Or InStr(CoverURL, ".jpg") Then
            FileExt = ".jpg"
        ElseIf InStr(CoverURL, ".png") Then
            FileExt = ".png"
        End If

        Dim FileName As String = FilePath & "\" & CourseID & FileExt
        Dim img As Image = GetImageFromURL(CoverURL)
        If img IsNot Nothing Then
            If File.Exists(FileName) = True Then
                Try
                    File.SetAttributes(FileName, FileAttributes.Normal)
                    File.Delete(FileName)
                Catch ex As Exception

                End Try
            End If

            img.Save(FileName)

            ret = FileName
        End If
        Return ret
    End Function

    Private Function SaveQuestionIcon(IconPath As String, IconURL As String, QuestionID As String) As String
        Dim ret As String = ""

        Dim FilePath As String = TempPath & "\" & IconPath
        If Directory.Exists(FilePath) = False Then
            Directory.CreateDirectory(FilePath)
        End If

        Dim FileExt As String = ""
        If InStr(IconURL, ".jpeg") Or InStr(IconURL, ".jpg") Then
            FileExt = ".jpg"
        ElseIf InStr(IconURL, ".png") Then
            FileExt = ".png"
        End If

        Dim FileName As String = FilePath & "\" & QuestionID & FileExt
        Dim img As Image = GetImageFromURL(IconURL)
        If img IsNot Nothing Then
            If File.Exists(FileName) = True Then
                Try
                    File.SetAttributes(FileName, FileAttributes.Normal)
                    File.Delete(FileName)
                Catch ex As Exception

                End Try
            End If

            img.Save(FileName)

            ret = FileName
        End If

        Return ret
    End Function

    Sub DeleteQuestionIcon(IconPath As String)
        Try
            If Directory.Exists(TempPath & "\" & IconPath) = True Then
                Directory.Delete(TempPath & "\" & IconPath)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub DeleteCourseIcon(IconPath As String)
        Try
            If Directory.Exists(TempPath & "\" & IconPath) = True Then
                Directory.Delete(TempPath & "\" & IconPath, True)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Sub DeleteCourseCover(CoverPath As String)
        Try
            If Directory.Exists(TempPath & "\" & CoverPath) = True Then
                Directory.Delete(TempPath & "\" & CoverPath, True)
            End If
        Catch ex As Exception

        End Try

    End Sub
    Sub DeleteDocumentFileWithDate(DateQty As Integer)
        Try
            For Each f As String In Directory.GetFiles(FolderCourseDocumentFile)
                Dim fInfo As New FileInfo(f)
                If fInfo.CreationTime.AddDays(DateQty) < DateTime.Now Then
                    Try
                        File.SetAttributes(f, FileAttributes.Normal)
                        File.Delete(f)
                    Catch ex As Exception

                    End Try
                End If
            Next
        Catch ex As Exception

        End Try
    End Sub

    Sub BindDocumentData(CourseID As Integer, pb As ProgressBar)
        Try
            Dim document_txt As String = ""
            Dim sql As String = " select document_detail from TB_COURSE where id=" & CourseID & " and bind_document='N'"
            Dim dt As DataTable = GetSqlDataTable(sql)
            If dt.Rows.Count > 0 Then
                sql = "delete from TB_COURSE_DOCUMENT_FILE where tb_course_document_id in (select id from TB_COURSE_DOCUMENT where tb_course_id=" & CourseID & ") "
                ExecuteSqlNoneQuery(sql)

                sql = " delete from TB_COURSE_DOCUMENT  where tb_course_id=" & CourseID
                ExecuteSqlNoneQuery(sql)

                If Convert.IsDBNull(dt.Rows(0)("document_detail")) = False Then
                    document_txt = dt.Rows(0)("document_detail")
                End If
            End If
            dt.Dispose()

            If document_txt.Trim <> "" Then
                pb.Maximum = Split(document_txt, "title").Length
                pb.Value = 0
                Application.DoEvents()
                'Dim document_txt As String = "{""document"":" & comment("document").ToString & "}"
                Dim document_ser As JObject = JObject.Parse(document_txt)
                Dim document_data As List(Of JToken) = document_ser.Children().ToList
                For Each document_item As JProperty In document_data

                    Dim cdi As Integer = 1
                    For Each document_comment As JObject In document_item.Values
                        document_item.CreateReader()

                        sql = "insert into TB_COURSE_DOCUMENT (id,tb_course_id,title,icon_id,version,type,sort,order_by)"
                        sql += " values('" & document_comment("id").ToString & "'"
                        sql += ", '" & CourseID & "'"
                        sql += ", '" & document_comment("title").ToString & "'"
                        sql += ", '" & document_comment("icon_id").ToString & "'"
                        sql += ", '" & document_comment("version").ToString & "'"
                        sql += ", '" & document_comment("type").ToString & "'"
                        sql += ", " & cdi
                        sql += ", '" & document_comment("order").ToString & "')"

                        If ExecuteSqlNoneQuery(sql) = True Then
                            doc_id = document_comment("id")
                            Dim file_txt As String = "{""file"":" & document_comment("file").ToString & "}"
                            Dim file_ser As JObject = JObject.Parse(file_txt)
                            Dim file_data As List(Of JToken) = file_ser.Children().ToList
                            For Each file_item As JProperty In file_data

                                Dim cdfi As Integer = 1
                                For Each file_comment As JObject In file_item.Values
                                    file_item.CreateReader()

                                    Dim FileName As String = file_comment("id").ToString & GetURLFileExtension(file_comment("file").ToString)
                                    Dim DocFileID As String = EnCripText(FileName)
                                    Dim DocFileName As String = "null"
                                    'Dim DocFile() As String = Directory.GetFiles(FolderCourseDocumentFile, DocFileID & ".*")
                                    Dim DocFile() As String = Directory.GetFiles(FolderCourseDocumentFile, DocFileID)
                                    If DocFile.Length > 0 Then
                                        DocFileName = "'" & EnCripText(FolderCourseDocumentFile & "\" & FileName) & "'"
                                    End If

                                    sql = "insert into TB_COURSE_DOCUMENT_FILE (id,tb_course_document_id,title,file_url, file_path,sort,order_by)"
                                    sql += " values('" & file_comment("id").ToString & "'"
                                    sql += ", '" & doc_id & "'"
                                    sql += ", '" & file_comment("title").ToString & "'"
                                    sql += ", '" & EnCripText(file_comment("file").ToString) & "'"
                                    sql += ", " & DocFileName
                                    sql += ", " & cdfi
                                    sql += ", '" & file_comment("order").ToString & "')"

                                    ExecuteSqlNoneQuery(sql)

                                    cdfi += 1
                                    pb.Value += 1
                                Next
                            Next
                        End If
                        cdi += 1
                        pb.Value += 1
                    Next
                Next

                sql = " update TB_COURSE set bind_document='Y' where id=" & CourseID
                ExecuteSqlNoneQuery(sql)
                pb.Value += 1
            End If
        Catch ex As Exception

        End Try
    End Sub


    Private Function GetURLFileExtension(URLFile As String) As String
        Dim ret As String = ""
        Dim Tmp() As String = Split(URLFile, ".")
        If Tmp.Length > 0 Then
            ret = "." & Tmp(Tmp.Length - 1)
        End If

        Return ret
    End Function

    'Sub ShowFormDocumentFile(ByVal CourseID As Int32, ByVal DocumentFileID As Int32)
    '    Dim TotalFile As Int32 = 0
    '    Dim sql As String = " select cdf.id, cdf.tb_course_document_id, cdf.title, cdf.file_url, cdf.sort, "
    '    sql += " cd.tb_course_id, cdf.file_path "
    '    sql += " from tb_course_document_file cdf"
    '    sql += " inner join tb_course_document cd on cd.id=cdf.tb_course_document_id"
    '    sql += " where cdf.id=" & DocumentFileID
    '    sql += " order by cdf.sort"

    '    Dim Temp As DataTable = GetSqlDataTable(sql)
    '    TotalFile = Temp.Rows.Count
    '    If Temp.Rows.Count > 0 AndAlso Temp.Rows(0).Item("file_url").ToString <> "" Then
    '        Dim FileUrl As String = DeCripText(Temp.Rows(0).Item("file_url"))
    '        Dim FilePath = ""
    '        If IsDBNull(Temp.Rows(0).Item("file_path")) = False Then
    '            FilePath = DeCripText(Temp.Rows(0).Item("file_path"))
    '        End If

    '        If InStr(FileUrl, ".png") > 0 Or InStr(FileUrl, ".jpg") > 0 Then
    '            Dim F As New frmDisplayImage
    '            F.CourseID = Temp.Rows(0).Item("tb_course_id")
    '            F.DocumentFileID = Temp.Rows(0).Item("id")
    '            F.CurrentDocID = Temp.Rows(0).Item("document_id")
    '            F.FileNo = Temp.Rows(0).Item("sort")
    '            F.TotalFile = TotalFile
    '            F.urlFile = FileUrl
    '            'F.Show(frmLessonDialog)
    '            F.Show(frmMain)
    '        ElseIf InStr(FileUrl, ".mp4") Then
    '            Dim F As New frmDisplayVDO
    '            F.CourseID = Temp.Rows(0).Item("tb_course_id")
    '            F.DocumentFileID = Temp.Rows(0).Item("id")
    '            F.CurrentDocID = Temp.Rows(0).Item("document_id")
    '            F.FileNo = Temp.Rows(0).Item("sort")
    '            F.TotalFile = TotalFile
    '            F.urlFile = FileUrl
    '            If FilePath <> "" Then
    '                F.PathFile = FilePath
    '            Else
    '                F.PathFile = FolderCourseDocumentFile & "\" & Temp.Rows(0).Item("id") & ".mp4"
    '            End If
    '            'F.Show(frmLessonDialog)
    '            F.Show(frmMain)
    '        ElseIf InStr(FileUrl, ".mpg") Then
    '            Dim F As New frmDisplayVDO
    '            F.CourseID = Temp.Rows(0).Item("tb_course_id")
    '            F.DocumentFileID = Temp.Rows(0).Item("id")
    '            F.CurrentDocID = Temp.Rows(0).Item("document_id")
    '            F.FileNo = Temp.Rows(0).Item("sort")
    '            F.TotalFile = TotalFile
    '            F.urlFile = FileUrl
    '            If FilePath <> "" Then
    '                F.PathFile = FilePath
    '            Else
    '                F.PathFile = FolderCourseDocumentFile & "\" & Temp.Rows(0).Item("id") & ".mpg"
    '            End If
    '            'F.Show(frmLessonDialog)
    '            F.Show(frmMain)
    '        ElseIf InStr(FileUrl, ".pdf") Then
    '            Dim F As New frmDisplayPDF
    '            F.CourseID = Temp.Rows(0).Item("tb_course_id")
    '            F.DocumentFileID = Temp.Rows(0).Item("id")
    '            F.CurrentDocID = Temp.Rows(0).Item("document_id")
    '            F.FileNo = Temp.Rows(0).Item("sort")
    '            F.TotalFile = TotalFile
    '            F.urlFile = FileUrl
    '            If FilePath <> "" Then
    '                F.PathFile = FilePath
    '            Else
    '                F.PathFile = FolderCourseDocumentFile & "\" & Temp.Rows(0).Item("id") & ".pdf"
    '            End If
    '            'F.Show(frmLessonDialog)
    '            F.Show(frmMain)
    '        ElseIf InStr(FileUrl, ".swf") Then
    '            Dim f As New frmDisplayURL
    '            f.CourseID = Temp.Rows(0).Item("tb_course_id")
    '            f.DocumentFileID = Temp.Rows(0).Item("id")
    '            f.CurrentDocID = Temp.Rows(0).Item("document_id")
    '            f.FileNo = Temp.Rows(0).Item("sort")
    '            f.TotalFile = TotalFile
    '            f.urlFile = FileUrl
    '            f.Show(frmMain)


    '        ElseIf InStr(FileUrl, ".ppt") Then
    '            Dim f As New frmDisplayPowerPoint
    '            f.CourseID = Temp.Rows(0).Item("tb_course_id")
    '            f.DocumentFileID = Temp.Rows(0).Item("id")
    '            f.CurrentDocID = Temp.Rows(0).Item("document_id")
    '            f.FileNo = Temp.Rows(0).Item("sort")
    '            f.TotalFile = TotalFile
    '            f.urlFile = FileUrl
    '            If FilePath <> "" Then
    '                f.PathFile = FilePath
    '            Else
    '                f.PathFile = FolderCourseDocumentFile & "\" & Temp.Rows(0).Item("id") & ".ppt"
    '            End If
    '            'f.Show(frmLessonDialog)
    '            f.Show(frmMain)
    '        ElseIf InStr(FileUrl, ".pptx") Then
    '            Dim f As New frmDisplayPowerPoint
    '            f.CourseID = Temp.Rows(0).Item("tb_course_id")
    '            f.DocumentFileID = Temp.Rows(0).Item("id")
    '            f.CurrentDocID = Temp.Rows(0).Item("document_id")
    '            f.FileNo = Temp.Rows(0).Item("sort")
    '            f.TotalFile = TotalFile
    '            f.urlFile = FileUrl
    '            If FilePath <> "" Then
    '                f.PathFile = FileUrl
    '            Else
    '                f.PathFile = FolderCourseDocumentFile & "\" & Temp.Rows(0).Item("id") & ".pptx"
    '            End If
    '            'f.Show(frmLessonDialog)
    '            f.Show(frmMain)
    '        ElseIf InStr(FileUrl, ".html") Or InStr(FileUrl, ".htm") Then
    '            Dim f As New frmDisplayURL
    '            f.CourseID = Temp.Rows(0).Item("tb_course_id")
    '            f.DocumentFileID = Temp.Rows(0).Item("id")
    '            f.CurrentDocID = Temp.Rows(0).Item("document_id")
    '            f.FileNo = Temp.Rows(0).Item("sort")
    '            f.TotalFile = TotalFile
    '            f.urlFile = FileUrl
    '            'f.Show(frmLessonDialog)
    '            f.Show(frmMain)
    '        End If
    '    Else
    '        MessageBox.Show("File not found", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '    End If
    '    'DT_COURSE_DOCUMENT_FILE.DefaultView.RowFilter = ""
    '    Temp.Dispose()
    'End Sub

    Sub ShowFormDocument(FileNo As Integer)
        Dim TotalFile As Int32 = 0

        TotalFile = DT_DOCUMENT_ALL_FILE.Rows.Count

        Dim Temp As New DataTable
        DT_DOCUMENT_ALL_FILE.DefaultView.RowFilter = "sort=" & FileNo
        Temp = DT_DOCUMENT_ALL_FILE.DefaultView.ToTable

        If Temp.Rows.Count > 0 AndAlso Temp.Rows(0).Item("file").ToString <> "" Then
            Dim FileUrl As String = DeCripText(Temp.Rows(0).Item("file"))
            Dim FilePath = ""
            If IsDBNull(Temp.Rows(0).Item("file_path")) = False Then
                FilePath = DeCripText(Temp.Rows(0).Item("file_path"))
            End If

            If InStr(FileUrl, ".png") > 0 Or InStr(FileUrl, ".jpg") > 0 Then
                Dim F As New frmDisplayImage
                F.CourseID = Temp.Rows(0).Item("tb_course_id")
                F.DocumentFileID = Temp.Rows(0).Item("id")
                F.CurrentDocID = Temp.Rows(0).Item("document_id")
                F.FileNo = FileNo
                F.TotalFile = TotalFile
                F.urlFile = FileUrl
                If FilePath <> "" Then F.PathFile = FilePath
                'F.Show(frmLessonDialog)
                F.Show(frmMain)
            ElseIf InStr(FileUrl, ".mp4") > 0 Then
                Dim F As New frmDisplayVDO
                F.CourseID = Temp.Rows(0).Item("tb_course_id")
                F.DocumentFileID = Temp.Rows(0).Item("id")
                F.CurrentDocID = Temp.Rows(0).Item("document_id")
                F.FileNo = FileNo
                F.TotalFile = TotalFile
                F.urlFile = FileUrl
                If FilePath <> "" Then
                    F.PathFile = FilePath
                Else
                    F.PathFile = FolderCourseDocumentFile & "\" & Temp.Rows(0).Item("id") & ".mp4"
                End If
                'F.Show(frmLessonDialog)
                F.Show(frmMain)
            ElseIf InStr(FileUrl, ".mpg") Then
                Dim F As New frmDisplayVDO
                F.CourseID = Temp.Rows(0).Item("tb_course_id")
                F.DocumentFileID = Temp.Rows(0).Item("id")
                F.CurrentDocID = Temp.Rows(0).Item("document_id")
                F.FileNo = FileNo
                F.TotalFile = TotalFile
                F.urlFile = FileUrl
                If FilePath <> "" Then
                    F.PathFile = FilePath
                Else
                    F.PathFile = FolderCourseDocumentFile & "\" & Temp.Rows(0).Item("id") & ".mpg"
                End If
                'F.Show(frmLessonDialog)
                F.Show(frmMain)
            ElseIf InStr(FileUrl, ".pdf") Then
                Dim F As New frmDisplayPDF
                F.CourseID = Temp.Rows(0).Item("tb_course_id")
                F.DocumentFileID = Temp.Rows(0).Item("id")
                F.CurrentDocID = Temp.Rows(0).Item("document_id")
                F.FileNo = FileNo
                F.TotalFile = TotalFile
                F.urlFile = FileUrl
                If FilePath <> "" Then
                    F.PathFile = FilePath
                Else
                    F.PathFile = FolderCourseDocumentFile & "\" & Temp.Rows(0).Item("id") & ".pdf"
                End If
                'F.Show(frmLessonDialog)
                F.Show(frmMain)
            ElseIf InStr(FileUrl, ".swf") Then
                Dim f As New frmDisplayURL
                f.CourseID = Temp.Rows(0).Item("tb_course_id")
                f.DocumentFileID = Temp.Rows(0).Item("id")
                f.CurrentDocID = Temp.Rows(0).Item("document_id")
                f.FileNo = FileNo
                f.TotalFile = TotalFile
                f.urlFile = FileUrl
                f.Show(frmMain)
            ElseIf InStr(FileUrl, ".ppt") Then
                Dim f As New frmDisplayPowerPoint
                f.CourseID = Temp.Rows(0).Item("tb_course_id")
                f.DocumentFileID = Temp.Rows(0).Item("id")
                f.CurrentDocID = Temp.Rows(0).Item("document_id")
                f.FileNo = FileNo
                f.TotalFile = TotalFile
                f.urlFile = FileUrl
                If FilePath <> "" Then
                    f.PathFile = FilePath
                Else
                    f.PathFile = FolderCourseDocumentFile & "\" & Temp.Rows(0).Item("id") & ".ppt"
                End If
                'f.Show(frmLessonDialog)
                f.Show(frmMain)
            ElseIf InStr(FileUrl, ".pptx") Then
                Dim f As New frmDisplayPowerPoint
                f.CourseID = Temp.Rows(0).Item("tb_course_id")
                f.DocumentFileID = Temp.Rows(0).Item("id")
                f.CurrentDocID = Temp.Rows(0).Item("document_id")
                f.FileNo = FileNo
                f.TotalFile = TotalFile
                f.urlFile = FileUrl
                If FilePath <> "" Then
                    f.PathFile = FileUrl
                Else
                    f.PathFile = FolderCourseDocumentFile & "\" & Temp.Rows(0).Item("id") & ".pptx"
                End If
                'f.Show(frmLessonDialog)
                f.Show(frmMain)
            ElseIf InStr(FileUrl, ".html") Or InStr(FileUrl, ".htm") Then
                Dim f As New frmDisplayURL
                f.CourseID = Temp.Rows(0).Item("tb_course_id")
                f.DocumentFileID = Temp.Rows(0).Item("id")
                f.CurrentDocID = Temp.Rows(0).Item("document_id")
                f.FileNo = FileNo
                f.TotalFile = TotalFile
                f.urlFile = FileUrl
                'f.Show(frmLessonDialog)
                f.Show(frmMain)
            End If
        Else
            MessageBox.Show("File not found", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
        'DT_COURSE_DOCUMENT_FILE.DefaultView.RowFilter = ""
        Temp.Dispose()
    End Sub

    Private Function UpdateUserStudy(UserID As String, ClassID As String, ClientID As String, FileNo As String) As Boolean
        Dim ret As Boolean = False
        Try
            Dim sql As String = "update TB_USER_STUDY_FILE "
            sql += " set is_study='Y'"
            sql += ", study_date = now()"
            sql += " where user_id=" & UserID & " and class_id=" & ClassID & " and client_id=" & ClientID
            sql += " and file_no = " & FileNo

            ret = ExecuteSqlNoneQuery(sql)
        Catch ex As Exception
            ret = False
        End Try

        Return ret
    End Function

    Function UpdateCurrentStudyClass(ClassID As Long, DocFinishList As String) As Boolean
        Dim ret As Boolean = False
        Dim info As String = ""
        info = GetStringDataFromURL(WebServiceURL & "api/class/update", Token & "&client_id=" & myUser.ClientID & "&class_id=" & ClassID & "&document_finished_list=" & DocFinishList)
        If info.Trim <> "" Then
            Dim json As String = info
            Dim ser As JObject = JObject.Parse(json)
            Dim data As List(Of JToken) = ser.Children().ToList

            If data.Count = 2 Then
                If DirectCast(data(0), JProperty).Value.ToString.ToLower = "true" Then
                    ret = True
                End If
            End If
        End If
        
        Return ret
    End Function

    Sub ShowFormDocumentAllFile(ByVal FileNo As Int32, ClassID As Long)
        Dim UserID As String = "1"
        If myUser.IsLogIn = True Then
            UserID = myUser.UserID
        End If
        If UpdateUserStudy(UserID, ClassID, myUser.ClientID, FileNo) = False Then
            MessageBox.Show("Cannot access Database please try again later", "Connection Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If


        Dim TotalFile As Int32 = 0
        TotalFile = DT_DOCUMENT_ALL_FILE.Rows.Count
        If TotalFile > 0 Then
            Dim tmp As String = ""
            For i As Integer = 0 To FileNo - 1
                Dim tmpDr As DataRow = DT_DOCUMENT_ALL_FILE.Rows(i)
                If tmp = "" Then
                    tmp = tmpDr("id")
                Else
                    tmp += "," & tmpDr("id")
                End If
            Next

            If myUser.IsLogIn = True Then
                If UpdateCurrentStudyClass(ClassID, tmp) = False Then
                    MessageBox.Show("Cannot update your current class please try again later", "Connection Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If
            End If

        End If


        Dim Temp As New DataTable
        DT_DOCUMENT_ALL_FILE.DefaultView.RowFilter = "sort=" & FileNo
        Temp = DT_DOCUMENT_ALL_FILE.DefaultView.ToTable

        If Temp.Rows.Count > 0 AndAlso Temp.Rows(0).Item("file").ToString <> "" Then
            Dim FileUrl As String = DeCripText(Temp.Rows(0).Item("file"))
            Dim FilePath As String = ""
            If IsDBNull(Temp.Rows(0).Item("file_path")) = False Then
                FilePath = DeCripText(Temp.Rows(0).Item("file_path"))
            End If

            If InStr(FileUrl, ".png") > 0 Or InStr(FileUrl, ".jpg") > 0 Then
                Dim F As New frmDisplayImage
                F.DocumentFileID = Temp.Rows(0).Item("id")
                F.CourseID = Temp.Rows(0).Item("tb_course_id")
                F.CurrentDocID = Temp.Rows(0).Item("document_id")
                F.FileNo = Temp.Rows(0).Item("sort")
                F.ClassID = ClassID
                F.TotalFile = TotalFile
                F.urlFile = FileUrl
                If FilePath <> "" Then F.PathFile = FilePath
                F.ViewAllFile = True
                'F.Show(frmLessonDialog)
                F.Show(frmMain)
            ElseIf InStr(FileUrl, ".mp4") > 0 Then
                Dim F As New frmDisplayVDO
                F.DocumentFileID = Temp.Rows(0).Item("id")
                F.CourseID = Temp.Rows(0).Item("tb_course_id")
                F.CurrentDocID = Temp.Rows(0).Item("document_id")
                F.FileNo = Temp.Rows(0).Item("sort")
                F.ClassID = ClassID
                F.TotalFile = TotalFile
                F.urlFile = FileUrl
                F.ViewAllFile = True
                If FilePath <> "" Then
                    F.PathFile = FilePath
                Else
                    F.PathFile = FolderCourseDocumentFile & "\" & Temp.Rows(0).Item("id") & ".mp4"
                End If
                'F.Show(frmLessonDialog)
                F.Show(frmMain)
            ElseIf InStr(FileUrl, ".mpg") > 0 Then
                Dim F As New frmDisplayVDO
                F.DocumentFileID = Temp.Rows(0).Item("id")
                F.CourseID = Temp.Rows(0).Item("tb_course_id")
                F.CurrentDocID = Temp.Rows(0).Item("document_id")
                F.FileNo = Temp.Rows(0).Item("sort")
                F.ClassID = ClassID
                F.TotalFile = TotalFile
                F.urlFile = FileUrl
                F.ViewAllFile = True
                If FilePath <> "" Then
                    F.PathFile = FilePath
                Else
                    F.PathFile = FolderCourseDocumentFile & "\" & Temp.Rows(0).Item("id") & ".mpg"
                End If
                'F.Show(frmLessonDialog)
                F.Show(frmMain)
            ElseIf InStr(FileUrl, ".pdf") Then
                Dim F As New frmDisplayPDF
                F.DocumentFileID = Temp.Rows(0).Item("id")
                F.CourseID = Temp.Rows(0).Item("tb_course_id")
                F.CurrentDocID = Temp.Rows(0).Item("document_id")
                F.FileNo = Temp.Rows(0).Item("sort")
                F.ClassID = ClassID
                F.TotalFile = TotalFile
                F.urlFile = FileUrl

                If FilePath <> "" Then
                    F.PathFile = FilePath
                Else
                    F.PathFile = FolderCourseDocumentFile & "\" & Temp.Rows(0).Item("id") & ".pdf"
                End If
                F.ViewAllFile = True
                'F.Show(frmLessonDialog)
                F.Show(frmMain)
                'Plexiglass(F, frmMain)
            ElseIf InStr(FileUrl, ".swf") Then
                'ถ้าเป็นไฟล์ Flash แล้วเล่นผ่าน Form แล้วมีปัญหา ก็เล่นผ่าน Browser มันซะเลย 555
                Dim f As New frmDisplayURL
                f.DocumentFileID = Temp.Rows(0).Item("id")
                f.CourseID = Temp.Rows(0).Item("tb_course_id")
                f.CurrentDocID = Temp.Rows(0).Item("document_id")
                f.FileNo = Temp.Rows(0).Item("sort")
                f.ClassID = ClassID
                f.TotalFile = TotalFile
                f.urlFile = FileUrl
                f.ViewAllFile = True
                f.Show(frmMain)

            ElseIf InStr(FileUrl, ".ppt") Then
                Dim f As New frmDisplayPowerPoint
                f.DocumentFileID = Temp.Rows(0).Item("id")
                f.CourseID = Temp.Rows(0).Item("tb_course_id")
                f.CurrentDocID = Temp.Rows(0).Item("document_id")
                f.FileNo = Temp.Rows(0).Item("sort")
                f.ClassID = ClassID
                f.TotalFile = TotalFile
                f.urlFile = FileUrl
                If FilePath <> "" Then
                    f.PathFile = FilePath
                Else
                    f.PathFile = FolderCourseDocumentFile & "\" & Temp.Rows(0).Item("id") & ".ppt"
                End If
                f.ViewAllFile = True
                'f.Show(frmLessonDialog)
                f.Show(frmMain)
            ElseIf InStr(FileUrl, ".pptx") Then
                Dim f As New frmDisplayPowerPoint
                f.DocumentFileID = Temp.Rows(0).Item("id")
                f.CurrentDocID = Temp.Rows(0).Item("document_id")
                f.FileNo = Temp.Rows(0).Item("sort")
                f.ClassID = ClassID
                f.TotalFile = TotalFile
                f.urlFile = FileUrl
                If FilePath <> "" Then
                    f.PathFile = FilePath
                Else
                    f.PathFile = FolderCourseDocumentFile & "\" & Temp.Rows(0).Item("id") & ".pptx"
                End If
                f.ViewAllFile = True
                'f.Show(frmLessonDialog)
                f.Show(frmMain)
            ElseIf InStr(FileUrl, ".html") Or InStr(FileUrl, ".htm") Then
                Dim f As New frmDisplayURL
                f.DocumentFileID = Temp.Rows(0).Item("id")
                f.CourseID = Temp.Rows(0).Item("tb_course_id")
                f.CurrentDocID = Temp.Rows(0).Item("document_id")
                f.FileNo = Temp.Rows(0).Item("sort")
                f.ClassID = ClassID
                f.TotalFile = TotalFile
                f.urlFile = FileUrl
                f.ViewAllFile = True
                'f.Show(frmLessonDialog)
                f.Show(frmMain)
            End If
        Else
            MessageBox.Show("File not found", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
        DT_DOCUMENT_ALL_FILE.DefaultView.RowFilter = ""
    End Sub

    Public Function UpdateCouseDocFilePath(ByVal TbCourseDocumentFileID As Long, ByVal FilePath As String) As Boolean
        Dim ret As Boolean = False
        Try
            Dim sql As String = "update tb_course_document_file"
            sql += " set file_path = '" & EnCripText(FilePath) & "'"
            sql += " where id=" & TbCourseDocumentFileID

            ret = ExecuteSqlNoneQuery(sql)
        Catch ex As Exception
            ret = False
        End Try
        Return ret
    End Function

    Public Function GetDatableTableFromTesting() As Boolean
        Dim ret As Boolean = False
        Try
            DT_TEST = New DataTable
            DT_TEST.Columns.Add("id", GetType(Integer))
            DT_TEST.Columns.Add("title")
            DT_TEST.Columns.Add("description")
            DT_TEST.Columns.Add("target_percentage", GetType(Integer))
            DT_TEST.Columns.Add("course_id", GetType(Integer))
            DT_TEST.Columns.Add("question_qty", GetType(Integer))

            DT_TEST_QUESTION = New DataTable
            DT_TEST_QUESTION.Columns.Add("id", GetType(Integer))
            DT_TEST_QUESTION.Columns.Add("tb_test_id", GetType(Integer))
            DT_TEST_QUESTION.Columns.Add("question")
            DT_TEST_QUESTION.Columns.Add("icon_url")
            DT_TEST_QUESTION.Columns.Add("icon_file")
            DT_TEST_QUESTION.Columns.Add("choice")
            DT_TEST_QUESTION.Columns.Add("answer")
            DT_TEST_QUESTION.Columns.Add("question_no", GetType(Integer))

            Dim QuestionIconFolder As String = "QuestionIcon"
            DeleteQuestionIcon(QuestionIconFolder)

            Dim info As String = ""
            info = GetStringDataFromURL(WebServiceURL & "api/testing/get", Token & "&client_id=" & myUser.ClientID & "&user_id=" & myUser.UserID)
            If info.Trim = "" Then Return False

            Dim json As String = info
            Dim ser As JObject = JObject.Parse(json)
            Dim data As List(Of JToken) = ser.Children().ToList
            Dim output As String = ""

            For Each item As JProperty In data
                item.CreateReader()
                Select Case item.Name
                    Case "testing"
                        Dim question_id As Int32 = 0

                        For Each comment As JObject In item.Values
                            Dim test_id As Int32 = comment("id")

                            Dim tDr As DataRow = DT_TEST.NewRow
                            tDr("id") = test_id
                            tDr("title") = comment("title").ToString
                            tDr("description") = comment("description").ToString
                            tDr("target_percentage") = Convert.ToInt32(comment("target_percentage"))
                            tDr("course_id") = Convert.ToInt32(comment("course_id"))
                            DT_TEST.Rows.Add(tDr)

                            Dim question_qty As Integer = 0
                            Dim question_txt As String = "{""question"":" & comment("question").ToString & "}"
                            Dim question_ser As JObject = JObject.Parse(question_txt)
                            Dim question_data As List(Of JToken) = question_ser.Children().ToList
                            For Each question_item As JProperty In question_data
                                For Each question_comment As JObject In question_item.Values
                                    question_id = question_id + 1
                                    question_qty = question_qty + 1
                                    question_item.CreateReader()

                                    Dim vChoice As String = ""
                                    Dim vAnswer As String = ""
                                    'Dim vAnswerID As String = ""
                                    Dim answer_txt As String = "{""answer"":" & question_comment("answer").ToString & "}"
                                    Dim answer_ser As JObject = JObject.Parse(answer_txt)
                                    Dim answer_data As List(Of JToken) = answer_ser.Children().ToList
                                    For Each answer_item As JProperty In answer_data

                                        Dim p As Integer = 1
                                        For Each answer_comment As JObject In answer_item.Values
                                            answer_item.CreateReader()

                                            Dim PreAlphabet As String = ""
                                            Select Case p
                                                Case 1
                                                    PreAlphabet = "ก. "
                                                Case 2
                                                    PreAlphabet = "ข. "
                                                Case 3
                                                    PreAlphabet = "ค. "
                                                Case 4
                                                    PreAlphabet = "ง. "
                                            End Select


                                            If vChoice = "" Then
                                                vChoice = PreAlphabet & answer_comment("text").ToString
                                            Else
                                                vChoice += "##" + PreAlphabet & answer_comment("text").ToString
                                            End If

                                            If vAnswer = "" Then
                                                vAnswer = answer_comment("is_correct").ToString
                                            Else
                                                vAnswer += "##" + answer_comment("is_correct").ToString
                                            End If
                                            p += 1
                                        Next
                                    Next

                                    Dim qDr As DataRow = DT_TEST_QUESTION.NewRow
                                    qDr("id") = question_id
                                    qDr("tb_test_id") = test_id
                                    qDr("question") = question_comment("description").ToString
                                    qDr("icon_url") = question_comment("cover").ToString
                                    'qDr("icon_file") = SaveQuestionIcon(QuestionIconFolder, question_comment("cover").ToString, question_id)
                                    qDr("icon_file") = question_comment("cover").ToString
                                    qDr("choice") = vChoice
                                    qDr("answer") = vAnswer
                                    qDr("question_no") = question_qty
                                    DT_TEST_QUESTION.Rows.Add(qDr)
                                Next
                            Next

                            'จำนวนคำถามในแบบทดสอบ
                            DT_TEST.Rows(DT_TEST.Rows.Count - 1)("question_qty") = question_qty
                        Next
                    Case "course_data"
                        For Each comment As JProperty In item.Values
                            Select Case comment.Name
                                Case "complete"
                                    myUser.CourseComplete = comment.First
                                Case "total"
                                    myUser.CourseTotal = comment.Last
                            End Select
                        Next
                    Case "testing_data"
                        For Each comment As JProperty In item.Values
                            Select Case comment.Name
                                Case "attempt"
                                    myUser.TestingAttempt = comment.First
                                Case "complete"
                                    myUser.TestingComplete = comment.First
                                Case "total"
                                    myUser.TestingTotal = comment.Last
                            End Select
                        Next
                End Select
            Next

            ret = True
        Catch ex As Exception
            'MessageBox.Show(ex.Message, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
            ret = False
        End Try
        Return ret
    End Function

    Public Sub OpenByDocument(DocumentID As Integer)
        Dim sql As String = "select cdf.id, cdf.file_url, cdf.file_path, cdf.sort, cdf.tb_course_document_id, cd.tb_course_id "
        sql += " from tb_course_document_file cdf "
        sql += " inner join tb_course_document cd on cd.id=cdf.tb_course_document_id"
        sql += " where cdf.tb_course_document_id=" & DocumentID
        sql += " order by cdf.sort"
        Dim Temp As DataTable = GetSqlDataTable(sql)
        If Temp.Rows.Count > 0 AndAlso Temp.Rows(0).Item("file_url").ToString <> "" Then

            DT_DOCUMENT_ALL_FILE = New DataTable
            DT_DOCUMENT_ALL_FILE.Columns.Add("id")
            DT_DOCUMENT_ALL_FILE.Columns.Add("tb_course_id")
            DT_DOCUMENT_ALL_FILE.Columns.Add("document_id")
            DT_DOCUMENT_ALL_FILE.Columns.Add("file")
            DT_DOCUMENT_ALL_FILE.Columns.Add("file_path")
            DT_DOCUMENT_ALL_FILE.Columns.Add("sort")

            For Each TempDr As DataRow In Temp.Rows
                Dim dr As DataRow = DT_DOCUMENT_ALL_FILE.NewRow
                dr("id") = TempDr("id").ToString
                dr("tb_course_id") = TempDr("tb_course_id")
                dr("document_id") = TempDr("tb_course_document_id").ToString
                dr("file") = TempDr("file_url").ToString
                dr("file_path") = TempDr("file_path").ToString
                If Convert.IsDBNull(TempDr("file_path")) = False Then dr("file_path") = TempDr("file_path").ToString
                dr("sort") = TempDr("sort").ToString

                DT_DOCUMENT_ALL_FILE.Rows.Add(dr)
            Next

            If DT_DOCUMENT_ALL_FILE.Rows.Count = 0 Then
                MessageBox.Show("File not found", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If

            ShowFormDocument(DT_DOCUMENT_ALL_FILE.Rows(0)("sort"))
        Else
            MessageBox.Show("File not found", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Public Sub DeleteClientLogonInfo()
        Try
            Dim sql As String = "delete from TB_CURRENT_LOGON_USER"
            ExecuteSqlNoneQuery(sql)
        Catch ex As Exception

        End Try
    End Sub

    Public Sub SaveClientLogonInfo(UserID As String, UserName As String, ClientID As String, ClientVersion As String)
        Try
            Dim sql As String = "select user_id from TB_CURRENT_LOGON_USER"
            Dim dt As DataTable = GetSqlDataTable(sql)
            If dt.Rows.Count = 0 Then
                sql = "insert into TB_CURRENT_LOGON_USER (user_id,username,client_id,client_version)"
                sql += " values('" & UserID & "'"
                sql += ", '" & UserName & "'"
                sql += ", '" & ClientID & "'"
                sql += ", '" & ClientVersion & "')"
            Else
                sql = "update TB_CURRENT_LOGON_USER"
                sql += " set user_id='" & UserID & "'"
                sql += ", username='" & UserName & "'"
                sql += ", client_id = '" & ClientID & "'"
                sql += ", client_version = '" & ClientVersion & "'"
            End If
            ExecuteSqlNoneQuery(sql)
        Catch ex As Exception

        End Try
    End Sub

    Public Function Plexiglass(dialog As Form) As DialogResult
        Using plexi = New Form()
            plexi.FormBorderStyle = FormBorderStyle.None
            plexi.Bounds = Screen.FromPoint(dialog.Location).Bounds
            plexi.StartPosition = FormStartPosition.Manual
            plexi.AutoScaleMode = AutoScaleMode.None
            plexi.ShowInTaskbar = False
            plexi.BackColor = Color.Black
            plexi.Opacity = 0.45
            plexi.Show()
            dialog.StartPosition = FormStartPosition.CenterParent
            Return dialog.ShowDialog(plexi)
        End Using
    End Function

    Public Function Plexiglass(dialog As Form, MainForm As Form) As DialogResult
        Using plexi = New Form()
            plexi.FormBorderStyle = FormBorderStyle.None
            plexi.Bounds = Screen.FromPoint(dialog.Location).Bounds
            plexi.StartPosition = FormStartPosition.Manual
            plexi.AutoScaleMode = AutoScaleMode.None
            plexi.ShowInTaskbar = False
            plexi.BackColor = Color.Black
            plexi.Opacity = 0.45
            plexi.Show(MainForm)
            dialog.StartPosition = FormStartPosition.CenterParent
            Return dialog.ShowDialog(plexi)
        End Using
    End Function

    'Public Function MyProxy() As WebProxy
    '    Dim ini As New IniReader(INIFile)
    '    ini.Section = "PROXY"
    '    Dim Address As String = ini.ReadString("Address")
    '    Dim Port As String = ini.ReadString("Port")
    '    Dim UserName As String = ini.ReadString("UserName")
    '    Dim Password As String = ini.ReadString("Password")
    '    Dim Domain As String = ini.ReadString("Domain")
    '    Dim Bypass As String = ini.ReadString("Bypass")
    '    If Address = "" Then Return Nothing
    '    If Not IsNumeric(Port) Then Return Nothing
    '    Try
    '        Dim Result As New WebProxy(Address, CInt(Port))
    '        If UserName = "" Or Password = "" Then Return Result
    '        Result.Credentials = New NetworkCredential(UserName, Password, Address)
    '        Return Result
    '    Catch ex As Exception
    '        Return Nothing
    '    End Try
    'End Function

    Function GetPasscode(student_id_list As String, course_id As Integer) As String
        'token : String,
        'client_id : Number,
        'teacher_id : Number, //user_id
        'student_id_list: [Number],
        'course_id : Number()

        Dim ret As String = ""
        Try
            Dim info As String = ""
            info = GetStringDataFromURL(WebServiceURL & "api/passcode/add", "token=" & myUser.Token & "&client_id=" & myUser.ClientID & "&teacher_id=" & myUser.UserID & "&student_id_list=" & student_id_list & "&course_id=" & course_id)

            Dim json As String = info
            Dim ser As JObject = JObject.Parse(json)
            Dim data As List(Of JToken) = ser.Children().ToList
            If data.Count = 3 Then
                Dim iTemStatus As JProperty = DirectCast(data.Item(0), JProperty)
                If iTemStatus.Name = "status" Then
                    If iTemStatus.Value = "True" Then
                        Dim Passcode As String = DirectCast(data.Item(2), JProperty)
                        ret = Passcode
                    End If
                End If
            End If
        Catch ex As Exception
            ret = ""
        End Try
        Return ret
    End Function


#Region "Convert PPT to PDF"
    Sub ConvertPPTtoPDF(InputFilePath As String, OutputFilePath As String)
        'Imports Microsoft.Office.Interop.PowerPoint
        Dim Errors As Integer = 0
        Dim PPApplication As Microsoft.Office.Interop.PowerPoint.Application
        Dim PPDoc As Microsoft.Office.Interop.PowerPoint.Presentation = Nothing

        Try
            ' Start an instance of PowerPoint
            PPApplication = New Microsoft.Office.Interop.PowerPoint.Application
            'PPApplication.Visible = True

            ' Open the source document.
            PPDoc = PPApplication.Presentations.Open(InputFilePath, WithWindow:=False)

            PPDoc.SaveAs(OutputFilePath, Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsJPG)

            'PPDoc.ExportAsFixedFormat(OutputFilePath, PpFixedFormatType.ppFixedFormatTypePDF, PpFixedFormatIntent.ppFixedFormatIntentScreen, Microsoft.Office.Core.MsoTriState.msoCTrue, PpPrintHandoutOrder.ppPrintHandoutHorizontalFirst, PpPrintOutputType.ppPrintOutputBuildSlides, Microsoft.Office.Core.MsoTriState.msoFalse, , , , False, False, False, False, False)


        Catch ex As Exception

            MsgBox(ex.Message)
            Errors = 1

        Finally
            ' Close and release the Document object.
            If Not PPDoc Is Nothing Then
                PPDoc.Close()
                PPDoc = Nothing
            End If

            ' Quit PowerPoint and release the ApplicationClass object.
            If Not PPApplication Is Nothing Then
                PPApplication.Quit()
                PPApplication = Nothing
            End If

            GC.Collect()
            GC.WaitForPendingFinalizers()
            GC.Collect()
            GC.WaitForPendingFinalizers()

        End Try


    End Sub
#End Region

#Region "Database Query"
    Public Function GetSqlDataTable(sql As String) As DataTable
        Dim dt As New DataTable
        Try
            Dim da As New OleDbDataAdapter(sql, ConnStr)
            da.Fill(dt)
        Catch ex As Exception
            dt = New DataTable
        End Try

        Return dt
    End Function
    Public Function GetAccessSchema(tableName As String) As DataTable
        Dim dt As New DataTable
        Try
            Dim conn As New OleDbConnection(ConnStr)
            conn.Open()
            dt = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Columns, New Object() {Nothing, Nothing, tableName, Nothing})

        Catch ex As Exception
            dt = New DataTable
        End Try

        Return dt
    End Function

    Public Function ExecuteSqlNoneQuery(sql As String) As Boolean
        Dim ret As Boolean = False
        Try
            Dim Command As New OleDbCommand
            Dim Conn As New OleDbConnection(ConnStr)
            Conn.Open()

            With Command
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = sql
                ret = (.ExecuteNonQuery() > -1)
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()

        Catch ex As Exception
            MessageBox.Show(ex.Message)
            ret = False
        End Try

        Return ret
    End Function
#End Region

    Public Function BuiltHtmlDocumentText(txtHtml As String) As String
        Dim ret As String = "" '= "<div style=""overflow-y:scroll;position:absolute;height:250px"">"
        'ret += "<table cellpadding=""0"" cellspacing=""0"" style=""height:100%;width:100%;"" >"
        'ret += "<tr>"
        'ret += "<td style=""width:100%"">"
        ret += txtHtml
        'ret += "</td>"
        'ret += "</tr>"
        'ret += "</table>"
        Return ret
    End Function
    Public Function getMyVersion() As String
        Dim version As System.Version = System.Reflection.Assembly.GetExecutingAssembly.GetName().Version
        Return version.Major & "." & version.Minor & "." & version.Build '& "." & version.Revision
    End Function

#Region " Encrypt/Decrypt "

    Public Function EnCripText(ByVal passString As String) As String
        Dim encrypted As String = ""
        Try
            Dim ByteData() As Byte = System.Text.Encoding.UTF8.GetBytes(passString)
            encrypted = Convert.ToBase64String(ByteData)
        Catch ex As Exception
        End Try

        Return encrypted
    End Function

    Public Function DeCripText(ByVal passString As String) As String
        Dim decrypted As String = ""
        Try
            Dim ByteData() As Byte = Convert.FromBase64String(passString)
            decrypted = System.Text.Encoding.UTF8.GetString(ByteData)
        Catch ex As Exception

        End Try
        '    Dim AES As New System.Security.Cryptography.RijndaelManaged
        '    Dim Hash_AES As New System.Security.Cryptography.MD5CryptoServiceProvider
        '    
        '    Try
        '        Dim hash(31) As Byte
        '        Dim temp As Byte() = Hash_AES.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(EncryptionKey))
        '        Array.Copy(temp, 0, hash, 0, 16)
        '        Array.Copy(temp, 0, hash, 15, 16)
        '        AES.Key = hash
        '        AES.Mode = System.Security.Cryptography.CipherMode.ECB
        '        Dim DESDecrypter As System.Security.Cryptography.ICryptoTransform = AES.CreateDecryptor
        '        Dim Buffer As Byte() = Convert.FromBase64String(passString)
        '        decrypted = System.Text.ASCIIEncoding.ASCII.GetString(DESDecrypter.TransformFinalBlock(Buffer, 0, Buffer.Length))
        '    Catch ex As Exception
        '    End Try

        Return decrypted
    End Function
#End Region

End Module
