﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmActivation
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmActivation))
        Me.lbl1 = New System.Windows.Forms.Label()
        Me.lbl2 = New System.Windows.Forms.Label()
        Me.txtActivate = New System.Windows.Forms.TextBox()
        Me.pnlFalse = New System.Windows.Forms.Panel()
        Me.btnBack = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.pnlTrue = New System.Windows.Forms.Panel()
        Me.btnLogin = New System.Windows.Forms.PictureBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.btnActivate = New System.Windows.Forms.PictureBox()
        Me.imgLogin = New System.Windows.Forms.PictureBox()
        Me.btnExit = New System.Windows.Forms.PictureBox()
        Me.pnlFalse.SuspendLayout()
        CType(Me.btnBack, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlTrue.SuspendLayout()
        CType(Me.btnLogin, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnActivate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgLogin, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnExit, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lbl1
        '
        Me.lbl1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl1.BackColor = System.Drawing.Color.White
        Me.lbl1.Font = New System.Drawing.Font("Tahoma", 24.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lbl1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(6, Byte), Integer), CType(CType(79, Byte), Integer), CType(CType(65, Byte), Integer))
        Me.lbl1.Location = New System.Drawing.Point(337, 246)
        Me.lbl1.Name = "lbl1"
        Me.lbl1.Size = New System.Drawing.Size(363, 50)
        Me.lbl1.TabIndex = 15
        Me.lbl1.Text = "Tesco Lotus Learning Center"
        Me.lbl1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl2
        '
        Me.lbl2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl2.BackColor = System.Drawing.Color.White
        Me.lbl2.Font = New System.Drawing.Font("Tahoma", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lbl2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(113, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(118, Byte), Integer))
        Me.lbl2.Location = New System.Drawing.Point(343, 300)
        Me.lbl2.Name = "lbl2"
        Me.lbl2.Size = New System.Drawing.Size(357, 35)
        Me.lbl2.TabIndex = 16
        Me.lbl2.Text = "สำหรับการติดตั้งครั้งแรกกรุณาใส่ VERIFICATION CODE"
        Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtActivate
        '
        Me.txtActivate.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtActivate.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtActivate.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtActivate.ForeColor = System.Drawing.Color.Gray
        Me.txtActivate.Location = New System.Drawing.Point(396, 345)
        Me.txtActivate.MaxLength = 30
        Me.txtActivate.Name = "txtActivate"
        Me.txtActivate.Size = New System.Drawing.Size(238, 23)
        Me.txtActivate.TabIndex = 19
        Me.txtActivate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'pnlFalse
        '
        Me.pnlFalse.BackColor = System.Drawing.SystemColors.ControlDark
        Me.pnlFalse.Controls.Add(Me.btnBack)
        Me.pnlFalse.Controls.Add(Me.Label1)
        Me.pnlFalse.Controls.Add(Me.PictureBox3)
        Me.pnlFalse.Controls.Add(Me.PictureBox2)
        Me.pnlFalse.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlFalse.Location = New System.Drawing.Point(0, 0)
        Me.pnlFalse.Name = "pnlFalse"
        Me.pnlFalse.Size = New System.Drawing.Size(1037, 625)
        Me.pnlFalse.TabIndex = 22
        Me.pnlFalse.Visible = False
        '
        'btnBack
        '
        Me.btnBack.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnBack.BackColor = System.Drawing.Color.White
        Me.btnBack.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnBack.Image = Global.TESCO_TMS.My.Resources.Resources.btnActivateBack
        Me.btnBack.Location = New System.Drawing.Point(447, 385)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(148, 39)
        Me.btnBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnBack.TabIndex = 24
        Me.btnBack.TabStop = False
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(113, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(118, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(340, 339)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(357, 35)
        Me.Label1.TabIndex = 23
        Me.Label1.Text = "VERIFICATION CODE ไม่ถูกต้อง"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'PictureBox3
        '
        Me.PictureBox3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox3.BackColor = System.Drawing.Color.White
        Me.PictureBox3.Image = Global.TESCO_TMS.My.Resources.Resources.interface_frontend_icoLock
        Me.PictureBox3.Location = New System.Drawing.Point(468, 201)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(100, 110)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 22
        Me.PictureBox3.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox2.Image = Global.TESCO_TMS.My.Resources.Resources.WelcomBoxMessage
        Me.PictureBox2.Location = New System.Drawing.Point(307, 185)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(422, 254)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 0
        Me.PictureBox2.TabStop = False
        '
        'pnlTrue
        '
        Me.pnlTrue.BackColor = System.Drawing.SystemColors.ControlDark
        Me.pnlTrue.Controls.Add(Me.btnLogin)
        Me.pnlTrue.Controls.Add(Me.Label2)
        Me.pnlTrue.Controls.Add(Me.PictureBox7)
        Me.pnlTrue.Controls.Add(Me.PictureBox8)
        Me.pnlTrue.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlTrue.Location = New System.Drawing.Point(0, 0)
        Me.pnlTrue.Name = "pnlTrue"
        Me.pnlTrue.Size = New System.Drawing.Size(1037, 625)
        Me.pnlTrue.TabIndex = 23
        Me.pnlTrue.Visible = False
        '
        'btnLogin
        '
        Me.btnLogin.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnLogin.BackColor = System.Drawing.Color.White
        Me.btnLogin.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnLogin.Image = CType(resources.GetObject("btnLogin.Image"), System.Drawing.Image)
        Me.btnLogin.Location = New System.Drawing.Point(447, 385)
        Me.btnLogin.Name = "btnLogin"
        Me.btnLogin.Size = New System.Drawing.Size(148, 39)
        Me.btnLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnLogin.TabIndex = 24
        Me.btnLogin.TabStop = False
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label2.BackColor = System.Drawing.Color.White
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(113, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(118, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(340, 339)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(357, 35)
        Me.Label2.TabIndex = 23
        Me.Label2.Text = "VERIFICATION CODE ถูกต้อง"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'PictureBox7
        '
        Me.PictureBox7.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox7.BackColor = System.Drawing.Color.White
        Me.PictureBox7.Image = Global.TESCO_TMS.My.Resources.Resources.interface_frontend_icoUnlock
        Me.PictureBox7.Location = New System.Drawing.Point(468, 201)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(100, 110)
        Me.PictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox7.TabIndex = 22
        Me.PictureBox7.TabStop = False
        '
        'PictureBox8
        '
        Me.PictureBox8.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox8.Image = Global.TESCO_TMS.My.Resources.Resources.WelcomBoxMessage
        Me.PictureBox8.Location = New System.Drawing.Point(307, 185)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(422, 254)
        Me.PictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox8.TabIndex = 0
        Me.PictureBox8.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox1.BackColor = System.Drawing.Color.White
        Me.PictureBox1.Image = Global.TESCO_TMS.My.Resources.Resources.interface_frontend_icoLock
        Me.PictureBox1.Location = New System.Drawing.Point(465, 133)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(100, 110)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 21
        Me.PictureBox1.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox5.Image = Global.TESCO_TMS.My.Resources.Resources.Textbox
        Me.PictureBox5.Location = New System.Drawing.Point(393, 336)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(244, 41)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox5.TabIndex = 20
        Me.PictureBox5.TabStop = False
        '
        'btnActivate
        '
        Me.btnActivate.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnActivate.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnActivate.Image = Global.TESCO_TMS.My.Resources.Resources.btnEnter
        Me.btnActivate.Location = New System.Drawing.Point(444, 399)
        Me.btnActivate.Name = "btnActivate"
        Me.btnActivate.Size = New System.Drawing.Size(148, 39)
        Me.btnActivate.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnActivate.TabIndex = 17
        Me.btnActivate.TabStop = False
        '
        'imgLogin
        '
        Me.imgLogin.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.imgLogin.Image = Global.TESCO_TMS.My.Resources.Resources.Activation
        Me.imgLogin.Location = New System.Drawing.Point(95, 112)
        Me.imgLogin.Name = "imgLogin"
        Me.imgLogin.Size = New System.Drawing.Size(672, 458)
        Me.imgLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgLogin.TabIndex = 1
        Me.imgLogin.TabStop = False
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnExit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnExit.Image = Global.TESCO_TMS.My.Resources.Resources.btnExit1
        Me.btnExit.Location = New System.Drawing.Point(905, 14)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(117, 46)
        Me.btnExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnExit.TabIndex = 24
        Me.btnExit.TabStop = False
        '
        'frmActivation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(119, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1037, 625)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.pnlTrue)
        Me.Controls.Add(Me.pnlFalse)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.txtActivate)
        Me.Controls.Add(Me.PictureBox5)
        Me.Controls.Add(Me.btnActivate)
        Me.Controls.Add(Me.lbl2)
        Me.Controls.Add(Me.lbl1)
        Me.Controls.Add(Me.imgLogin)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmActivation"
        Me.ShowInTaskbar = False
        Me.Text = "frmActivation"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.pnlFalse.ResumeLayout(False)
        CType(Me.btnBack, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlTrue.ResumeLayout(False)
        CType(Me.btnLogin, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnActivate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgLogin, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnExit, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lbl1 As System.Windows.Forms.Label
    Friend WithEvents btnActivate As System.Windows.Forms.PictureBox
    Friend WithEvents lbl2 As System.Windows.Forms.Label
    Friend WithEvents imgLogin As System.Windows.Forms.PictureBox
    Friend WithEvents txtActivate As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents pnlFalse As System.Windows.Forms.Panel
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents btnBack As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents pnlTrue As System.Windows.Forms.Panel
    Friend WithEvents btnLogin As System.Windows.Forms.PictureBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox8 As System.Windows.Forms.PictureBox
    Friend WithEvents btnExit As System.Windows.Forms.PictureBox
End Class
