﻿
Public Class frmActivation

    Dim buttonClick As Boolean = False

    Private Sub btnActivate_Click(sender As Object, e As System.EventArgs) Handles btnActivate.Click
        If txtActivate.Text = "" Then
            MessageBox.Show("Please enter verification code", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        If TescoActivate(txtActivate.Text) Then
            pnlTrue.Visible = True
        Else
            pnlFalse.Visible = True
        End If
        txtActivate.Text = ""
    End Sub

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        pnlFalse.Visible = False
    End Sub

    Private Sub btnLogin_Click(sender As Object, e As System.EventArgs) Handles btnLogin.Click
        buttonClick = True
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub txtActivate_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtActivate.KeyPress
        If Asc(e.KeyChar) = 13 Then
            btnActivate_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        buttonClick = True
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub frmActivation_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If buttonClick = False Then
            e.Cancel = True
            Exit Sub
        End If
    End Sub
End Class