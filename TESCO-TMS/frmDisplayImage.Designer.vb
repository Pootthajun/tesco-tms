﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDisplayImage
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtPage = New System.Windows.Forms.TextBox()
        Me.pbContent = New System.Windows.Forms.PictureBox()
        Me.btnClose = New System.Windows.Forms.PictureBox()
        Me.pb = New System.Windows.Forms.PictureBox()
        Me.btnPreviousDoc = New System.Windows.Forms.PictureBox()
        Me.btnNextDoc = New System.Windows.Forms.PictureBox()
        CType(Me.pbContent, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pb, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnPreviousDoc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnNextDoc, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtPage
        '
        Me.txtPage.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPage.BackColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(66, Byte), Integer))
        Me.txtPage.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPage.Font = New System.Drawing.Font("Microsoft Sans Serif", 1.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPage.ForeColor = System.Drawing.Color.Black
        Me.txtPage.Location = New System.Drawing.Point(772, 563)
        Me.txtPage.Name = "txtPage"
        Me.txtPage.Size = New System.Drawing.Size(26, 3)
        Me.txtPage.TabIndex = 34
        Me.txtPage.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'pbContent
        '
        Me.pbContent.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pbContent.Cursor = System.Windows.Forms.Cursors.Hand
        Me.pbContent.Image = Global.TESCO_TMS.My.Resources.Resources.index_icon
        Me.pbContent.Location = New System.Drawing.Point(597, 519)
        Me.pbContent.Name = "pbContent"
        Me.pbContent.Size = New System.Drawing.Size(104, 50)
        Me.pbContent.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbContent.TabIndex = 42
        Me.pbContent.TabStop = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnClose.Image = Global.TESCO_TMS.My.Resources.Resources.btnCloseDoc
        Me.btnClose.Location = New System.Drawing.Point(717, 519)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(105, 50)
        Me.btnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnClose.TabIndex = 30
        Me.btnClose.TabStop = False
        '
        'pb
        '
        Me.pb.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pb.Location = New System.Drawing.Point(3, 3)
        Me.pb.Name = "pb"
        Me.pb.Size = New System.Drawing.Size(828, 510)
        Me.pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pb.TabIndex = 33
        Me.pb.TabStop = False
        '
        'btnPreviousDoc
        '
        Me.btnPreviousDoc.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnPreviousDoc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnPreviousDoc.Image = Global.TESCO_TMS.My.Resources.Resources.btnPreviousDoc
        Me.btnPreviousDoc.Location = New System.Drawing.Point(277, 519)
        Me.btnPreviousDoc.Name = "btnPreviousDoc"
        Me.btnPreviousDoc.Size = New System.Drawing.Size(119, 50)
        Me.btnPreviousDoc.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnPreviousDoc.TabIndex = 32
        Me.btnPreviousDoc.TabStop = False
        '
        'btnNextDoc
        '
        Me.btnNextDoc.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnNextDoc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNextDoc.Image = Global.TESCO_TMS.My.Resources.Resources.btnNextDoc
        Me.btnNextDoc.Location = New System.Drawing.Point(438, 519)
        Me.btnNextDoc.Name = "btnNextDoc"
        Me.btnNextDoc.Size = New System.Drawing.Size(117, 50)
        Me.btnNextDoc.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnNextDoc.TabIndex = 31
        Me.btnNextDoc.TabStop = False
        '
        'frmDisplayImage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(66, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(834, 578)
        Me.ControlBox = False
        Me.Controls.Add(Me.pbContent)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.txtPage)
        Me.Controls.Add(Me.pb)
        Me.Controls.Add(Me.btnPreviousDoc)
        Me.Controls.Add(Me.btnNextDoc)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmDisplayImage"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.pbContent, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pb, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnPreviousDoc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnNextDoc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnClose As System.Windows.Forms.PictureBox
    Friend WithEvents btnPreviousDoc As System.Windows.Forms.PictureBox
    Friend WithEvents btnNextDoc As System.Windows.Forms.PictureBox
    Friend WithEvents pb As System.Windows.Forms.PictureBox
    Friend WithEvents txtPage As System.Windows.Forms.TextBox
    Friend WithEvents pbContent As System.Windows.Forms.PictureBox
End Class
