﻿Imports System.Data

Public Class frmDisplayImage

    Public CourseID As Int32
    Public DocumentFileID As Int32
    Public CurrentDocID As Int32
    Public ClassID As Long
    Public FileNo As Int32
    Public TotalFile As Int32
    Public urlFile As String
    Public PathFile As String
    Public ViewAllFile As Boolean = False

    Private Sub frmDisplayImage_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.WindowState = FormWindowState.Maximized
        Me.ControlBox = False

        If FileNo = 1 Then
            btnPreviousDoc.Visible = False
        End If

        'บันทึกข้อมูลเอกสารที่เรียนผ่านแล้ว
        CallAPIUpdateLog(Token, myUser.ClientID, "complete", "document", "{" & Chr(34) & "class_id" & Chr(34) & ":" & ClassID & "," & Chr(34) & "document_id" & Chr(34) & ":" & CurrentDocID & "}")

        If FileNo = TotalFile Then
            btnNextDoc.Visible = False

            'เมื่อเรียนจบหลักสูตรให้บันทึก Log
            CallAPIUpdateLog(Token, myUser.ClientID, "complete", "class", "{" & Chr(34) & "class_id" & Chr(34) & ":" & ClassID & "}")
        End If

        Dim Image As Image = GetImageFromURL(urlFile)
        pb.Image = Image

    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub txtPage_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtPage.KeyUp
        Try
            Select Case e.KeyCode
                Case Keys.PageUp
                    If btnNextDoc.Visible = True Then
                        btnNextDoc_Click(Nothing, Nothing)
                    End If
                Case Keys.PageDown
                    If btnPreviousDoc.Visible = True Then
                        btnPreviousDoc_Click(Nothing, Nothing)
                    End If
                Case Keys.Home
                    btnClose_Click(Nothing, Nothing)
            End Select
        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnPreviousDoc_Click(sender As System.Object, e As System.EventArgs) Handles btnPreviousDoc.Click

        If ViewAllFile Then
            ShowFormDocumentAllFile(FileNo - 1, ClassID)
        Else
            'ShowFormDocumentFile(CourseID, DocumentFileID)
            ShowFormDocument(FileNo - 1)
        End If
        Me.Close()
    End Sub

    Private Sub btnNextDoc_Click(sender As System.Object, e As System.EventArgs) Handles btnNextDoc.Click

        If ViewAllFile Then
            ShowFormDocumentAllFile(FileNo + 1, ClassID)
        Else
            'ShowFormDocumentFile(CourseID, DocumentFileID)
            ShowFormDocument(FileNo + 1)
        End If
        Me.Close()
    End Sub

    Private Sub pbContent_Click(sender As System.Object, e As System.EventArgs) Handles pbContent.Click
        If Me.OwnedForms.Length > 0 Then
            'ถ้าเปิดฟอร์ม frmShowListContent อยู่แล้ว ก็ไม่ต้องเปิดซ้ำอีก
            If Me.OwnedForms(0).Name = frmShowListContent.Name Then
                Exit Sub
            End If
        End If

        Dim sFrm As New frmShowListContent
        sFrm.CourseID = CourseID
        Application.DoEvents()

        sFrm.Show(Me)
    End Sub
End Class