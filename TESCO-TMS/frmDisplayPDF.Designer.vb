﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDisplayPDF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDisplayPDF))
        Me.lblTotalPage = New System.Windows.Forms.Label()
        Me.txtPage = New System.Windows.Forms.TextBox()
        Me.btnPreviousDisable = New System.Windows.Forms.PictureBox()
        Me.btnNextDisable = New System.Windows.Forms.PictureBox()
        Me.btnClose = New System.Windows.Forms.PictureBox()
        Me.btnPrevious = New System.Windows.Forms.PictureBox()
        Me.btnNext = New System.Windows.Forms.PictureBox()
        Me.btnPreviousDoc = New System.Windows.Forms.PictureBox()
        Me.btnNextDoc = New System.Windows.Forms.PictureBox()
        Me.PDF = New AxPDFViewerLib.AxPDFViewer()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.pbContent = New System.Windows.Forms.PictureBox()
        CType(Me.btnPreviousDisable, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnNextDisable, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnPrevious, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnNext, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnPreviousDoc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnNextDoc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PDF, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbContent, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblTotalPage
        '
        Me.lblTotalPage.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.lblTotalPage.AutoSize = True
        Me.lblTotalPage.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblTotalPage.ForeColor = System.Drawing.Color.White
        Me.lblTotalPage.Location = New System.Drawing.Point(422, 566)
        Me.lblTotalPage.Name = "lblTotalPage"
        Me.lblTotalPage.Size = New System.Drawing.Size(59, 31)
        Me.lblTotalPage.TabIndex = 27
        Me.lblTotalPage.Text = "/ 00"
        '
        'txtPage
        '
        Me.txtPage.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.txtPage.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPage.ForeColor = System.Drawing.Color.Black
        Me.txtPage.Location = New System.Drawing.Point(371, 563)
        Me.txtPage.Name = "txtPage"
        Me.txtPage.Size = New System.Drawing.Size(46, 38)
        Me.txtPage.TabIndex = 26
        Me.txtPage.Text = "1"
        Me.txtPage.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnPreviousDisable
        '
        Me.btnPreviousDisable.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnPreviousDisable.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnPreviousDisable.Image = Global.TESCO_TMS.My.Resources.Resources.btnPreviousDidable
        Me.btnPreviousDisable.Location = New System.Drawing.Point(285, 553)
        Me.btnPreviousDisable.Name = "btnPreviousDisable"
        Me.btnPreviousDisable.Size = New System.Drawing.Size(50, 50)
        Me.btnPreviousDisable.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnPreviousDisable.TabIndex = 29
        Me.btnPreviousDisable.TabStop = False
        Me.btnPreviousDisable.Visible = False
        '
        'btnNextDisable
        '
        Me.btnNextDisable.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnNextDisable.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnNextDisable.Image = Global.TESCO_TMS.My.Resources.Resources.btnNextDisable
        Me.btnNextDisable.Location = New System.Drawing.Point(505, 553)
        Me.btnNextDisable.Name = "btnNextDisable"
        Me.btnNextDisable.Size = New System.Drawing.Size(50, 50)
        Me.btnNextDisable.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnNextDisable.TabIndex = 28
        Me.btnNextDisable.TabStop = False
        Me.btnNextDisable.Visible = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnClose.Image = Global.TESCO_TMS.My.Resources.Resources.btnCloseDoc
        Me.btnClose.Location = New System.Drawing.Point(764, 553)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(105, 50)
        Me.btnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnClose.TabIndex = 17
        Me.btnClose.TabStop = False
        '
        'btnPrevious
        '
        Me.btnPrevious.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnPrevious.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnPrevious.Image = Global.TESCO_TMS.My.Resources.Resources.btnPrevious
        Me.btnPrevious.Location = New System.Drawing.Point(233, 553)
        Me.btnPrevious.Name = "btnPrevious"
        Me.btnPrevious.Size = New System.Drawing.Size(104, 50)
        Me.btnPrevious.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnPrevious.TabIndex = 23
        Me.btnPrevious.TabStop = False
        '
        'btnNext
        '
        Me.btnNext.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnNext.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNext.Image = Global.TESCO_TMS.My.Resources.Resources.btnNext
        Me.btnNext.Location = New System.Drawing.Point(525, 553)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(85, 50)
        Me.btnNext.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnNext.TabIndex = 22
        Me.btnNext.TabStop = False
        '
        'btnPreviousDoc
        '
        Me.btnPreviousDoc.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnPreviousDoc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnPreviousDoc.Image = Global.TESCO_TMS.My.Resources.Resources.btnPreviousDoc
        Me.btnPreviousDoc.Location = New System.Drawing.Point(96, 553)
        Me.btnPreviousDoc.Name = "btnPreviousDoc"
        Me.btnPreviousDoc.Size = New System.Drawing.Size(131, 50)
        Me.btnPreviousDoc.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnPreviousDoc.TabIndex = 34
        Me.btnPreviousDoc.TabStop = False
        '
        'btnNextDoc
        '
        Me.btnNextDoc.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnNextDoc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNextDoc.Image = Global.TESCO_TMS.My.Resources.Resources.btnNextDoc
        Me.btnNextDoc.Location = New System.Drawing.Point(620, 553)
        Me.btnNextDoc.Name = "btnNextDoc"
        Me.btnNextDoc.Size = New System.Drawing.Size(122, 50)
        Me.btnNextDoc.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnNextDoc.TabIndex = 33
        Me.btnNextDoc.TabStop = False
        '
        'PDF
        '
        Me.PDF.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PDF.Enabled = True
        Me.PDF.Location = New System.Drawing.Point(12, -21)
        Me.PDF.Name = "PDF"
        Me.PDF.OcxState = CType(resources.GetObject("PDF.OcxState"), System.Windows.Forms.AxHost.State)
        Me.PDF.Size = New System.Drawing.Size(857, 566)
        Me.PDF.TabIndex = 35
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Location = New System.Drawing.Point(0, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(834, 10)
        Me.Label1.TabIndex = 36
        '
        'pbContent
        '
        Me.pbContent.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pbContent.Cursor = System.Windows.Forms.Cursors.Hand
        Me.pbContent.Image = Global.TESCO_TMS.My.Resources.Resources.index_icon
        Me.pbContent.Location = New System.Drawing.Point(655, 553)
        Me.pbContent.Name = "pbContent"
        Me.pbContent.Size = New System.Drawing.Size(104, 50)
        Me.pbContent.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbContent.TabIndex = 41
        Me.pbContent.TabStop = False
        '
        'frmDisplayPDF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(66, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(881, 612)
        Me.ControlBox = False
        Me.Controls.Add(Me.pbContent)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PDF)
        Me.Controls.Add(Me.btnPreviousDoc)
        Me.Controls.Add(Me.btnNextDoc)
        Me.Controls.Add(Me.btnPreviousDisable)
        Me.Controls.Add(Me.btnNextDisable)
        Me.Controls.Add(Me.lblTotalPage)
        Me.Controls.Add(Me.txtPage)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnPrevious)
        Me.Controls.Add(Me.btnNext)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmDisplayPDF"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.TransparencyKey = System.Drawing.Color.DarkMagenta
        CType(Me.btnPreviousDisable, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnNextDisable, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnPrevious, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnNext, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnPreviousDoc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnNextDoc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PDF, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbContent, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnClose As System.Windows.Forms.PictureBox
    Friend WithEvents btnNext As System.Windows.Forms.PictureBox
    Friend WithEvents btnPrevious As System.Windows.Forms.PictureBox
    Friend WithEvents lblTotalPage As System.Windows.Forms.Label
    Friend WithEvents txtPage As System.Windows.Forms.TextBox
    Friend WithEvents btnPreviousDisable As System.Windows.Forms.PictureBox
    Friend WithEvents btnNextDisable As System.Windows.Forms.PictureBox
    Friend WithEvents btnPreviousDoc As System.Windows.Forms.PictureBox
    Friend WithEvents btnNextDoc As System.Windows.Forms.PictureBox
    Friend WithEvents PDF As AxPDFViewerLib.AxPDFViewer
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents pbContent As System.Windows.Forms.PictureBox
End Class
