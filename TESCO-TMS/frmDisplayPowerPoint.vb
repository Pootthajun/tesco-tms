﻿Imports System.IO
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports DocumentConverter

Public Class frmDisplayPowerPoint

    Public CourseID As Int32
    Public DocumentFileID As Int32
    Public CurrentDocID As Int32
    Public ClassID As Long
    Public FileNo As Int32
    Public TotalFile As Int32
    Public urlFile As String
    Public PathFile As String
    Public ViewAllFile As Boolean = False
    Dim TempFile As String = ""

    Dim CurrentPage As Integer = 1
    Dim TotalPage As Integer = 1

    Private Sub frmDisplayPowerPoint_Shown(sender As System.Object, e As System.EventArgs) Handles MyBase.Shown
        Me.WindowState = FormWindowState.Maximized
        Me.ControlBox = False

        If FileNo = 1 Then
            btnPreviousDoc.Visible = False
        End If

        If FileNo = TotalFile Then
            btnNextDoc.Visible = False
        End If

        Dim EncryptFile As String = FolderCourseDocumentFile & "\" & EnCripText(PathFile.Replace(FolderCourseDocumentFile & "\", ""))   'Encrypt เฉพาะชื่อไฟล์
        If File.Exists(EncryptFile) = False Then
            Dim f As New frmProgressDownload
            f.URL = urlFile
            f.PathDownloadTo = EncryptFile
            If f.ShowDialog() = Windows.Forms.DialogResult.OK Then
                UpdateCouseDocFilePath(DocumentFileID, PathFile)
            End If
        End If

        If File.Exists(EncryptFile) = True Then

            Dim cPPTtoPDF As New ConvDoc2PdfWithMsOffice

            If InStr(urlFile, ".pptx") Then
                'TempFile = TempPath & "\" & EnCripText("4.pdf")
                TempFile = TempPath & "\" & "4.pdf"
            ElseIf InStr(urlFile, ".ppt") Then
                'TempFile = TempPath & "\" & EnCripText("5.pdf")
                TempFile = TempPath & "\" & "5.pdf"
            End If
            cPPTtoPDF.powerPoint2Pdf(EncryptFile, TempFile)
            PDF.LoadFile(TempFile)
            cPPTtoPDF = Nothing

            'PDF.Scrollbar = False
            'PDF.Toolbars = False
            PDF.SetReadOnly()
            PDF.DisableCopyToolbarButton()
            PDF.DisableHotKeyCopy()
            PDF.DisableHotKeyPrint()
            PDF.DisableHotKeySave()
            PDF.DisableHotKeySearch()
            PDF.DisableHotKeyShowBookMarks()
            PDF.DisableHotKeyShowThumnails()
            PDF.DisableHotKeyShowToolbars()
            PDF.DisablePrintToolbarButton()
            PDF.DisableSaveToolbarButton()

            Dim bytes As Byte() = File.ReadAllBytes(TempFile)
            Dim reader As New PdfReader(bytes)
            Dim pages As Integer = reader.NumberOfPages
            If pages > 0 Then
                lblTotalPage.Text = "/ " & pages.ToString
                btnPreviousEnable(False)
                btnNextEnable(True)
                TotalPage = pages
                PDF.GotToPage(1)
            Else
                CurrentPage = 0
                lblTotalPage.Text = "/ 0"
                btnPreviousEnable(False)
                btnNextEnable(False)
            End If
            txtPage.Text = CurrentPage.ToString

            Label1.Dock = DockStyle.Fill
            Label1.Parent = PDF
        End If
    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnPrevious_Click(sender As System.Object, e As System.EventArgs) Handles btnPrevious.Click
        PDF.GotoPreviousPage()
        CurrentPage = CurrentPage - 1
        ChangeButton()
    End Sub

    Private Sub btnNext_Click(sender As System.Object, e As System.EventArgs) Handles btnNext.Click
        PDF.GotoNextPage()
        CurrentPage = CurrentPage + 1
        ChangeButton()
    End Sub

    Private Sub txtPage_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtPage.KeyPress
        If (e.KeyChar < "0" Or e.KeyChar > "9") And Asc(e.KeyChar) <> 8 Then
            e.Handled = True
        End If
        If Asc(e.KeyChar) = 13 Then
            PDF.GotToPage(CInt(txtPage.Text))
            CurrentPage = CInt(txtPage.Text)
            ChangeButton()
        End If
    End Sub

    Private Sub txtPage_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtPage.KeyUp
        Select Case e.KeyCode
            Case Keys.Left
                If btnPrevious.Visible = True Then
                    btnPrevious_Click(Nothing, Nothing)
                End If
            Case Keys.Right
                If btnNext.Visible = True Then
                    btnNext_Click(Nothing, Nothing)
                End If
            Case Keys.Space
                If btnNext.Visible = True Then
                    btnNext_Click(Nothing, Nothing)
                End If
            Case Keys.PageUp
                If btnNextDoc.Visible = True Then
                    btnNextDoc_Click(Nothing, Nothing)
                End If
            Case Keys.PageDown
                If btnPreviousDoc.Visible = True Then
                    btnPreviousDoc_Click(Nothing, Nothing)
                End If
            Case Keys.Home
                btnClose_Click(Nothing, Nothing)
        End Select
    End Sub

    Sub ChangeButton()
        If CurrentPage = 1 Then
            btnPreviousEnable(False)
            btnNextEnable(True)
        ElseIf CurrentPage = TotalPage Or CurrentPage > TotalPage Then
            CurrentPage = TotalPage
            btnPreviousEnable(True)
            btnNextEnable(False)

            'บันทึกข้อมูลเอกสารที่เรียนผ่านแล้ว
            CallAPIUpdateLog(Token, myUser.ClientID, "complete", "document", "{" & Chr(34) & "class_id" & Chr(34) & ":" & ClassID & "," & Chr(34) & "document_id" & Chr(34) & ":" & CurrentDocID & "}")

            If FileNo = TotalFile Then
                'เมื่อเรียนจบหลักสูตรให้บันทึก Log
                CallAPIUpdateLog(Token, myUser.ClientID, "complete", "class", "{" & Chr(34) & "class_id" & Chr(34) & ":" & ClassID & "}")
            End If
        Else
            btnPreviousEnable(True)
            btnNextEnable(True)
        End If
        txtPage.Text = CurrentPage.ToString
    End Sub

    Sub btnPreviousEnable(ByVal EnableButton As Boolean)
        If EnableButton = True Then
            btnPrevious.Visible = True
            btnPreviousDisable.Visible = False
        Else
            btnPrevious.Visible = False
            btnPreviousDisable.Visible = True
        End If
    End Sub

    Sub btnNextEnable(ByVal EnableButton As Boolean)
        If EnableButton = True Then
            btnNext.Visible = True
            btnNextDisable.Visible = False
        Else
            btnNext.Visible = False
            btnNextDisable.Visible = True
        End If
    End Sub

    Private Sub btnPreviousDoc_Click(sender As System.Object, e As System.EventArgs) Handles btnPreviousDoc.Click
        PDF.Dispose()
        Threading.Thread.Sleep(2000)
        File.SetAttributes(TempFile, FileAttributes.Normal)
        File.Delete(TempFile)

        If ViewAllFile Then
            ShowFormDocumentAllFile(FileNo - 1, ClassID)
        Else
            'ShowFormDocumentFile(CourseID, DocumentFileID)
            ShowFormDocument(FileNo - 1)
        End If
        Me.Close()
    End Sub

    Private Sub btnNextDoc_Click(sender As System.Object, e As System.EventArgs) Handles btnNextDoc.Click

        PDF.Dispose()
        Threading.Thread.Sleep(2000)
        File.SetAttributes(TempFile, FileAttributes.Normal)
        File.Delete(TempFile)
        If ViewAllFile Then
            ShowFormDocumentAllFile(FileNo + 1, ClassID)
        Else
            'ShowFormDocumentFile(CourseID, DocumentFileID)
            ShowFormDocument(FileNo + 1)
        End If
        Me.Close()
    End Sub

    Private Sub frmDisplayPowerPoint_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        lblTotalPage.Focus()
    End Sub

    
    Private Sub pbContent_Click(sender As System.Object, e As System.EventArgs) Handles pbContent.Click
        If Me.OwnedForms.Length > 0 Then
            'ถ้าเปิดฟอร์ม frmShowListContent อยู่แล้ว ก็ไม่ต้องเปิดซ้ำอีก
            If Me.OwnedForms(0).Name = frmShowListContent.Name Then
                Exit Sub
            End If
        End If

        Dim sFrm As New frmShowListContent
        sFrm.CourseID = CourseID
        Application.DoEvents()

        sFrm.Show(Me)
    End Sub
End Class