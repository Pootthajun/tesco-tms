﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDisplaySWF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDisplaySWF))
        Me.btnClose = New System.Windows.Forms.PictureBox()
        Me.btnPreviousDoc = New System.Windows.Forms.PictureBox()
        Me.btnNextDoc = New System.Windows.Forms.PictureBox()
        Me.Flash = New AxShockwaveFlashObjects.AxShockwaveFlash()
        Me.txtPage = New System.Windows.Forms.TextBox()
        Me.pbContent = New System.Windows.Forms.PictureBox()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnPreviousDoc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnNextDoc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Flash, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbContent, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnClose.Image = Global.TESCO_TMS.My.Resources.Resources.btnCloseDoc
        Me.btnClose.Location = New System.Drawing.Point(711, 519)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(111, 50)
        Me.btnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnClose.TabIndex = 17
        Me.btnClose.TabStop = False
        '
        'btnPreviousDoc
        '
        Me.btnPreviousDoc.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnPreviousDoc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnPreviousDoc.Image = Global.TESCO_TMS.My.Resources.Resources.btnPreviousDoc
        Me.btnPreviousDoc.Location = New System.Drawing.Point(244, 519)
        Me.btnPreviousDoc.Name = "btnPreviousDoc"
        Me.btnPreviousDoc.Size = New System.Drawing.Size(140, 50)
        Me.btnPreviousDoc.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnPreviousDoc.TabIndex = 38
        Me.btnPreviousDoc.TabStop = False
        '
        'btnNextDoc
        '
        Me.btnNextDoc.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnNextDoc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNextDoc.Image = Global.TESCO_TMS.My.Resources.Resources.btnNextDoc
        Me.btnNextDoc.Location = New System.Drawing.Point(440, 519)
        Me.btnNextDoc.Name = "btnNextDoc"
        Me.btnNextDoc.Size = New System.Drawing.Size(123, 50)
        Me.btnNextDoc.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnNextDoc.TabIndex = 37
        Me.btnNextDoc.TabStop = False
        '
        'Flash
        '
        Me.Flash.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Flash.Enabled = True
        Me.Flash.Location = New System.Drawing.Point(12, 12)
        Me.Flash.Name = "Flash"
        Me.Flash.OcxState = CType(resources.GetObject("Flash.OcxState"), System.Windows.Forms.AxHost.State)
        Me.Flash.Size = New System.Drawing.Size(810, 498)
        Me.Flash.TabIndex = 39
        '
        'txtPage
        '
        Me.txtPage.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPage.BackColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(66, Byte), Integer))
        Me.txtPage.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPage.Font = New System.Drawing.Font("Microsoft Sans Serif", 1.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPage.ForeColor = System.Drawing.Color.Black
        Me.txtPage.Location = New System.Drawing.Point(772, 563)
        Me.txtPage.Name = "txtPage"
        Me.txtPage.Size = New System.Drawing.Size(40, 3)
        Me.txtPage.TabIndex = 0
        Me.txtPage.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'pbContent
        '
        Me.pbContent.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pbContent.Cursor = System.Windows.Forms.Cursors.Hand
        Me.pbContent.Image = Global.TESCO_TMS.My.Resources.Resources.index_icon
        Me.pbContent.Location = New System.Drawing.Point(595, 519)
        Me.pbContent.Name = "pbContent"
        Me.pbContent.Size = New System.Drawing.Size(104, 50)
        Me.pbContent.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbContent.TabIndex = 44
        Me.pbContent.TabStop = False
        '
        'frmDisplaySWF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(66, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(834, 578)
        Me.ControlBox = False
        Me.Controls.Add(Me.pbContent)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.txtPage)
        Me.Controls.Add(Me.Flash)
        Me.Controls.Add(Me.btnPreviousDoc)
        Me.Controls.Add(Me.btnNextDoc)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmDisplaySWF"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnPreviousDoc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnNextDoc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Flash, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbContent, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnClose As System.Windows.Forms.PictureBox
    Friend WithEvents btnPreviousDoc As System.Windows.Forms.PictureBox
    Friend WithEvents btnNextDoc As System.Windows.Forms.PictureBox
    Friend WithEvents Flash As AxShockwaveFlashObjects.AxShockwaveFlash
    Friend WithEvents txtPage As System.Windows.Forms.TextBox
    Friend WithEvents pbContent As System.Windows.Forms.PictureBox
End Class
