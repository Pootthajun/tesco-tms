﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDisplayVDO
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDisplayVDO))
        Me.VDO = New AxWMPLib.AxWindowsMediaPlayer()
        Me.txtPage = New System.Windows.Forms.TextBox()
        Me.pbContent = New System.Windows.Forms.PictureBox()
        Me.btnClose = New System.Windows.Forms.PictureBox()
        Me.btnPreviousDoc = New System.Windows.Forms.PictureBox()
        Me.btnNextDoc = New System.Windows.Forms.PictureBox()
        Me.btnPause = New System.Windows.Forms.PictureBox()
        Me.btnStop = New System.Windows.Forms.PictureBox()
        Me.btnPlay = New System.Windows.Forms.PictureBox()
        CType(Me.VDO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbContent, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnPreviousDoc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnNextDoc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnPause, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnStop, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnPlay, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'VDO
        '
        Me.VDO.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.VDO.Enabled = True
        Me.VDO.Location = New System.Drawing.Point(12, 12)
        Me.VDO.Name = "VDO"
        Me.VDO.OcxState = CType(resources.GetObject("VDO.OcxState"), System.Windows.Forms.AxHost.State)
        Me.VDO.Size = New System.Drawing.Size(869, 498)
        Me.VDO.TabIndex = 37
        '
        'txtPage
        '
        Me.txtPage.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPage.BackColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(66, Byte), Integer))
        Me.txtPage.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPage.Font = New System.Drawing.Font("Microsoft Sans Serif", 1.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPage.ForeColor = System.Drawing.Color.Black
        Me.txtPage.Location = New System.Drawing.Point(831, 548)
        Me.txtPage.Name = "txtPage"
        Me.txtPage.Size = New System.Drawing.Size(41, 3)
        Me.txtPage.TabIndex = 39
        Me.txtPage.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'pbContent
        '
        Me.pbContent.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pbContent.Cursor = System.Windows.Forms.Cursors.Hand
        Me.pbContent.Image = Global.TESCO_TMS.My.Resources.Resources.index_icon
        Me.pbContent.Location = New System.Drawing.Point(656, 516)
        Me.pbContent.Name = "pbContent"
        Me.pbContent.Size = New System.Drawing.Size(104, 50)
        Me.pbContent.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbContent.TabIndex = 40
        Me.pbContent.TabStop = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnClose.Image = Global.TESCO_TMS.My.Resources.Resources.btnCloseDoc
        Me.btnClose.Location = New System.Drawing.Point(768, 516)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(113, 50)
        Me.btnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnClose.TabIndex = 21
        Me.btnClose.TabStop = False
        '
        'btnPreviousDoc
        '
        Me.btnPreviousDoc.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnPreviousDoc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnPreviousDoc.Image = Global.TESCO_TMS.My.Resources.Resources.btnPreviousDoc
        Me.btnPreviousDoc.Location = New System.Drawing.Point(207, 519)
        Me.btnPreviousDoc.Name = "btnPreviousDoc"
        Me.btnPreviousDoc.Size = New System.Drawing.Size(126, 50)
        Me.btnPreviousDoc.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnPreviousDoc.TabIndex = 36
        Me.btnPreviousDoc.TabStop = False
        '
        'btnNextDoc
        '
        Me.btnNextDoc.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnNextDoc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNextDoc.Image = Global.TESCO_TMS.My.Resources.Resources.btnNextDoc
        Me.btnNextDoc.Location = New System.Drawing.Point(520, 519)
        Me.btnNextDoc.Name = "btnNextDoc"
        Me.btnNextDoc.Size = New System.Drawing.Size(122, 50)
        Me.btnNextDoc.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnNextDoc.TabIndex = 35
        Me.btnNextDoc.TabStop = False
        '
        'btnPause
        '
        Me.btnPause.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnPause.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnPause.Image = Global.TESCO_TMS.My.Resources.Resources.btnPause
        Me.btnPause.Location = New System.Drawing.Point(437, 519)
        Me.btnPause.Name = "btnPause"
        Me.btnPause.Size = New System.Drawing.Size(50, 50)
        Me.btnPause.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnPause.TabIndex = 20
        Me.btnPause.TabStop = False
        '
        'btnStop
        '
        Me.btnStop.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnStop.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnStop.Image = Global.TESCO_TMS.My.Resources.Resources.btnStop
        Me.btnStop.Location = New System.Drawing.Point(366, 519)
        Me.btnStop.Name = "btnStop"
        Me.btnStop.Size = New System.Drawing.Size(50, 50)
        Me.btnStop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnStop.TabIndex = 19
        Me.btnStop.TabStop = False
        '
        'btnPlay
        '
        Me.btnPlay.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnPlay.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnPlay.Image = Global.TESCO_TMS.My.Resources.Resources.btnPlay
        Me.btnPlay.Location = New System.Drawing.Point(401, 519)
        Me.btnPlay.Name = "btnPlay"
        Me.btnPlay.Size = New System.Drawing.Size(50, 50)
        Me.btnPlay.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnPlay.TabIndex = 18
        Me.btnPlay.TabStop = False
        Me.btnPlay.Visible = False
        '
        'frmDisplayVDO
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(66, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(893, 578)
        Me.ControlBox = False
        Me.Controls.Add(Me.pbContent)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.txtPage)
        Me.Controls.Add(Me.VDO)
        Me.Controls.Add(Me.btnPreviousDoc)
        Me.Controls.Add(Me.btnNextDoc)
        Me.Controls.Add(Me.btnPause)
        Me.Controls.Add(Me.btnStop)
        Me.Controls.Add(Me.btnPlay)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmDisplayVDO"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        CType(Me.VDO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbContent, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnPreviousDoc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnNextDoc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnPause, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnStop, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnPlay, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnPlay As System.Windows.Forms.PictureBox
    Friend WithEvents btnStop As System.Windows.Forms.PictureBox
    Friend WithEvents btnPause As System.Windows.Forms.PictureBox
    Friend WithEvents btnClose As System.Windows.Forms.PictureBox
    Friend WithEvents btnPreviousDoc As System.Windows.Forms.PictureBox
    Friend WithEvents btnNextDoc As System.Windows.Forms.PictureBox
    Friend WithEvents VDO As AxWMPLib.AxWindowsMediaPlayer
    Friend WithEvents txtPage As System.Windows.Forms.TextBox
    Friend WithEvents pbContent As System.Windows.Forms.PictureBox
End Class
