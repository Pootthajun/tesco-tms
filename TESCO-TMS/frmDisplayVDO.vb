﻿Imports System.IO

Public Class frmDisplayVDO

    Public CourseID As Int32
    Public DocumentFileID As Int32
    Public CurrentDocID As Int32
    Public ClassID As Long
    Public FileNo As Int32
    Public TotalFile As Int32
    Public urlFile As String
    Public PathFile As String
    Public ViewAllFile As Boolean = False
    Public FileName As String


    Private Sub frmDisplayVDO_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If IO.File.Exists(PathFile) = True Then
            Try
                IO.File.SetAttributes(PathFile, IO.FileAttributes.Normal)
                IO.File.Delete(PathFile)
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub frmDisplayVDO_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Shown
        Me.WindowState = FormWindowState.Maximized
        Me.ControlBox = False

        If FileNo = 1 Then
            btnPreviousDoc.Visible = False
        End If


        CallAPIUpdateLog(Token, myUser.ClientID, "complete", "document", "{" & Chr(34) & "class_id" & Chr(34) & ":" & ClassID & "," & Chr(34) & "document_id" & Chr(34) & ":" & CurrentDocID & "}")
        If FileNo = TotalFile Then
            btnNextDoc.Visible = False

            'เมื่อเรียนจบหลักสูตรให้บันทึก Log
            CallAPIUpdateLog(Token, myUser.ClientID, "complete", "class", "{" & Chr(34) & "class_id" & Chr(34) & ":" & ClassID & "}")
        End If


        Dim EncryptFile As String = FolderCourseDocumentFile & "\" & EnCripText(PathFile.Replace(FolderCourseDocumentFile & "\", ""))   'Encrypt เฉพาะชื่อไฟล์
        If IO.File.Exists(EncryptFile) = False Then
            Dim f As New frmProgressDownload
            f.URL = urlFile
            f.PathDownloadTo = EncryptFile
            If f.ShowDialog() = Windows.Forms.DialogResult.OK Then
                UpdateCouseDocFilePath(DocumentFileID, PathFile)
            End If
            Threading.Thread.Sleep(2000)
        Else
            Dim info As New FileInfo(EncryptFile)
            If info.Length = 0 Then
                Dim f As New frmProgressDownload
                f.URL = urlFile
                f.PathDownloadTo = EncryptFile
                If f.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    UpdateCouseDocFilePath(DocumentFileID, PathFile)
                End If
            End If
        End If

        Try
            IO.File.Copy(EncryptFile, PathFile, True)
            Threading.Thread.Sleep(2000)

            VDO.settings.setMode("loop", False)
            VDO.URL = PathFile
            VDO.uiMode = "none"
            VDO.stretchToFit = True
            VDO.Ctlcontrols.play()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        IO.File.Delete(PathFile)
        Me.Close()
    End Sub

    Private Sub btnPause_Click(sender As System.Object, e As System.EventArgs) Handles btnPause.Click
        VDO.Ctlcontrols.pause()
        btnPause.Visible = False
        btnPlay.Visible = True
        btnStop.Visible = False
        If FileNo <> 1 Then
            btnPreviousDoc.Visible = True
        End If
        If FileNo <> TotalFile Then
            btnNextDoc.Visible = True
        End If
    End Sub

    Private Sub btnPlay_Click(sender As Object, e As System.EventArgs) Handles btnPlay.Click
        VDO.Ctlcontrols.play()
        btnPause.Visible = True
        btnPlay.Visible = False
        btnStop.Visible = True
        btnPreviousDoc.Visible = False
        btnNextDoc.Visible = False
    End Sub

    Private Sub btnStop_Click(sender As Object, e As System.EventArgs) Handles btnStop.Click
        VDO.Ctlcontrols.stop()
        btnPause.Visible = False
        btnPlay.Visible = True
        btnStop.Visible = False
        If FileNo <> 1 Then
            btnPreviousDoc.Visible = True
        End If
        If FileNo <> TotalFile Then
            btnNextDoc.Visible = True
        End If
    End Sub

    Private Sub btnPreviousDoc_Click(sender As System.Object, e As System.EventArgs) Handles btnPreviousDoc.Click
        IO.File.Delete(PathFile)

        If ViewAllFile Then
            ShowFormDocumentAllFile(FileNo - 1, ClassID)
        Else
            'ShowFormDocumentFile(CourseID, DocumentFileID)
            ShowFormDocument(FileNo - 1)
        End If
        Me.Close()
    End Sub

    Private Sub btnNextDoc_Click(sender As System.Object, e As System.EventArgs) Handles btnNextDoc.Click
        IO.File.Delete(PathFile)

        If ViewAllFile Then
            ShowFormDocumentAllFile(FileNo + 1, ClassID)
        Else
            'ShowFormDocumentFile(CourseID, DocumentFileID)
            ShowFormDocument(FileNo + 1)
        End If
        Me.Close()
    End Sub

    Private Sub VDO_ErrorEvent(ByVal sender As Object, ByVal e As System.EventArgs) Handles VDO.ErrorEvent
        If VDO.Error.errorCount > 0 Then
            MessageBox.Show(VDO.Error.Item(0).errorDescription, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

        IO.File.Delete(PathFile)
        Me.Close()
        If ViewAllFile Then
            ShowFormDocumentAllFile(FileNo + 1, ClassID)
        End If
    End Sub

    Private Sub txtPage_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtPage.KeyUp
        Try
            Select Case e.KeyCode
                Case Keys.PageUp
                    If btnNextDoc.Visible = True Then
                        btnNextDoc_Click(Nothing, Nothing)
                    End If
                Case Keys.PageDown
                    If btnPreviousDoc.Visible = True Then
                        btnPreviousDoc_Click(Nothing, Nothing)
                    End If
                Case Keys.Home
                    btnClose_Click(Nothing, Nothing)
            End Select
        Catch ex As Exception

        End Try

    End Sub

    Private Sub pbContent_Click(sender As System.Object, e As System.EventArgs) Handles pbContent.Click
        If Me.OwnedForms.Length > 0 Then
            'ถ้าเปิดฟอร์ม frmShowListContent อยู่แล้ว ก็ไม่ต้องเปิดซ้ำอีก
            If Me.OwnedForms(0).Name = frmShowListContent.Name Then
                Exit Sub
            End If
        End If


        Dim sFrm As New frmShowListContent
        sFrm.CourseID = CourseID
        'sFrm.Top = 0
        'sFrm.Left = 0
        'Me.Dock = DockStyle.Left

        Application.DoEvents()
        'sFrm.TopMost = True

        sFrm.Show(Me)

    End Sub

    Private Sub VDO_PlayStateChange(sender As Object, e As AxWMPLib._WMPOCXEvents_PlayStateChangeEvent) Handles VDO.PlayStateChange
        'If e.newState.ToString = "8" Then
        '    'เมื่อไฟล์ VDO เล่นจบ ให้อัพเดท Log 

        'End If


        ''########## Video State Reference
        '0 = Undefined
        '1 = Stopped (by User)
        '2 = Paused
        '3 = Playing
        '4 = Scan Forward
        '5 = Scan Backwards
        '6 = Buffering
        '7 = Waiting
        '8 = Media Ended
        '9 = Transitioning
        '10 = Ready
        '11 = Reconnecting
        '12 = Last

    End Sub
End Class