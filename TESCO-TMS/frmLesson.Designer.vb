﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLesson
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblWelcome = New System.Windows.Forms.Label()
        Me.flp = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtTextSearch = New System.Windows.Forms.TextBox()
        Me.pbButtonSearch = New System.Windows.Forms.PictureBox()
        Me.pbTextSearch = New System.Windows.Forms.PictureBox()
        Me.lblCourse = New System.Windows.Forms.Label()
        CType(Me.pbButtonSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbTextSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblWelcome
        '
        Me.lblWelcome.AutoSize = True
        Me.lblWelcome.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!)
        Me.lblWelcome.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(22, Byte), Integer), CType(CType(137, Byte), Integer))
        Me.lblWelcome.Location = New System.Drawing.Point(30, 43)
        Me.lblWelcome.Name = "lblWelcome"
        Me.lblWelcome.Size = New System.Drawing.Size(199, 63)
        Me.lblWelcome.TabIndex = 21
        Me.lblWelcome.Text = "หลักสูตร"
        '
        'flp
        '
        Me.flp.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.flp.AutoScroll = True
        Me.flp.Location = New System.Drawing.Point(26, 137)
        Me.flp.Margin = New System.Windows.Forms.Padding(100)
        Me.flp.Name = "flp"
        Me.flp.Size = New System.Drawing.Size(1246, 456)
        Me.flp.TabIndex = 25
        '
        'txtTextSearch
        '
        Me.txtTextSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTextSearch.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtTextSearch.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtTextSearch.Location = New System.Drawing.Point(924, 90)
        Me.txtTextSearch.Name = "txtTextSearch"
        Me.txtTextSearch.Size = New System.Drawing.Size(275, 23)
        Me.txtTextSearch.TabIndex = 27
        '
        'pbButtonSearch
        '
        Me.pbButtonSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pbButtonSearch.Image = Global.TESCO_TMS.My.Resources.Resources.search_icon
        Me.pbButtonSearch.Location = New System.Drawing.Point(1226, 82)
        Me.pbButtonSearch.Name = "pbButtonSearch"
        Me.pbButtonSearch.Size = New System.Drawing.Size(44, 40)
        Me.pbButtonSearch.TabIndex = 28
        Me.pbButtonSearch.TabStop = False
        '
        'pbTextSearch
        '
        Me.pbTextSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pbTextSearch.Image = Global.TESCO_TMS.My.Resources.Resources.search_bar
        Me.pbTextSearch.Location = New System.Drawing.Point(900, 82)
        Me.pbTextSearch.Name = "pbTextSearch"
        Me.pbTextSearch.Size = New System.Drawing.Size(320, 40)
        Me.pbTextSearch.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbTextSearch.TabIndex = 26
        Me.pbTextSearch.TabStop = False
        '
        'lblCourse
        '
        Me.lblCourse.AutoSize = True
        Me.lblCourse.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCourse.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(22, Byte), Integer), CType(CType(137, Byte), Integer))
        Me.lblCourse.Location = New System.Drawing.Point(229, 55)
        Me.lblCourse.Name = "lblCourse"
        Me.lblCourse.Size = New System.Drawing.Size(0, 46)
        Me.lblCourse.TabIndex = 21
        '
        'frmLesson
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1288, 612)
        Me.ControlBox = False
        Me.Controls.Add(Me.pbButtonSearch)
        Me.Controls.Add(Me.txtTextSearch)
        Me.Controls.Add(Me.pbTextSearch)
        Me.Controls.Add(Me.flp)
        Me.Controls.Add(Me.lblCourse)
        Me.Controls.Add(Me.lblWelcome)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmLesson"
        Me.ShowInTaskbar = False
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.pbButtonSearch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbTextSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblWelcome As System.Windows.Forms.Label
    Friend WithEvents flp As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents pbTextSearch As System.Windows.Forms.PictureBox
    Friend WithEvents txtTextSearch As System.Windows.Forms.TextBox
    Friend WithEvents pbButtonSearch As System.Windows.Forms.PictureBox
    Friend WithEvents lblCourse As System.Windows.Forms.Label

End Class
