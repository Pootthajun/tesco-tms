﻿Imports System.Data
Imports System.Data.OleDb

Public Class frmLesson

    Private Sub frmLesson_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Me.WindowState = FormWindowState.Maximized
        Me.ControlBox = False

        SearchLesson()

    End Sub

    Private Sub pbButtonSearch_Click(sender As System.Object, e As System.EventArgs) Handles pbButtonSearch.Click
        SearchLesson()
    End Sub

    Private Sub SearchLesson()
        Dim sql As String = "select id, title, description, icon_file "
        sql += " from tb_course "
        sql += " where 1=1 "
        If txtTextSearch.Text.Trim <> "" Then
            sql += "and (title like '%" & txtTextSearch.Text.Trim & "%' or description like '%" & txtTextSearch.Text.Trim & "%')"
        End If
        sql += " order by sort"
        Dim dt As DataTable = GetSqlDataTable(sql)
        If dt.Rows.Count > 0 Then
            flp.Controls.Clear()

            Dim TotHeight As Integer = 0

            For i As Int32 = 0 To dt.Rows.Count - 1
                Dim Form As New CourseBox
                lblCourse.Text = lblCourse.Text.Replace(" && ", "/")
                Form.CourseName = lblCourse.Text + " > " + (dt.Rows(i).Item("title").ToString)
                Form.CourseID = dt.Rows(i).Item("id").ToString
                Form.Display(dt.Rows(i).Item("title").ToString, dt.Rows(i).Item("description").ToString, dt.Rows(i).Item("icon_file").ToString)
                flp.Controls.Add(Form)
                If i Mod 2 = 0 Then
                    TotHeight += Form.Height
                End If

            Next
        End If
        dt.Dispose()
    End Sub

    Private Sub txtTextSearch_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtTextSearch.KeyPress
        If e.KeyChar = Chr(13) Then
            SearchLesson()
        End If
    End Sub
End Class