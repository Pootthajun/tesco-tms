﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLessonDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLessonDialog))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnLearn02 = New System.Windows.Forms.PictureBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.imgIcon = New System.Windows.Forms.PictureBox()
        Me.TC = New System.Windows.Forms.TabControl()
        Me.TP1 = New System.Windows.Forms.TabPage()
        Me.pb1 = New System.Windows.Forms.ProgressBar()
        Me.btnCreatePasscode = New System.Windows.Forms.Label()
        Me.flpUser = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.txtID = New System.Windows.Forms.TextBox()
        Me.btnCheckUser = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblTitle1 = New System.Windows.Forms.Label()
        Me.lblHeadStudent = New System.Windows.Forms.Label()
        Me.btnStudy = New System.Windows.Forms.PictureBox()
        Me.TP2 = New System.Windows.Forms.TabPage()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.lblTitle2 = New System.Windows.Forms.Label()
        Me.flp = New System.Windows.Forms.FlowLayoutPanel()
        Me.Line = New System.Windows.Forms.PictureBox()
        Me.imgCover = New System.Windows.Forms.PictureBox()
        Me.TP3 = New System.Windows.Forms.TabPage()
        Me.flpStudy = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblTitle3 = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.TP_Filter1 = New System.Windows.Forms.TabPage()
        Me.txtSearchFilter1 = New System.Windows.Forms.TextBox()
        Me.btnSearchFilter1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.lblCourseFilter1 = New System.Windows.Forms.Label()
        Me.lbl = New System.Windows.Forms.Label()
        Me.lblTitleFilter1 = New System.Windows.Forms.Label()
        Me.flpFilter1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.TP_Filter2 = New System.Windows.Forms.TabPage()
        Me.txtSearchFilter2 = New System.Windows.Forms.TextBox()
        Me.btnSearchFilter2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.lblCourseFilter2 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblTitleFilter2 = New System.Windows.Forms.Label()
        Me.flpFilter2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnDetail02 = New System.Windows.Forms.PictureBox()
        Me.btnDetail01 = New System.Windows.Forms.PictureBox()
        Me.btnStudent01 = New System.Windows.Forms.PictureBox()
        Me.btnStudent02 = New System.Windows.Forms.PictureBox()
        Me.btnLearn01 = New System.Windows.Forms.PictureBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnClose = New System.Windows.Forms.PictureBox()
        Me.Panel1.SuspendLayout()
        CType(Me.btnLearn02, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        CType(Me.imgIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TC.SuspendLayout()
        Me.TP1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.btnCheckUser, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnStudy, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TP2.SuspendLayout()
        CType(Me.Line, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgCover, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TP3.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TP_Filter1.SuspendLayout()
        CType(Me.btnSearchFilter1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TP_Filter2.SuspendLayout()
        CType(Me.btnSearchFilter2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnDetail02, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnDetail01, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnStudent01, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnStudent02, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnLearn01, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.btnLearn02)
        Me.Panel1.Controls.Add(Me.Panel4)
        Me.Panel1.Controls.Add(Me.TC)
        Me.Panel1.Controls.Add(Me.btnDetail02)
        Me.Panel1.Controls.Add(Me.btnDetail01)
        Me.Panel1.Controls.Add(Me.btnStudent01)
        Me.Panel1.Controls.Add(Me.btnStudent02)
        Me.Panel1.Controls.Add(Me.btnLearn01)
        Me.Panel1.Location = New System.Drawing.Point(4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(801, 688)
        Me.Panel1.TabIndex = 2
        '
        'btnLearn02
        '
        Me.btnLearn02.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnLearn02.Image = CType(resources.GetObject("btnLearn02.Image"), System.Drawing.Image)
        Me.btnLearn02.Location = New System.Drawing.Point(29, 476)
        Me.btnLearn02.Name = "btnLearn02"
        Me.btnLearn02.Size = New System.Drawing.Size(70, 90)
        Me.btnLearn02.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnLearn02.TabIndex = 24
        Me.btnLearn02.TabStop = False
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.FromArgb(CType(CType(147, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(31, Byte), Integer))
        Me.Panel4.Controls.Add(Me.imgIcon)
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(128, 238)
        Me.Panel4.TabIndex = 23
        '
        'imgIcon
        '
        Me.imgIcon.BackColor = System.Drawing.Color.FromArgb(CType(CType(147, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(31, Byte), Integer))
        Me.imgIcon.Image = CType(resources.GetObject("imgIcon.Image"), System.Drawing.Image)
        Me.imgIcon.Location = New System.Drawing.Point(14, 12)
        Me.imgIcon.Name = "imgIcon"
        Me.imgIcon.Size = New System.Drawing.Size(100, 100)
        Me.imgIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgIcon.TabIndex = 16
        Me.imgIcon.TabStop = False
        '
        'TC
        '
        Me.TC.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TC.Controls.Add(Me.TP1)
        Me.TC.Controls.Add(Me.TP2)
        Me.TC.Controls.Add(Me.TP3)
        Me.TC.Controls.Add(Me.TP_Filter1)
        Me.TC.Controls.Add(Me.TP_Filter2)
        Me.TC.Location = New System.Drawing.Point(128, -23)
        Me.TC.Name = "TC"
        Me.TC.SelectedIndex = 0
        Me.TC.Size = New System.Drawing.Size(678, 715)
        Me.TC.TabIndex = 0
        '
        'TP1
        '
        Me.TP1.BackColor = System.Drawing.Color.White
        Me.TP1.Controls.Add(Me.pb1)
        Me.TP1.Controls.Add(Me.btnCreatePasscode)
        Me.TP1.Controls.Add(Me.flpUser)
        Me.TP1.Controls.Add(Me.Panel3)
        Me.TP1.Controls.Add(Me.lblTitle1)
        Me.TP1.Controls.Add(Me.lblHeadStudent)
        Me.TP1.Controls.Add(Me.btnStudy)
        Me.TP1.Location = New System.Drawing.Point(4, 22)
        Me.TP1.Name = "TP1"
        Me.TP1.Padding = New System.Windows.Forms.Padding(3)
        Me.TP1.Size = New System.Drawing.Size(670, 689)
        Me.TP1.TabIndex = 0
        Me.TP1.Text = "TabPage1"
        '
        'pb1
        '
        Me.pb1.Location = New System.Drawing.Point(14, 642)
        Me.pb1.Name = "pb1"
        Me.pb1.Size = New System.Drawing.Size(212, 23)
        Me.pb1.TabIndex = 30
        Me.pb1.Visible = False
        '
        'btnCreatePasscode
        '
        Me.btnCreatePasscode.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCreatePasscode.AutoSize = True
        Me.btnCreatePasscode.BackColor = System.Drawing.Color.Transparent
        Me.btnCreatePasscode.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnCreatePasscode.Font = New System.Drawing.Font("Tahoma", 14.0!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnCreatePasscode.ForeColor = System.Drawing.Color.DimGray
        Me.btnCreatePasscode.Location = New System.Drawing.Point(500, 642)
        Me.btnCreatePasscode.Name = "btnCreatePasscode"
        Me.btnCreatePasscode.Size = New System.Drawing.Size(130, 23)
        Me.btnCreatePasscode.TabIndex = 29
        Me.btnCreatePasscode.Text = "สร้าง Passcode"
        '
        'flpUser
        '
        Me.flpUser.AutoScroll = True
        Me.flpUser.Location = New System.Drawing.Point(14, 182)
        Me.flpUser.Name = "flpUser"
        Me.flpUser.Size = New System.Drawing.Size(643, 435)
        Me.flpUser.TabIndex = 27
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(147, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(31, Byte), Integer))
        Me.Panel3.Controls.Add(Me.txtID)
        Me.Panel3.Controls.Add(Me.btnCheckUser)
        Me.Panel3.Controls.Add(Me.PictureBox1)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Location = New System.Drawing.Point(14, 85)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(643, 53)
        Me.Panel3.TabIndex = 26
        '
        'txtID
        '
        Me.txtID.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtID.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtID.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtID.ForeColor = System.Drawing.Color.Gray
        Me.txtID.Location = New System.Drawing.Point(358, 15)
        Me.txtID.MaxLength = 20
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(191, 23)
        Me.txtID.TabIndex = 27
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnCheckUser
        '
        Me.btnCheckUser.BackColor = System.Drawing.Color.Transparent
        Me.btnCheckUser.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnCheckUser.Image = Global.TESCO_TMS.My.Resources.Resources.cb_head
        Me.btnCheckUser.Location = New System.Drawing.Point(568, 7)
        Me.btnCheckUser.Name = "btnCheckUser"
        Me.btnCheckUser.Size = New System.Drawing.Size(45, 40)
        Me.btnCheckUser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnCheckUser.TabIndex = 27
        Me.btnCheckUser.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(147, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(31, Byte), Integer))
        Me.PictureBox1.Image = Global.TESCO_TMS.My.Resources.Resources.Textbox1
        Me.PictureBox1.Location = New System.Drawing.Point(346, 7)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(216, 40)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 17
        Me.PictureBox1.TabStop = False
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(6, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(287, 24)
        Me.Label2.TabIndex = 26
        Me.Label2.Text = "เพิ่มผู้เรียน ใส่รหัสพนักงาน 8 หลัก"
        '
        'lblTitle1
        '
        Me.lblTitle1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTitle1.AutoSize = True
        Me.lblTitle1.BackColor = System.Drawing.Color.White
        Me.lblTitle1.Font = New System.Drawing.Font("Tahoma", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblTitle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(147, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(31, Byte), Integer))
        Me.lblTitle1.Location = New System.Drawing.Point(18, 13)
        Me.lblTitle1.Name = "lblTitle1"
        Me.lblTitle1.Size = New System.Drawing.Size(186, 33)
        Me.lblTitle1.TabIndex = 24
        Me.lblTitle1.Text = "เครื่องแคชเชียร์"
        '
        'lblHeadStudent
        '
        Me.lblHeadStudent.AutoSize = True
        Me.lblHeadStudent.BackColor = System.Drawing.Color.White
        Me.lblHeadStudent.Font = New System.Drawing.Font("Tahoma", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblHeadStudent.ForeColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(119, Byte), Integer))
        Me.lblHeadStudent.Location = New System.Drawing.Point(290, 150)
        Me.lblHeadStudent.Name = "lblHeadStudent"
        Me.lblHeadStudent.Size = New System.Drawing.Size(80, 29)
        Me.lblHeadStudent.TabIndex = 25
        Me.lblHeadStudent.Text = "เช็คชื่อ"
        '
        'btnStudy
        '
        Me.btnStudy.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnStudy.Image = CType(resources.GetObject("btnStudy.Image"), System.Drawing.Image)
        Me.btnStudy.Location = New System.Drawing.Point(238, 627)
        Me.btnStudy.Name = "btnStudy"
        Me.btnStudy.Size = New System.Drawing.Size(194, 51)
        Me.btnStudy.TabIndex = 20
        Me.btnStudy.TabStop = False
        '
        'TP2
        '
        Me.TP2.Controls.Add(Me.Label1)
        Me.TP2.Controls.Add(Me.txtDescription)
        Me.TP2.Controls.Add(Me.lblTitle2)
        Me.TP2.Controls.Add(Me.flp)
        Me.TP2.Controls.Add(Me.Line)
        Me.TP2.Controls.Add(Me.imgCover)
        Me.TP2.Location = New System.Drawing.Point(4, 22)
        Me.TP2.Name = "TP2"
        Me.TP2.Padding = New System.Windows.Forms.Padding(3)
        Me.TP2.Size = New System.Drawing.Size(670, 689)
        Me.TP2.TabIndex = 1
        Me.TP2.Text = "TabPage2"
        Me.TP2.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(218, 262)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(193, 23)
        Me.Label1.TabIndex = 36
        Me.Label1.Text = "เอกสารประกอบบทเรียน"
        '
        'txtDescription
        '
        Me.txtDescription.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.ForeColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(66, Byte), Integer))
        Me.txtDescription.Location = New System.Drawing.Point(271, 80)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(378, 136)
        Me.txtDescription.TabIndex = 35
        Me.txtDescription.TabStop = False
        Me.txtDescription.Text = "Descrition"
        '
        'lblTitle2
        '
        Me.lblTitle2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTitle2.AutoSize = True
        Me.lblTitle2.BackColor = System.Drawing.Color.White
        Me.lblTitle2.Font = New System.Drawing.Font("Tahoma", 20.0!)
        Me.lblTitle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(119, Byte), Integer))
        Me.lblTitle2.Location = New System.Drawing.Point(40, 13)
        Me.lblTitle2.Name = "lblTitle2"
        Me.lblTitle2.Size = New System.Drawing.Size(186, 33)
        Me.lblTitle2.TabIndex = 34
        Me.lblTitle2.Text = "เครื่องแคชเชียร์"
        '
        'flp
        '
        Me.flp.AutoScroll = True
        Me.flp.Location = New System.Drawing.Point(8, 299)
        Me.flp.Name = "flp"
        Me.flp.Size = New System.Drawing.Size(656, 388)
        Me.flp.TabIndex = 32
        '
        'Line
        '
        Me.Line.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Line.BackColor = System.Drawing.Color.White
        Me.Line.Image = Global.TESCO_TMS.My.Resources.Resources.Line
        Me.Line.Location = New System.Drawing.Point(8, 65)
        Me.Line.Name = "Line"
        Me.Line.Size = New System.Drawing.Size(654, 1)
        Me.Line.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Line.TabIndex = 33
        Me.Line.TabStop = False
        '
        'imgCover
        '
        Me.imgCover.Location = New System.Drawing.Point(8, 71)
        Me.imgCover.Name = "imgCover"
        Me.imgCover.Size = New System.Drawing.Size(250, 170)
        Me.imgCover.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgCover.TabIndex = 30
        Me.imgCover.TabStop = False
        '
        'TP3
        '
        Me.TP3.Controls.Add(Me.flpStudy)
        Me.TP3.Controls.Add(Me.Label3)
        Me.TP3.Controls.Add(Me.lblTitle3)
        Me.TP3.Controls.Add(Me.PictureBox2)
        Me.TP3.Location = New System.Drawing.Point(4, 22)
        Me.TP3.Name = "TP3"
        Me.TP3.Padding = New System.Windows.Forms.Padding(3)
        Me.TP3.Size = New System.Drawing.Size(670, 689)
        Me.TP3.TabIndex = 2
        Me.TP3.Text = "TabPage1"
        Me.TP3.UseVisualStyleBackColor = True
        '
        'flpStudy
        '
        Me.flpStudy.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.flpStudy.AutoScroll = True
        Me.flpStudy.Location = New System.Drawing.Point(8, 121)
        Me.flpStudy.Margin = New System.Windows.Forms.Padding(100)
        Me.flpStudy.Name = "flpStudy"
        Me.flpStudy.Size = New System.Drawing.Size(654, 561)
        Me.flpStudy.TabIndex = 38
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.White
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(119, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(293, 81)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(89, 29)
        Me.Label3.TabIndex = 36
        Me.Label3.Text = "เรียนต่อ"
        '
        'lblTitle3
        '
        Me.lblTitle3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTitle3.AutoSize = True
        Me.lblTitle3.BackColor = System.Drawing.Color.White
        Me.lblTitle3.Font = New System.Drawing.Font("Tahoma", 20.0!)
        Me.lblTitle3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(119, Byte), Integer))
        Me.lblTitle3.Location = New System.Drawing.Point(40, 13)
        Me.lblTitle3.Name = "lblTitle3"
        Me.lblTitle3.Size = New System.Drawing.Size(186, 33)
        Me.lblTitle3.TabIndex = 35
        Me.lblTitle3.Text = "เครื่องแคชเชียร์"
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.BackColor = System.Drawing.Color.White
        Me.PictureBox2.Image = Global.TESCO_TMS.My.Resources.Resources.Line
        Me.PictureBox2.Location = New System.Drawing.Point(8, 65)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(654, 1)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 37
        Me.PictureBox2.TabStop = False
        '
        'TP_Filter1
        '
        Me.TP_Filter1.Controls.Add(Me.txtSearchFilter1)
        Me.TP_Filter1.Controls.Add(Me.btnSearchFilter1)
        Me.TP_Filter1.Controls.Add(Me.PictureBox4)
        Me.TP_Filter1.Controls.Add(Me.lblCourseFilter1)
        Me.TP_Filter1.Controls.Add(Me.lbl)
        Me.TP_Filter1.Controls.Add(Me.lblTitleFilter1)
        Me.TP_Filter1.Controls.Add(Me.flpFilter1)
        Me.TP_Filter1.Location = New System.Drawing.Point(4, 22)
        Me.TP_Filter1.Name = "TP_Filter1"
        Me.TP_Filter1.Padding = New System.Windows.Forms.Padding(3)
        Me.TP_Filter1.Size = New System.Drawing.Size(670, 689)
        Me.TP_Filter1.TabIndex = 3
        Me.TP_Filter1.Text = "TabPage1"
        Me.TP_Filter1.UseVisualStyleBackColor = True
        '
        'txtSearchFilter1
        '
        Me.txtSearchFilter1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtSearchFilter1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSearchFilter1.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtSearchFilter1.ForeColor = System.Drawing.Color.Gray
        Me.txtSearchFilter1.Location = New System.Drawing.Point(383, 37)
        Me.txtSearchFilter1.MaxLength = 20
        Me.txtSearchFilter1.Name = "txtSearchFilter1"
        Me.txtSearchFilter1.Size = New System.Drawing.Size(191, 23)
        Me.txtSearchFilter1.TabIndex = 27
        Me.txtSearchFilter1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnSearchFilter1
        '
        Me.btnSearchFilter1.BackColor = System.Drawing.Color.Transparent
        Me.btnSearchFilter1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSearchFilter1.Image = Global.TESCO_TMS.My.Resources.Resources.cb_head
        Me.btnSearchFilter1.Location = New System.Drawing.Point(593, 29)
        Me.btnSearchFilter1.Name = "btnSearchFilter1"
        Me.btnSearchFilter1.Size = New System.Drawing.Size(45, 40)
        Me.btnSearchFilter1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnSearchFilter1.TabIndex = 27
        Me.btnSearchFilter1.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.BackColor = System.Drawing.SystemColors.Window
        Me.PictureBox4.Image = Global.TESCO_TMS.My.Resources.Resources.Textbox1
        Me.PictureBox4.Location = New System.Drawing.Point(371, 29)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(216, 40)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox4.TabIndex = 17
        Me.PictureBox4.TabStop = False
        '
        'lblCourseFilter1
        '
        Me.lblCourseFilter1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCourseFilter1.AutoSize = True
        Me.lblCourseFilter1.BackColor = System.Drawing.Color.White
        Me.lblCourseFilter1.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCourseFilter1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblCourseFilter1.Location = New System.Drawing.Point(106, 92)
        Me.lblCourseFilter1.Name = "lblCourseFilter1"
        Me.lblCourseFilter1.Size = New System.Drawing.Size(76, 25)
        Me.lblCourseFilter1.TabIndex = 28
        Me.lblCourseFilter1.Text = "Course"
        '
        'lbl
        '
        Me.lbl.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl.AutoSize = True
        Me.lbl.BackColor = System.Drawing.Color.White
        Me.lbl.Font = New System.Drawing.Font("Tahoma", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lbl.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lbl.Location = New System.Drawing.Point(9, 89)
        Me.lbl.Name = "lbl"
        Me.lbl.Size = New System.Drawing.Size(98, 29)
        Me.lbl.TabIndex = 28
        Me.lbl.Text = "หลักสูตร"
        Me.lbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTitleFilter1
        '
        Me.lblTitleFilter1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTitleFilter1.AutoSize = True
        Me.lblTitleFilter1.BackColor = System.Drawing.Color.White
        Me.lblTitleFilter1.Font = New System.Drawing.Font("Tahoma", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblTitleFilter1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblTitleFilter1.Location = New System.Drawing.Point(8, 29)
        Me.lblTitleFilter1.Name = "lblTitleFilter1"
        Me.lblTitleFilter1.Size = New System.Drawing.Size(186, 33)
        Me.lblTitleFilter1.TabIndex = 28
        Me.lblTitleFilter1.Text = "เครื่องแคชเชียร์"
        Me.lblTitleFilter1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'flpFilter1
        '
        Me.flpFilter1.AutoScroll = True
        Me.flpFilter1.Location = New System.Drawing.Point(14, 129)
        Me.flpFilter1.Name = "flpFilter1"
        Me.flpFilter1.Size = New System.Drawing.Size(643, 554)
        Me.flpFilter1.TabIndex = 31
        '
        'TP_Filter2
        '
        Me.TP_Filter2.Controls.Add(Me.txtSearchFilter2)
        Me.TP_Filter2.Controls.Add(Me.btnSearchFilter2)
        Me.TP_Filter2.Controls.Add(Me.PictureBox5)
        Me.TP_Filter2.Controls.Add(Me.lblCourseFilter2)
        Me.TP_Filter2.Controls.Add(Me.Label5)
        Me.TP_Filter2.Controls.Add(Me.lblTitleFilter2)
        Me.TP_Filter2.Controls.Add(Me.flpFilter2)
        Me.TP_Filter2.Location = New System.Drawing.Point(4, 22)
        Me.TP_Filter2.Name = "TP_Filter2"
        Me.TP_Filter2.Padding = New System.Windows.Forms.Padding(3)
        Me.TP_Filter2.Size = New System.Drawing.Size(670, 689)
        Me.TP_Filter2.TabIndex = 4
        Me.TP_Filter2.Text = "TabPage1"
        Me.TP_Filter2.UseVisualStyleBackColor = True
        '
        'txtSearchFilter2
        '
        Me.txtSearchFilter2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtSearchFilter2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSearchFilter2.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtSearchFilter2.ForeColor = System.Drawing.Color.Gray
        Me.txtSearchFilter2.Location = New System.Drawing.Point(383, 38)
        Me.txtSearchFilter2.MaxLength = 20
        Me.txtSearchFilter2.Name = "txtSearchFilter2"
        Me.txtSearchFilter2.Size = New System.Drawing.Size(191, 23)
        Me.txtSearchFilter2.TabIndex = 34
        Me.txtSearchFilter2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnSearchFilter2
        '
        Me.btnSearchFilter2.BackColor = System.Drawing.Color.Transparent
        Me.btnSearchFilter2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSearchFilter2.Image = Global.TESCO_TMS.My.Resources.Resources.cb_head
        Me.btnSearchFilter2.Location = New System.Drawing.Point(593, 30)
        Me.btnSearchFilter2.Name = "btnSearchFilter2"
        Me.btnSearchFilter2.Size = New System.Drawing.Size(45, 40)
        Me.btnSearchFilter2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnSearchFilter2.TabIndex = 33
        Me.btnSearchFilter2.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.BackColor = System.Drawing.SystemColors.Window
        Me.PictureBox5.Image = Global.TESCO_TMS.My.Resources.Resources.Textbox1
        Me.PictureBox5.Location = New System.Drawing.Point(371, 30)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(216, 40)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox5.TabIndex = 32
        Me.PictureBox5.TabStop = False
        '
        'lblCourseFilter2
        '
        Me.lblCourseFilter2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCourseFilter2.AutoSize = True
        Me.lblCourseFilter2.BackColor = System.Drawing.Color.White
        Me.lblCourseFilter2.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCourseFilter2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblCourseFilter2.Location = New System.Drawing.Point(109, 93)
        Me.lblCourseFilter2.Name = "lblCourseFilter2"
        Me.lblCourseFilter2.Size = New System.Drawing.Size(76, 25)
        Me.lblCourseFilter2.TabIndex = 37
        Me.lblCourseFilter2.Text = "Course"
        '
        'Label5
        '
        Me.Label5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.White
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(12, 90)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(98, 29)
        Me.Label5.TabIndex = 36
        Me.Label5.Text = "หลักสูตร"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTitleFilter2
        '
        Me.lblTitleFilter2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTitleFilter2.AutoSize = True
        Me.lblTitleFilter2.BackColor = System.Drawing.Color.White
        Me.lblTitleFilter2.Font = New System.Drawing.Font("Tahoma", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblTitleFilter2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblTitleFilter2.Location = New System.Drawing.Point(11, 29)
        Me.lblTitleFilter2.Name = "lblTitleFilter2"
        Me.lblTitleFilter2.Size = New System.Drawing.Size(186, 33)
        Me.lblTitleFilter2.TabIndex = 35
        Me.lblTitleFilter2.Text = "เครื่องแคชเชียร์"
        Me.lblTitleFilter2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'flpFilter2
        '
        Me.flpFilter2.AutoScroll = True
        Me.flpFilter2.Location = New System.Drawing.Point(17, 127)
        Me.flpFilter2.Name = "flpFilter2"
        Me.flpFilter2.Size = New System.Drawing.Size(643, 556)
        Me.flpFilter2.TabIndex = 38
        '
        'btnDetail02
        '
        Me.btnDetail02.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDetail02.Image = CType(resources.GetObject("btnDetail02.Image"), System.Drawing.Image)
        Me.btnDetail02.Location = New System.Drawing.Point(29, 364)
        Me.btnDetail02.Name = "btnDetail02"
        Me.btnDetail02.Size = New System.Drawing.Size(70, 90)
        Me.btnDetail02.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnDetail02.TabIndex = 14
        Me.btnDetail02.TabStop = False
        '
        'btnDetail01
        '
        Me.btnDetail01.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDetail01.Image = CType(resources.GetObject("btnDetail01.Image"), System.Drawing.Image)
        Me.btnDetail01.Location = New System.Drawing.Point(29, 364)
        Me.btnDetail01.Name = "btnDetail01"
        Me.btnDetail01.Size = New System.Drawing.Size(70, 90)
        Me.btnDetail01.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnDetail01.TabIndex = 16
        Me.btnDetail01.TabStop = False
        Me.btnDetail01.Visible = False
        '
        'btnStudent01
        '
        Me.btnStudent01.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnStudent01.Image = CType(resources.GetObject("btnStudent01.Image"), System.Drawing.Image)
        Me.btnStudent01.Location = New System.Drawing.Point(30, 252)
        Me.btnStudent01.Name = "btnStudent01"
        Me.btnStudent01.Size = New System.Drawing.Size(70, 90)
        Me.btnStudent01.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnStudent01.TabIndex = 13
        Me.btnStudent01.TabStop = False
        '
        'btnStudent02
        '
        Me.btnStudent02.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnStudent02.Image = CType(resources.GetObject("btnStudent02.Image"), System.Drawing.Image)
        Me.btnStudent02.Location = New System.Drawing.Point(29, 252)
        Me.btnStudent02.Name = "btnStudent02"
        Me.btnStudent02.Size = New System.Drawing.Size(70, 90)
        Me.btnStudent02.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnStudent02.TabIndex = 16
        Me.btnStudent02.TabStop = False
        Me.btnStudent02.Visible = False
        '
        'btnLearn01
        '
        Me.btnLearn01.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnLearn01.Image = Global.TESCO_TMS.My.Resources.Resources.btnLernEnable
        Me.btnLearn01.Location = New System.Drawing.Point(29, 476)
        Me.btnLearn01.Name = "btnLearn01"
        Me.btnLearn01.Size = New System.Drawing.Size(70, 90)
        Me.btnLearn01.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnLearn01.TabIndex = 25
        Me.btnLearn01.TabStop = False
        Me.btnLearn01.Visible = False
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.SystemColors.ControlDark
        Me.Panel2.Controls.Add(Me.Panel1)
        Me.Panel2.Location = New System.Drawing.Point(12, 12)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(809, 696)
        Me.Panel2.TabIndex = 3
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.Transparent
        Me.btnClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.Location = New System.Drawing.Point(798, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(35, 33)
        Me.btnClose.TabIndex = 6
        Me.btnClose.TabStop = False
        '
        'frmLessonDialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(833, 720)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.Panel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmLessonDialog"
        Me.ShowInTaskbar = False
        Me.TransparencyKey = System.Drawing.Color.FromArgb(CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.Panel1.ResumeLayout(False)
        CType(Me.btnLearn02, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        CType(Me.imgIcon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TC.ResumeLayout(False)
        Me.TP1.ResumeLayout(False)
        Me.TP1.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.btnCheckUser, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnStudy, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TP2.ResumeLayout(False)
        Me.TP2.PerformLayout()
        CType(Me.Line, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgCover, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TP3.ResumeLayout(False)
        Me.TP3.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TP_Filter1.ResumeLayout(False)
        Me.TP_Filter1.PerformLayout()
        CType(Me.btnSearchFilter1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TP_Filter2.ResumeLayout(False)
        Me.TP_Filter2.PerformLayout()
        CType(Me.btnSearchFilter2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnDetail02, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnDetail01, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnStudent01, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnStudent02, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnLearn01, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnClose As System.Windows.Forms.PictureBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents TC As System.Windows.Forms.TabControl
    Friend WithEvents TP1 As System.Windows.Forms.TabPage
    Friend WithEvents TP2 As System.Windows.Forms.TabPage
    Friend WithEvents btnStudent01 As System.Windows.Forms.PictureBox
    Friend WithEvents btnDetail02 As System.Windows.Forms.PictureBox
    Friend WithEvents btnStudent02 As System.Windows.Forms.PictureBox
    Friend WithEvents btnDetail01 As System.Windows.Forms.PictureBox
    Friend WithEvents btnStudy As System.Windows.Forms.PictureBox
    Friend WithEvents imgIcon As System.Windows.Forms.PictureBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents lblTitle1 As System.Windows.Forms.Label
    Friend WithEvents lblHeadStudent As System.Windows.Forms.Label
    Friend WithEvents imgCover As System.Windows.Forms.PictureBox
    Friend WithEvents flp As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Line As System.Windows.Forms.PictureBox
    Friend WithEvents lblTitle2 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents btnCheckUser As System.Windows.Forms.PictureBox
    Friend WithEvents txtID As System.Windows.Forms.TextBox
    Friend WithEvents flpUser As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnLearn02 As System.Windows.Forms.PictureBox
    Friend WithEvents TP3 As System.Windows.Forms.TabPage
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblTitle3 As System.Windows.Forms.Label
    Friend WithEvents btnLearn01 As System.Windows.Forms.PictureBox
    Friend WithEvents flpStudy As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnCreatePasscode As System.Windows.Forms.Label
    Friend WithEvents pb1 As System.Windows.Forms.ProgressBar
    Friend WithEvents TP_Filter1 As System.Windows.Forms.TabPage
    Friend WithEvents txtSearchFilter1 As System.Windows.Forms.TextBox
    Friend WithEvents btnSearchFilter1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents lblTitleFilter1 As System.Windows.Forms.Label
    Friend WithEvents flpFilter1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lbl As System.Windows.Forms.Label
    Friend WithEvents lblCourseFilter1 As System.Windows.Forms.Label
    Friend WithEvents TP_Filter2 As System.Windows.Forms.TabPage
    Friend WithEvents txtSearchFilter2 As System.Windows.Forms.TextBox
    Friend WithEvents btnSearchFilter2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents lblCourseFilter2 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblTitleFilter2 As System.Windows.Forms.Label
    Friend WithEvents flpFilter2 As System.Windows.Forms.FlowLayoutPanel
End Class
