﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLogin
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtUser = New System.Windows.Forms.TextBox()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.txtPassCode = New System.Windows.Forms.TextBox()
        Me.lblLogin1 = New System.Windows.Forms.Label()
        Me.lblLogin2 = New System.Windows.Forms.Label()
        Me.lblLogin3 = New System.Windows.Forms.Label()
        Me.lblLogin4 = New System.Windows.Forms.Label()
        Me.btnClose = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.btnLogin = New System.Windows.Forms.PictureBox()
        Me.btnPassCode = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.imgLogin = New System.Windows.Forms.PictureBox()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnLogin, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnPassCode, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgLogin, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtUser
        '
        Me.txtUser.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtUser.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtUser.ForeColor = System.Drawing.Color.Gray
        Me.txtUser.Location = New System.Drawing.Point(189, 316)
        Me.txtUser.MaxLength = 20
        Me.txtUser.Name = "txtUser"
        Me.txtUser.Size = New System.Drawing.Size(238, 23)
        Me.txtUser.TabIndex = 1
        Me.txtUser.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPassword
        '
        Me.txtPassword.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPassword.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPassword.ForeColor = System.Drawing.Color.Gray
        Me.txtPassword.Location = New System.Drawing.Point(189, 402)
        Me.txtPassword.MaxLength = 20
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.Size = New System.Drawing.Size(238, 23)
        Me.txtPassword.TabIndex = 2
        Me.txtPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPassCode
        '
        Me.txtPassCode.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtPassCode.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPassCode.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPassCode.ForeColor = System.Drawing.Color.Gray
        Me.txtPassCode.Location = New System.Drawing.Point(530, 316)
        Me.txtPassCode.MaxLength = 20
        Me.txtPassCode.Name = "txtPassCode"
        Me.txtPassCode.Size = New System.Drawing.Size(238, 23)
        Me.txtPassCode.TabIndex = 3
        Me.txtPassCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtPassCode.Visible = False
        '
        'lblLogin1
        '
        Me.lblLogin1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblLogin1.BackColor = System.Drawing.Color.White
        Me.lblLogin1.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLogin1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(113, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(118, Byte), Integer))
        Me.lblLogin1.Location = New System.Drawing.Point(182, 273)
        Me.lblLogin1.Name = "lblLogin1"
        Me.lblLogin1.Size = New System.Drawing.Size(248, 26)
        Me.lblLogin1.TabIndex = 22
        Me.lblLogin1.Text = "USER LOGIN"
        Me.lblLogin1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLogin2
        '
        Me.lblLogin2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblLogin2.BackColor = System.Drawing.Color.White
        Me.lblLogin2.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLogin2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(113, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(118, Byte), Integer))
        Me.lblLogin2.Location = New System.Drawing.Point(523, 273)
        Me.lblLogin2.Name = "lblLogin2"
        Me.lblLogin2.Size = New System.Drawing.Size(248, 26)
        Me.lblLogin2.TabIndex = 23
        Me.lblLogin2.Text = "PASSCODE"
        Me.lblLogin2.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblLogin2.Visible = False
        '
        'lblLogin3
        '
        Me.lblLogin3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblLogin3.BackColor = System.Drawing.Color.White
        Me.lblLogin3.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLogin3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(113, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(118, Byte), Integer))
        Me.lblLogin3.Location = New System.Drawing.Point(182, 357)
        Me.lblLogin3.Name = "lblLogin3"
        Me.lblLogin3.Size = New System.Drawing.Size(248, 26)
        Me.lblLogin3.TabIndex = 24
        Me.lblLogin3.Text = "PASSWORD"
        Me.lblLogin3.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLogin4
        '
        Me.lblLogin4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblLogin4.BackColor = System.Drawing.Color.White
        Me.lblLogin4.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLogin4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.lblLogin4.Location = New System.Drawing.Point(479, 357)
        Me.lblLogin4.Name = "lblLogin4"
        Me.lblLogin4.Size = New System.Drawing.Size(334, 35)
        Me.lblLogin4.TabIndex = 25
        Me.lblLogin4.Text = "ใส่รหัส 8 หลักที่ได้รับ เพื่อเข้าสู่บทเรียน"
        Me.lblLogin4.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblLogin4.Visible = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnClose.BackColor = System.Drawing.Color.Transparent
        Me.btnClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnClose.Image = Global.TESCO_TMS.My.Resources.Resources.btnClose
        Me.btnClose.Location = New System.Drawing.Point(460, 623)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(35, 33)
        Me.btnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnClose.TabIndex = 26
        Me.btnClose.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox4.Image = Global.TESCO_TMS.My.Resources.Resources.Login_Line
        Me.PictureBox4.Location = New System.Drawing.Point(470, 273)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(15, 231)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox4.TabIndex = 21
        Me.PictureBox4.TabStop = False
        Me.PictureBox4.Visible = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox5.Image = Global.TESCO_TMS.My.Resources.Resources.Textbox
        Me.PictureBox5.Location = New System.Drawing.Point(186, 307)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(244, 41)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox5.TabIndex = 18
        Me.PictureBox5.TabStop = False
        '
        'btnLogin
        '
        Me.btnLogin.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnLogin.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnLogin.Image = Global.TESCO_TMS.My.Resources.Resources.Login_btnLogin
        Me.btnLogin.Location = New System.Drawing.Point(221, 448)
        Me.btnLogin.Name = "btnLogin"
        Me.btnLogin.Size = New System.Drawing.Size(175, 40)
        Me.btnLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnLogin.TabIndex = 12
        Me.btnLogin.TabStop = False
        '
        'btnPassCode
        '
        Me.btnPassCode.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnPassCode.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnPassCode.Image = Global.TESCO_TMS.My.Resources.Resources.Login_btnOK
        Me.btnPassCode.Location = New System.Drawing.Point(563, 394)
        Me.btnPassCode.Name = "btnPassCode"
        Me.btnPassCode.Size = New System.Drawing.Size(175, 40)
        Me.btnPassCode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnPassCode.TabIndex = 11
        Me.btnPassCode.TabStop = False
        Me.btnPassCode.Visible = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox2.BackColor = System.Drawing.Color.White
        Me.PictureBox2.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureBox2.Image = Global.TESCO_TMS.My.Resources.Resources.Login_Logo
        Me.PictureBox2.Location = New System.Drawing.Point(335, 108)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(285, 133)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 14
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox1.Image = Global.TESCO_TMS.My.Resources.Resources.Textbox
        Me.PictureBox1.Location = New System.Drawing.Point(527, 307)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(244, 41)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 19
        Me.PictureBox1.TabStop = False
        Me.PictureBox1.Visible = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox3.Image = Global.TESCO_TMS.My.Resources.Resources.Textbox
        Me.PictureBox3.Location = New System.Drawing.Point(186, 390)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(244, 41)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 20
        Me.PictureBox3.TabStop = False
        '
        'imgLogin
        '
        Me.imgLogin.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.imgLogin.Image = Global.TESCO_TMS.My.Resources.Resources.Login_Form
        Me.imgLogin.Location = New System.Drawing.Point(129, 72)
        Me.imgLogin.Name = "imgLogin"
        Me.imgLogin.Size = New System.Drawing.Size(697, 513)
        Me.imgLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgLogin.TabIndex = 0
        Me.imgLogin.TabStop = False
        '
        'frmLogin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(228, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(955, 668)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(Me.lblLogin4)
        Me.Controls.Add(Me.lblLogin3)
        Me.Controls.Add(Me.lblLogin2)
        Me.Controls.Add(Me.lblLogin1)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.txtPassCode)
        Me.Controls.Add(Me.txtUser)
        Me.Controls.Add(Me.PictureBox5)
        Me.Controls.Add(Me.btnLogin)
        Me.Controls.Add(Me.btnPassCode)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.imgLogin)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Name = "frmLogin"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnLogin, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnPassCode, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgLogin, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents imgLogin As System.Windows.Forms.PictureBox
    Friend WithEvents txtUser As System.Windows.Forms.TextBox
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents txtPassCode As System.Windows.Forms.TextBox
    Friend WithEvents btnPassCode As System.Windows.Forms.PictureBox
    Friend WithEvents btnLogin As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents lblLogin1 As System.Windows.Forms.Label
    Friend WithEvents lblLogin2 As System.Windows.Forms.Label
    Friend WithEvents lblLogin3 As System.Windows.Forms.Label
    Friend WithEvents lblLogin4 As System.Windows.Forms.Label
    Friend WithEvents btnClose As System.Windows.Forms.PictureBox
End Class
