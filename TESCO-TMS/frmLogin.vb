﻿Public Class frmLogin

    Private Sub frmLogin_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        'txtUser.Text = "admin01"
        'txtPassword.Text = "tesco"
    End Sub

    Private Sub frmLogin_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        txtUser.Focus()
    End Sub

    Private Sub txtUser_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtUser.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtPassword.Focus()
        End If
    End Sub

    Private Sub txtPassword_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtPassword.KeyPress
        If Asc(e.KeyChar) = 13 Then
            btnLogin_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub btnLogin_Click(sender As System.Object, e As System.EventArgs) Handles btnLogin.Click
        If Login(txtUser.Text, txtPassword.Text) = True Then

            Dim v As System.Version = System.Reflection.Assembly.GetExecutingAssembly.GetName().Version
            'Dim ClientVersion As String = v.Major & "." & v.Minor & "." & v.Build & "." & v.Revision
            Dim ClientVersion As String = v.Minor & "." & v.Build & "." & v.Revision

            SaveClientLogonInfo(myUser.UserID, myUser.UserID, myUser.ClientID, ClientVersion)


            BindDatableTableFromCourse(lblWait)

            frmMain.CloseAllChildForm()
            Dim f As New frmSelectFormat
            f.MdiParent = frmMain
            f.Show()
            Me.Close()
        Else
            MessageBox.Show("Invalid Username or Password", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Dim f As New frmWelcome
        f.Show()
        Me.Close()
    End Sub
End Class