﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.pnlHead = New System.Windows.Forms.Panel()
        Me.pnlHeadCourse = New System.Windows.Forms.Panel()
        Me.lbl_H_Username = New System.Windows.Forms.Label()
        Me.lbl_H_Exam = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lbl_H_Course = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.lblHead = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.pbHeadLogo = New System.Windows.Forms.PictureBox()
        Me.btn_H_Logout = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.pnlHead.SuspendLayout()
        Me.pnlHeadCourse.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbHeadLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlHead
        '
        Me.pnlHead.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.pnlHead.BackColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(119, Byte), Integer))
        Me.pnlHead.Controls.Add(Me.pnlHeadCourse)
        Me.pnlHead.Controls.Add(Me.PictureBox1)
        Me.pnlHead.Controls.Add(Me.lblHead)
        Me.pnlHead.Controls.Add(Me.Label1)
        Me.pnlHead.Controls.Add(Me.pbHeadLogo)
        Me.pnlHead.Controls.Add(Me.btn_H_Logout)
        Me.pnlHead.Controls.Add(Me.Label2)
        Me.pnlHead.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHead.Location = New System.Drawing.Point(0, 0)
        Me.pnlHead.Name = "pnlHead"
        Me.pnlHead.Size = New System.Drawing.Size(1123, 44)
        Me.pnlHead.TabIndex = 1
        '
        'pnlHeadCourse
        '
        Me.pnlHeadCourse.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlHeadCourse.Controls.Add(Me.lbl_H_Username)
        Me.pnlHeadCourse.Controls.Add(Me.lbl_H_Exam)
        Me.pnlHeadCourse.Controls.Add(Me.Label4)
        Me.pnlHeadCourse.Controls.Add(Me.Label3)
        Me.pnlHeadCourse.Controls.Add(Me.lbl_H_Course)
        Me.pnlHeadCourse.Location = New System.Drawing.Point(603, 2)
        Me.pnlHeadCourse.Name = "pnlHeadCourse"
        Me.pnlHeadCourse.Size = New System.Drawing.Size(382, 42)
        Me.pnlHeadCourse.TabIndex = 30
        '
        'lbl_H_Username
        '
        Me.lbl_H_Username.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_H_Username.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lbl_H_Username.Font = New System.Drawing.Font("Kittithada Medium 65 P", 14.25!, System.Drawing.FontStyle.Bold)
        Me.lbl_H_Username.ForeColor = System.Drawing.Color.White
        Me.lbl_H_Username.Location = New System.Drawing.Point(3, 7)
        Me.lbl_H_Username.Name = "lbl_H_Username"
        Me.lbl_H_Username.Size = New System.Drawing.Size(184, 29)
        Me.lbl_H_Username.TabIndex = 30
        Me.lbl_H_Username.Text = "username"
        Me.lbl_H_Username.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_H_Exam
        '
        Me.lbl_H_Exam.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_H_Exam.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lbl_H_Exam.Font = New System.Drawing.Font("Kittithada Medium 65 P", 14.25!, System.Drawing.FontStyle.Bold)
        Me.lbl_H_Exam.ForeColor = System.Drawing.Color.White
        Me.lbl_H_Exam.Location = New System.Drawing.Point(281, 7)
        Me.lbl_H_Exam.Name = "lbl_H_Exam"
        Me.lbl_H_Exam.Size = New System.Drawing.Size(99, 29)
        Me.lbl_H_Exam.TabIndex = 29
        Me.lbl_H_Exam.Text = "แบบทดสอบ"
        Me.lbl_H_Exam.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(270, 3)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(20, 35)
        Me.Label4.TabIndex = 28
        Me.Label4.Text = "|"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(193, 2)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(20, 35)
        Me.Label3.TabIndex = 27
        Me.Label3.Text = "|"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_H_Course
        '
        Me.lbl_H_Course.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_H_Course.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lbl_H_Course.Font = New System.Drawing.Font("Kittithada Medium 65 P", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lbl_H_Course.ForeColor = System.Drawing.Color.White
        Me.lbl_H_Course.Location = New System.Drawing.Point(205, 7)
        Me.lbl_H_Course.Name = "lbl_H_Course"
        Me.lbl_H_Course.Size = New System.Drawing.Size(70, 29)
        Me.lbl_H_Course.TabIndex = 26
        Me.lbl_H_Course.Text = "บทเรียน"
        Me.lbl_H_Course.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.Image = Global.TESCO_TMS.My.Resources.Resources.icon_shutdown
        Me.PictureBox1.Location = New System.Drawing.Point(995, 11)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(24, 24)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 28
        Me.PictureBox1.TabStop = False
        '
        'lblHead
        '
        Me.lblHead.AutoSize = True
        Me.lblHead.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblHead.ForeColor = System.Drawing.Color.White
        Me.lblHead.Location = New System.Drawing.Point(73, 13)
        Me.lblHead.Name = "lblHead"
        Me.lblHead.Size = New System.Drawing.Size(166, 19)
        Me.lblHead.TabIndex = 0
        Me.lblHead.Text = "LEARNING CENTER" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(59, 2)
        Me.Label1.Margin = New System.Windows.Forms.Padding(0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(20, 36)
        Me.Label1.TabIndex = 27
        Me.Label1.Text = "|"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'pbHeadLogo
        '
        Me.pbHeadLogo.Image = Global.TESCO_TMS.My.Resources.Resources.Logo
        Me.pbHeadLogo.Location = New System.Drawing.Point(5, 3)
        Me.pbHeadLogo.Name = "pbHeadLogo"
        Me.pbHeadLogo.Size = New System.Drawing.Size(53, 38)
        Me.pbHeadLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pbHeadLogo.TabIndex = 26
        Me.pbHeadLogo.TabStop = False
        '
        'btn_H_Logout
        '
        Me.btn_H_Logout.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_H_Logout.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_H_Logout.Font = New System.Drawing.Font("Kittithada Medium 65 P", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_H_Logout.ForeColor = System.Drawing.Color.White
        Me.btn_H_Logout.Location = New System.Drawing.Point(1020, 7)
        Me.btn_H_Logout.Name = "btn_H_Logout"
        Me.btn_H_Logout.Size = New System.Drawing.Size(96, 29)
        Me.btn_H_Logout.TabIndex = 25
        Me.btn_H_Logout.Text = "ออกจากระบบ"
        Me.btn_H_Logout.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(981, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(20, 35)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "|"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1123, 615)
        Me.ControlBox = False
        Me.Controls.Add(Me.pnlHead)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.pnlHead.ResumeLayout(False)
        Me.pnlHead.PerformLayout()
        Me.pnlHeadCourse.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbHeadLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlHead As System.Windows.Forms.Panel
    Friend WithEvents lblHead As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btn_H_Logout As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents pbHeadLogo As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents pnlHeadCourse As System.Windows.Forms.Panel
    Friend WithEvents lbl_H_Exam As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lbl_H_Course As System.Windows.Forms.Label
    Friend WithEvents lbl_H_Username As System.Windows.Forms.Label
End Class
