﻿Imports System.Data
Imports System.IO

Public Class frmMain

    Public buttonClick As Boolean = False
    Private Delegate Sub Dlgt(ByVal Results As List(Of InstalledProgram))

    Private Sub GetProgramsFinished(ByVal InstalledPrograms As List(Of InstalledProgram))
        Dim dt As New DataTable
        dt.Columns.Add("DisplayName")
        dt.Columns.Add("Version")

        If Not InstalledPrograms Is Nothing Then
            For i As Integer = 0 To InstalledPrograms.Count - 1
                Dim dr As DataRow = dt.NewRow
                dr("DisplayName") = InstalledPrograms(i).DisplayName
                dr("Version") = InstalledPrograms(i).Version
                dt.Rows.Add(dr)
            Next

            If dt.Rows.Count > 0 Then
                dt = dt.DefaultView.ToTable(True, "DisplayName", "Version")
            End If
        End If

        'dt.DefaultView.RowFilter = "(DisplayName like 'Adobe Acrobat Reader%' and Version >= '15.023') or (DisplayName like 'Adobe Acrobat%' and Version >= '11.0') "
        'If dt.DefaultView.Count = 0 Then
        '    MessageBox.Show("Please Install Acrobat Reader", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        '    End

        '    'Dim p As New Process
        '    'p.StartInfo.FileName = Application.StartupPath & "\install_reader11_en_aih.exe"
        '    'p.StartInfo.WindowStyle = ProcessWindowStyle.Normal
        '    'p.Start()
        '    'p.WaitForExit()
        'End If
        'dt.DefaultView.RowFilter = ""

        dt.DefaultView.RowFilter = "(DisplayName like 'Microsoft Access%' and Version >= '12.0.4518.1031') or (DisplayName like 'Microsoft Office%' and Version >= '15.0')"
        If dt.DefaultView.Count = 0 Then
            MessageBox.Show("Please Install Microsoft Access Database Engine", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End

            'Dim p As New Process
            'p.StartInfo.FileName = Application.StartupPath & "\AccessDatabaseEngine.exe"
            'p.StartInfo.WindowStyle = ProcessWindowStyle.Normal
            'p.Start()
            'p.WaitForExit()
        End If
        dt.DefaultView.RowFilter = ""

        
    End Sub

    Private Sub CheckSoftwarePrerequisites()
        Try
            Dim InstalledPrograms As List(Of InstalledProgram) = InstalledProgram.GetInstalledPrograms("", False)
            Me.Invoke(New Dlgt(AddressOf GetProgramsFinished), InstalledPrograms)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub frmMain_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If buttonClick = False Then
            e.Cancel = True
            Exit Sub
        End If

        If IO.File.Exists(TempPath & "\" & "1.pdf") = True Then
            Try
                IO.File.SetAttributes(TempPath & "\" & "1.pdf", IO.FileAttributes.Normal)
                IO.File.Delete(TempPath & "\" & "1.pdf")
            Catch ex As Exception

            End Try
        End If

        If IO.File.Exists(TempPath & "\" & "4.pdf") = True Then
            Try
                IO.File.SetAttributes(TempPath & "\" & "4.pdf", IO.FileAttributes.Normal)
                IO.File.Delete(TempPath & "\" & "4.pdf")
            Catch ex As Exception

            End Try
        End If

        If IO.File.Exists(TempPath & "\" & "5.pdf") = True Then
            Try
                IO.File.SetAttributes(TempPath & "\" & "5.pdf", IO.FileAttributes.Normal)
                IO.File.Delete(TempPath & "\" & "5.pdf")
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub frmMain_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        CheckSoftwarePrerequisites()

        Try
            FolderCourseDocumentFile = TempPath & "\CourseDocumentFile"
            If IO.Directory.Exists(FolderCourseDocumentFile) = False Then
                IO.Directory.CreateDirectory(FolderCourseDocumentFile)
            End If

        Catch ex As Exception

        End Try

        Dim f As New frmWelcome
        f.Show(Me)
        f.WindowState = FormWindowState.Maximized
        'pnlMenuLong.Visible = False
    End Sub

    Sub CloseAllChildForm()
        For i As Integer = My.Application.OpenForms.Count - 1 To 0 Step -1
            If My.Application.OpenForms.Item(i) IsNot Me Then
                My.Application.OpenForms.Item(i).Close()
            End If
        Next i
    End Sub

    Sub CloseAllChildForm(CurrentForm As Form)
        For i As Integer = My.Application.OpenForms.Count - 1 To 0 Step -1
            If My.Application.OpenForms.Item(i) IsNot Me Then
                If My.Application.OpenForms.Item(i) IsNot CurrentForm Then
                    My.Application.OpenForms.Item(i).Close()
                End If
            End If
        Next i
    End Sub

#Region "Menu"

    Private Sub lbl_H_Course_Click(sender As System.Object, e As System.EventArgs) Handles lbl_H_Course.Click
        CloseAllChildForm()
        'Me.Close()

        'Dim f As New frmSelectFunction
        'f.MdiParent = Me
        'f.WindowState = FormWindowState.Maximized
        'f.lblFormatName.Text = myUser.UserFormat.ToUpper
        'f.Show()

        Dim f As New frmSelectFormat
        f.MdiParent = Me
        f.Show()
        buttonClick = True
        lbl_H_Username.Text = myUser.Name
        'Me.Close()
    End Sub

    Private Sub btn_H_Logout_Click(sender As System.Object, e As System.EventArgs) Handles btn_H_Logout.Click
        If MessageBox.Show("คุณต้องการออกจากระบบ ใช่หรือไม่ ?", "Logout", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            DeleteClientLogonInfo()

            CallAPIOnExit(Token, myUser.ClientID)
            buttonClick = True
            CloseAllChildForm()
            Application.Exit()
            Me.Close()

        End If
    End Sub


    Private Sub lbl_H_Exam_Click(sender As System.Object, e As System.EventArgs) Handles lbl_H_Exam.Click
        If GetDatableTableFromTesting() = True Then
            CloseAllChildForm()
            Dim f As New frmSelectTestCourse
            f.MdiParent = Me
            f.Show()
        End If
    End Sub

#End Region


End Class