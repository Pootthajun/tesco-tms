﻿Imports System.Web

Public Class frmProgressDownload

    Public URL As String = ""
    Public PathDownloadTo As String = ""
    Private WithEvents _Downloader As WebFileDownloader

    Public Shared Sub Main()
        Application.EnableVisualStyles()
        Application.DoEvents()
        Application.Run(New frmMain)
        Application.Exit()
    End Sub

    Private Sub frmProgressDownload_Load(sender As Object, e As System.EventArgs) Handles Me.Shown
        DownloadFile()
    End Sub

    Private Sub _Downloader_FileDownloadSizeObtained(ByVal iFileSize As Long) Handles _Downloader.FileDownloadSizeObtained
        ProgBar.Value = 0
        ProgBar.Maximum = Convert.ToInt32(iFileSize)
    End Sub

    'FIRES WHEN DOWNLOAD IS COMPLETE
    Private Sub _Downloader_FileDownloadComplete() Handles _Downloader.FileDownloadComplete
        ProgBar.Value = ProgBar.Maximum
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    'FIRES WHEN DOWNLOAD FAILES. PASSES IN EXCEPTION INFO
    Private Sub _Downloader_FileDownloadFailed(ByVal ex As System.Exception) Handles _Downloader.FileDownloadFailed
        'MessageBox.Show("An error has occured during download: " & ex.Message)
        Me.Close()
    End Sub

    'FIRES WHEN MORE OF THE FILE HAS BEEN DOWNLOADED
    Private Sub _Downloader_AmountDownloadedChanged(ByVal iNewProgress As Long) Handles _Downloader.AmountDownloadedChanged
        ProgBar.Value = Convert.ToInt32(iNewProgress)
        lblProgress.Text = WebFileDownloader.FormatFileSize(iNewProgress) & " of " & WebFileDownloader.FormatFileSize(ProgBar.Maximum) & " downloaded"
        Application.DoEvents()
    End Sub

    Sub DownloadFile()
        'If InStr(URL, ".mp4") Then
        '    'PathDownloadTo = TempPath & "\1.mp4"
        '    'ElseIf InStr(URL, ".pdf") Then
        '    '    PathDownloadTo = TempPath & "\2.pdf"
        'ElseIf InStr(URL, ".swf") Then
        '    PathDownloadTo = TempPath & "\3.swf"
        '    'ElseIf InStr(URL, ".ppt") Then
        '    '    PathDownloadTo = TempPath & "\4.ppt"
        '    'ElseIf InStr(URL, ".pptx") Then
        '    '    PathDownloadTo = TempPath & "\5.ppt"
        '    'ElseIf InStr(URL, ".mpg") Then
        '    '    PathDownloadTo = TempPath & "\6.mpg"
        'End If

        If System.IO.File.Exists(PathDownloadTo) = True Then
            System.IO.File.Delete(PathDownloadTo)
        End If

        Try
            _Downloader = New WebFileDownloader
            _Downloader.DownloadFileWithProgress(URL, PathDownloadTo)
        Catch ex As Exception
            Me.Close()
        End Try
    End Sub

End Class