﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmQuestion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pnlQuestion = New System.Windows.Forms.Panel()
        Me.pnlSummary = New System.Windows.Forms.Panel()
        Me.lblBack = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblQuestionQty = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblTargetPercent = New System.Windows.Forms.Label()
        Me.lblCurrentScore = New System.Windows.Forms.Label()
        Me.lblResultHeader = New System.Windows.Forms.Label()
        Me.lblChoice4 = New System.Windows.Forms.Label()
        Me.btnAnswer = New System.Windows.Forms.Label()
        Me.lblQuestionDescription = New System.Windows.Forms.Label()
        Me.lblCheck4 = New System.Windows.Forms.Label()
        Me.lblCheck3 = New System.Windows.Forms.Label()
        Me.lblCheck2 = New System.Windows.Forms.Label()
        Me.lblCheck1 = New System.Windows.Forms.Label()
        Me.lblQuestionNo = New System.Windows.Forms.Label()
        Me.lblCorrect4 = New System.Windows.Forms.Label()
        Me.lblCorrect3 = New System.Windows.Forms.Label()
        Me.lblCorrect2 = New System.Windows.Forms.Label()
        Me.lblCorrect1 = New System.Windows.Forms.Label()
        Me.lblChoice3 = New System.Windows.Forms.Label()
        Me.lblChoice2 = New System.Windows.Forms.Label()
        Me.lblChoice1 = New System.Windows.Forms.Label()
        Me.pbCh4 = New System.Windows.Forms.PictureBox()
        Me.pbCh3 = New System.Windows.Forms.PictureBox()
        Me.pbCh2 = New System.Windows.Forms.PictureBox()
        Me.pbCh1 = New System.Windows.Forms.PictureBox()
        Me.pbQuestionCoverImage = New System.Windows.Forms.PictureBox()
        Me.lblTestingName = New System.Windows.Forms.Label()
        Me.lblFormatName = New System.Windows.Forms.Label()
        Me.pnlQuestion.SuspendLayout()
        Me.pnlSummary.SuspendLayout()
        CType(Me.pbCh4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbCh3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbCh2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbCh1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbQuestionCoverImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlQuestion
        '
        Me.pnlQuestion.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlQuestion.BackColor = System.Drawing.Color.Silver
        Me.pnlQuestion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pnlQuestion.Controls.Add(Me.pnlSummary)
        Me.pnlQuestion.Controls.Add(Me.lblChoice4)
        Me.pnlQuestion.Controls.Add(Me.btnAnswer)
        Me.pnlQuestion.Controls.Add(Me.lblQuestionDescription)
        Me.pnlQuestion.Controls.Add(Me.lblCheck4)
        Me.pnlQuestion.Controls.Add(Me.lblCheck3)
        Me.pnlQuestion.Controls.Add(Me.lblCheck2)
        Me.pnlQuestion.Controls.Add(Me.lblCheck1)
        Me.pnlQuestion.Controls.Add(Me.lblQuestionNo)
        Me.pnlQuestion.Controls.Add(Me.lblCorrect4)
        Me.pnlQuestion.Controls.Add(Me.lblCorrect3)
        Me.pnlQuestion.Controls.Add(Me.lblCorrect2)
        Me.pnlQuestion.Controls.Add(Me.lblCorrect1)
        Me.pnlQuestion.Controls.Add(Me.lblChoice3)
        Me.pnlQuestion.Controls.Add(Me.lblChoice2)
        Me.pnlQuestion.Controls.Add(Me.lblChoice1)
        Me.pnlQuestion.Controls.Add(Me.pbCh4)
        Me.pnlQuestion.Controls.Add(Me.pbCh3)
        Me.pnlQuestion.Controls.Add(Me.pbCh2)
        Me.pnlQuestion.Controls.Add(Me.pbCh1)
        Me.pnlQuestion.Controls.Add(Me.pbQuestionCoverImage)
        Me.pnlQuestion.Location = New System.Drawing.Point(50, 104)
        Me.pnlQuestion.Name = "pnlQuestion"
        Me.pnlQuestion.Size = New System.Drawing.Size(1262, 616)
        Me.pnlQuestion.TabIndex = 49
        '
        'pnlSummary
        '
        Me.pnlSummary.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlSummary.BackgroundImage = Global.TESCO_TMS.My.Resources.Resources.Test_Quset_Dialog_White
        Me.pnlSummary.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pnlSummary.Controls.Add(Me.lblBack)
        Me.pnlSummary.Controls.Add(Me.Label4)
        Me.pnlSummary.Controls.Add(Me.lblQuestionQty)
        Me.pnlSummary.Controls.Add(Me.Label3)
        Me.pnlSummary.Controls.Add(Me.Label2)
        Me.pnlSummary.Controls.Add(Me.lblTargetPercent)
        Me.pnlSummary.Controls.Add(Me.lblCurrentScore)
        Me.pnlSummary.Controls.Add(Me.lblResultHeader)
        Me.pnlSummary.Location = New System.Drawing.Point(0, 0)
        Me.pnlSummary.Name = "pnlSummary"
        Me.pnlSummary.Size = New System.Drawing.Size(1262, 616)
        Me.pnlSummary.TabIndex = 71
        Me.pnlSummary.Visible = False
        '
        'lblBack
        '
        Me.lblBack.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.lblBack.BackColor = System.Drawing.Color.FromArgb(CType(CType(2, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(121, Byte), Integer))
        Me.lblBack.Font = New System.Drawing.Font("Kittithada Medium 65 F", 30.0!, System.Drawing.FontStyle.Bold)
        Me.lblBack.ForeColor = System.Drawing.Color.White
        Me.lblBack.Location = New System.Drawing.Point(495, 509)
        Me.lblBack.Name = "lblBack"
        Me.lblBack.Size = New System.Drawing.Size(228, 61)
        Me.lblBack.TabIndex = 50
        Me.lblBack.Text = "กลับหน้าแรก"
        Me.lblBack.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Kittithada Medium 65 F", 40.0!)
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(872, 247)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(72, 66)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "ข้อ"
        '
        'lblQuestionQty
        '
        Me.lblQuestionQty.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblQuestionQty.BackColor = System.Drawing.Color.Transparent
        Me.lblQuestionQty.Font = New System.Drawing.Font("Kittithada Medium 65 F", 60.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQuestionQty.ForeColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(119, Byte), Integer))
        Me.lblQuestionQty.Location = New System.Drawing.Point(786, 224)
        Me.lblQuestionQty.Name = "lblQuestionQty"
        Me.lblQuestionQty.Size = New System.Drawing.Size(105, 97)
        Me.lblQuestionQty.TabIndex = 5
        Me.lblQuestionQty.Text = "88"
        Me.lblQuestionQty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Kittithada Medium 65 F", 40.0!)
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(559, 247)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(243, 66)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "ข้อจากทั้งหมด"
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Kittithada Medium 65 F", 40.0!)
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(281, 247)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(200, 66)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "คุณตอบถูก"
        '
        'lblTargetPercent
        '
        Me.lblTargetPercent.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblTargetPercent.AutoSize = True
        Me.lblTargetPercent.BackColor = System.Drawing.Color.Transparent
        Me.lblTargetPercent.Font = New System.Drawing.Font("Kittithada Medium 65 F", 40.0!)
        Me.lblTargetPercent.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblTargetPercent.Location = New System.Drawing.Point(419, 315)
        Me.lblTargetPercent.Name = "lblTargetPercent"
        Me.lblTargetPercent.Size = New System.Drawing.Size(369, 66)
        Me.lblTargetPercent.TabIndex = 2
        Me.lblTargetPercent.Text = "ผ่านเกณฑ์คะแนน 50%"
        '
        'lblCurrentScore
        '
        Me.lblCurrentScore.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblCurrentScore.BackColor = System.Drawing.Color.Transparent
        Me.lblCurrentScore.Font = New System.Drawing.Font("Kittithada Medium 65 F", 60.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrentScore.ForeColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(119, Byte), Integer))
        Me.lblCurrentScore.Location = New System.Drawing.Point(461, 224)
        Me.lblCurrentScore.Margin = New System.Windows.Forms.Padding(0)
        Me.lblCurrentScore.Name = "lblCurrentScore"
        Me.lblCurrentScore.Size = New System.Drawing.Size(113, 97)
        Me.lblCurrentScore.TabIndex = 1
        Me.lblCurrentScore.Text = "88"
        Me.lblCurrentScore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblResultHeader
        '
        Me.lblResultHeader.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblResultHeader.BackColor = System.Drawing.Color.Transparent
        Me.lblResultHeader.Font = New System.Drawing.Font("Kittithada Bold 75 F", 60.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblResultHeader.ForeColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(119, Byte), Integer))
        Me.lblResultHeader.Location = New System.Drawing.Point(131, 62)
        Me.lblResultHeader.Name = "lblResultHeader"
        Me.lblResultHeader.Size = New System.Drawing.Size(986, 98)
        Me.lblResultHeader.TabIndex = 0
        Me.lblResultHeader.Text = "ยินดีด้วยคุณสอบผ่านวิชานี้แล้ว"
        Me.lblResultHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblChoice4
        '
        Me.lblChoice4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblChoice4.BackColor = System.Drawing.Color.Transparent
        Me.lblChoice4.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblChoice4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblChoice4.Location = New System.Drawing.Point(464, 346)
        Me.lblChoice4.Name = "lblChoice4"
        Me.lblChoice4.Size = New System.Drawing.Size(734, 56)
        Me.lblChoice4.TabIndex = 62
        Me.lblChoice4.Text = "A 4"
        '
        'btnAnswer
        '
        Me.btnAnswer.BackColor = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(121, Byte), Integer))
        Me.btnAnswer.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnAnswer.Font = New System.Drawing.Font("Kittithada Medium 65 F", 24.0!)
        Me.btnAnswer.ForeColor = System.Drawing.Color.White
        Me.btnAnswer.Location = New System.Drawing.Point(423, 461)
        Me.btnAnswer.Name = "btnAnswer"
        Me.btnAnswer.Size = New System.Drawing.Size(169, 48)
        Me.btnAnswer.TabIndex = 73
        Me.btnAnswer.Text = "ตอบ"
        Me.btnAnswer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblQuestionDescription
        '
        Me.lblQuestionDescription.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblQuestionDescription.BackColor = System.Drawing.Color.Transparent
        Me.lblQuestionDescription.Font = New System.Drawing.Font("Kittithada Medium 65 F", 30.0!)
        Me.lblQuestionDescription.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblQuestionDescription.Location = New System.Drawing.Point(168, 14)
        Me.lblQuestionDescription.Name = "lblQuestionDescription"
        Me.lblQuestionDescription.Size = New System.Drawing.Size(1078, 169)
        Me.lblQuestionDescription.TabIndex = 72
        Me.lblQuestionDescription.Text = "คำถามที่ 1"
        '
        'lblCheck4
        '
        Me.lblCheck4.AutoSize = True
        Me.lblCheck4.BackColor = System.Drawing.Color.Transparent
        Me.lblCheck4.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCheck4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblCheck4.Location = New System.Drawing.Point(384, 346)
        Me.lblCheck4.Name = "lblCheck4"
        Me.lblCheck4.Size = New System.Drawing.Size(25, 23)
        Me.lblCheck4.TabIndex = 70
        Me.lblCheck4.Text = "N"
        Me.lblCheck4.Visible = False
        '
        'lblCheck3
        '
        Me.lblCheck3.AutoSize = True
        Me.lblCheck3.BackColor = System.Drawing.Color.Transparent
        Me.lblCheck3.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCheck3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblCheck3.Location = New System.Drawing.Point(384, 299)
        Me.lblCheck3.Name = "lblCheck3"
        Me.lblCheck3.Size = New System.Drawing.Size(25, 23)
        Me.lblCheck3.TabIndex = 69
        Me.lblCheck3.Text = "N"
        Me.lblCheck3.Visible = False
        '
        'lblCheck2
        '
        Me.lblCheck2.AutoSize = True
        Me.lblCheck2.BackColor = System.Drawing.Color.Transparent
        Me.lblCheck2.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCheck2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblCheck2.Location = New System.Drawing.Point(384, 251)
        Me.lblCheck2.Name = "lblCheck2"
        Me.lblCheck2.Size = New System.Drawing.Size(25, 23)
        Me.lblCheck2.TabIndex = 68
        Me.lblCheck2.Text = "N"
        Me.lblCheck2.Visible = False
        '
        'lblCheck1
        '
        Me.lblCheck1.AutoSize = True
        Me.lblCheck1.BackColor = System.Drawing.Color.Transparent
        Me.lblCheck1.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCheck1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblCheck1.Location = New System.Drawing.Point(384, 204)
        Me.lblCheck1.Name = "lblCheck1"
        Me.lblCheck1.Size = New System.Drawing.Size(25, 23)
        Me.lblCheck1.TabIndex = 67
        Me.lblCheck1.Text = "N"
        Me.lblCheck1.Visible = False
        '
        'lblQuestionNo
        '
        Me.lblQuestionNo.BackColor = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(121, Byte), Integer))
        Me.lblQuestionNo.Font = New System.Drawing.Font("Kittithada Medium 65 F", 30.0!)
        Me.lblQuestionNo.ForeColor = System.Drawing.Color.White
        Me.lblQuestionNo.Location = New System.Drawing.Point(0, 14)
        Me.lblQuestionNo.Name = "lblQuestionNo"
        Me.lblQuestionNo.Size = New System.Drawing.Size(162, 48)
        Me.lblQuestionNo.TabIndex = 52
        Me.lblQuestionNo.Text = "ข้อ 1"
        Me.lblQuestionNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCorrect4
        '
        Me.lblCorrect4.AutoSize = True
        Me.lblCorrect4.BackColor = System.Drawing.Color.Transparent
        Me.lblCorrect4.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCorrect4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblCorrect4.Location = New System.Drawing.Point(650, 346)
        Me.lblCorrect4.Name = "lblCorrect4"
        Me.lblCorrect4.Size = New System.Drawing.Size(25, 23)
        Me.lblCorrect4.TabIndex = 66
        Me.lblCorrect4.Text = "N"
        Me.lblCorrect4.Visible = False
        '
        'lblCorrect3
        '
        Me.lblCorrect3.AutoSize = True
        Me.lblCorrect3.BackColor = System.Drawing.Color.Transparent
        Me.lblCorrect3.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCorrect3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblCorrect3.Location = New System.Drawing.Point(650, 299)
        Me.lblCorrect3.Name = "lblCorrect3"
        Me.lblCorrect3.Size = New System.Drawing.Size(25, 23)
        Me.lblCorrect3.TabIndex = 65
        Me.lblCorrect3.Text = "N"
        Me.lblCorrect3.Visible = False
        '
        'lblCorrect2
        '
        Me.lblCorrect2.AutoSize = True
        Me.lblCorrect2.BackColor = System.Drawing.Color.Transparent
        Me.lblCorrect2.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCorrect2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblCorrect2.Location = New System.Drawing.Point(650, 251)
        Me.lblCorrect2.Name = "lblCorrect2"
        Me.lblCorrect2.Size = New System.Drawing.Size(25, 23)
        Me.lblCorrect2.TabIndex = 64
        Me.lblCorrect2.Text = "N"
        Me.lblCorrect2.Visible = False
        '
        'lblCorrect1
        '
        Me.lblCorrect1.AutoSize = True
        Me.lblCorrect1.BackColor = System.Drawing.Color.Transparent
        Me.lblCorrect1.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCorrect1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblCorrect1.Location = New System.Drawing.Point(650, 204)
        Me.lblCorrect1.Name = "lblCorrect1"
        Me.lblCorrect1.Size = New System.Drawing.Size(25, 23)
        Me.lblCorrect1.TabIndex = 63
        Me.lblCorrect1.Text = "N"
        Me.lblCorrect1.Visible = False
        '
        'lblChoice3
        '
        Me.lblChoice3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblChoice3.BackColor = System.Drawing.Color.Transparent
        Me.lblChoice3.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblChoice3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblChoice3.Location = New System.Drawing.Point(464, 299)
        Me.lblChoice3.Name = "lblChoice3"
        Me.lblChoice3.Size = New System.Drawing.Size(734, 47)
        Me.lblChoice3.TabIndex = 61
        Me.lblChoice3.Text = "A 3"
        '
        'lblChoice2
        '
        Me.lblChoice2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblChoice2.BackColor = System.Drawing.Color.Transparent
        Me.lblChoice2.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblChoice2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblChoice2.Location = New System.Drawing.Point(464, 251)
        Me.lblChoice2.Name = "lblChoice2"
        Me.lblChoice2.Size = New System.Drawing.Size(734, 48)
        Me.lblChoice2.TabIndex = 60
        Me.lblChoice2.Text = "A 2"
        '
        'lblChoice1
        '
        Me.lblChoice1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblChoice1.BackColor = System.Drawing.Color.Transparent
        Me.lblChoice1.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblChoice1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblChoice1.Location = New System.Drawing.Point(464, 204)
        Me.lblChoice1.Name = "lblChoice1"
        Me.lblChoice1.Size = New System.Drawing.Size(734, 47)
        Me.lblChoice1.TabIndex = 59
        Me.lblChoice1.Text = "A 1"
        '
        'pbCh4
        '
        Me.pbCh4.BackColor = System.Drawing.Color.Transparent
        Me.pbCh4.BackgroundImage = Global.TESCO_TMS.My.Resources.Resources.chkAnswerCheckFalse
        Me.pbCh4.Location = New System.Drawing.Point(422, 341)
        Me.pbCh4.Name = "pbCh4"
        Me.pbCh4.Size = New System.Drawing.Size(31, 31)
        Me.pbCh4.TabIndex = 58
        Me.pbCh4.TabStop = False
        '
        'pbCh3
        '
        Me.pbCh3.BackColor = System.Drawing.Color.Transparent
        Me.pbCh3.BackgroundImage = Global.TESCO_TMS.My.Resources.Resources.chkAnswerCheckFalse
        Me.pbCh3.Location = New System.Drawing.Point(422, 295)
        Me.pbCh3.Name = "pbCh3"
        Me.pbCh3.Size = New System.Drawing.Size(31, 31)
        Me.pbCh3.TabIndex = 57
        Me.pbCh3.TabStop = False
        '
        'pbCh2
        '
        Me.pbCh2.BackColor = System.Drawing.Color.Transparent
        Me.pbCh2.BackgroundImage = Global.TESCO_TMS.My.Resources.Resources.chkAnswerCheckFalse
        Me.pbCh2.Location = New System.Drawing.Point(422, 247)
        Me.pbCh2.Name = "pbCh2"
        Me.pbCh2.Size = New System.Drawing.Size(31, 31)
        Me.pbCh2.TabIndex = 56
        Me.pbCh2.TabStop = False
        '
        'pbCh1
        '
        Me.pbCh1.BackColor = System.Drawing.Color.Transparent
        Me.pbCh1.BackgroundImage = Global.TESCO_TMS.My.Resources.Resources.chkAnswerCheckFalse
        Me.pbCh1.Location = New System.Drawing.Point(422, 200)
        Me.pbCh1.Name = "pbCh1"
        Me.pbCh1.Size = New System.Drawing.Size(31, 31)
        Me.pbCh1.TabIndex = 55
        Me.pbCh1.TabStop = False
        '
        'pbQuestionCoverImage
        '
        Me.pbQuestionCoverImage.BackColor = System.Drawing.Color.Transparent
        Me.pbQuestionCoverImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbQuestionCoverImage.Location = New System.Drawing.Point(106, 186)
        Me.pbQuestionCoverImage.Name = "pbQuestionCoverImage"
        Me.pbQuestionCoverImage.Size = New System.Drawing.Size(272, 218)
        Me.pbQuestionCoverImage.TabIndex = 54
        Me.pbQuestionCoverImage.TabStop = False
        Me.pbQuestionCoverImage.Visible = False
        '
        'lblTestingName
        '
        Me.lblTestingName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTestingName.AutoSize = True
        Me.lblTestingName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblTestingName.Font = New System.Drawing.Font("Kittithada Medium 65 P", 22.0!, System.Drawing.FontStyle.Bold)
        Me.lblTestingName.ForeColor = System.Drawing.Color.White
        Me.lblTestingName.Location = New System.Drawing.Point(149, 47)
        Me.lblTestingName.Name = "lblTestingName"
        Me.lblTestingName.Size = New System.Drawing.Size(192, 37)
        Me.lblTestingName.TabIndex = 51
        Me.lblTestingName.Text = "แบบทดสอบวิชาที่ 1"
        Me.lblTestingName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFormatName
        '
        Me.lblFormatName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblFormatName.AutoSize = True
        Me.lblFormatName.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lblFormatName.Font = New System.Drawing.Font("Kittithada Medium 65 P", 22.0!, System.Drawing.FontStyle.Bold)
        Me.lblFormatName.ForeColor = System.Drawing.Color.FromArgb(CType(CType(90, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(143, Byte), Integer))
        Me.lblFormatName.Location = New System.Drawing.Point(12, 47)
        Me.lblFormatName.Name = "lblFormatName"
        Me.lblFormatName.Size = New System.Drawing.Size(153, 37)
        Me.lblFormatName.TabIndex = 50
        Me.lblFormatName.Text = "แบบทดสอบ > "
        Me.lblFormatName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmQuestion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1366, 768)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblTestingName)
        Me.Controls.Add(Me.lblFormatName)
        Me.Controls.Add(Me.pnlQuestion)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmQuestion"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.pnlQuestion.ResumeLayout(False)
        Me.pnlQuestion.PerformLayout()
        Me.pnlSummary.ResumeLayout(False)
        Me.pnlSummary.PerformLayout()
        CType(Me.pbCh4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbCh3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbCh2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbCh1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbQuestionCoverImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pnlQuestion As System.Windows.Forms.Panel
    Friend WithEvents lblQuestionNo As System.Windows.Forms.Label
    Friend WithEvents pbQuestionCoverImage As System.Windows.Forms.PictureBox
    Friend WithEvents pbCh1 As System.Windows.Forms.PictureBox
    Friend WithEvents pbCh4 As System.Windows.Forms.PictureBox
    Friend WithEvents pbCh3 As System.Windows.Forms.PictureBox
    Friend WithEvents pbCh2 As System.Windows.Forms.PictureBox
    Friend WithEvents lblCorrect4 As System.Windows.Forms.Label
    Friend WithEvents lblCorrect3 As System.Windows.Forms.Label
    Friend WithEvents lblCorrect2 As System.Windows.Forms.Label
    Friend WithEvents lblCorrect1 As System.Windows.Forms.Label
    Friend WithEvents lblChoice4 As System.Windows.Forms.Label
    Friend WithEvents lblChoice3 As System.Windows.Forms.Label
    Friend WithEvents lblChoice2 As System.Windows.Forms.Label
    Friend WithEvents lblChoice1 As System.Windows.Forms.Label
    Friend WithEvents lblCheck4 As System.Windows.Forms.Label
    Friend WithEvents lblCheck3 As System.Windows.Forms.Label
    Friend WithEvents lblCheck2 As System.Windows.Forms.Label
    Friend WithEvents lblCheck1 As System.Windows.Forms.Label
    Friend WithEvents pnlSummary As System.Windows.Forms.Panel
    Friend WithEvents lblTargetPercent As System.Windows.Forms.Label
    Friend WithEvents lblCurrentScore As System.Windows.Forms.Label
    Friend WithEvents lblResultHeader As System.Windows.Forms.Label
    Friend WithEvents lblQuestionDescription As System.Windows.Forms.Label
    Friend WithEvents btnAnswer As System.Windows.Forms.Label
    Friend WithEvents lblTestingName As System.Windows.Forms.Label
    Friend WithEvents lblFormatName As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblQuestionQty As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblBack As System.Windows.Forms.Label
End Class
