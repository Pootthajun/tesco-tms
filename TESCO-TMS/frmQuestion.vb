﻿Imports System.Data

Public Class frmQuestion

    Dim _QuestionQty As Integer = 0
    Dim _CurrentScore As Integer = 0
    Dim _CurrentQuestionNo As Integer = 0
    Dim _TestID As Long = 0
    Dim _CourseID As Long = 0
    Dim qDT As New DataTable

    Public WriteOnly Property QuestionQty As Double
        Set(value As Double)
            _QuestionQty = value
        End Set
    End Property
    Public WriteOnly Property TestID As Long
        Set(value As Long)
            _TestID = value

            DT_TEST.DefaultView.RowFilter = "id=" & _TestID
            If DT_TEST.DefaultView.Count > 0 Then
                lblTestingName.Text = DT_TEST.DefaultView(0)("title")
                _CourseID = DT_TEST.DefaultView(0)("course_id")
            End If
            DT_TEST.DefaultView.RowFilter = ""

            DT_TEST_QUESTION.DefaultView.RowFilter = "tb_test_id=" & _TestID
            If DT_TEST_QUESTION.DefaultView.Count > 0 Then
                qDT = DT_TEST_QUESTION.DefaultView.ToTable.Copy
                If qDT.Rows.Count > 0 Then
                    qDT.Columns.Add("answer_id")
                    qDT.Columns.Add("answer_result")
                    qDT.Columns.Add("time_spent", GetType(Int16))
                End If
            End If
            DT_TEST_QUESTION.DefaultView.RowFilter = ""

        End Set
    End Property


    Private Sub frmQuestion_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Me.WindowState = FormWindowState.Maximized
        Me.ControlBox = False

        ShowQuestion(1)
    End Sub

    Dim StartTime As DateTime = DateTime.Now
    Private Sub ShowQuestion(QuestionNo As Long)
        lblCheck1.Text = "N"
        lblCheck2.Text = "N"
        lblCheck3.Text = "N"
        lblCheck4.Text = "N"

        pbCh1.BackgroundImage = My.Resources.chkAnswerCheckFalse
        pbCh2.BackgroundImage = My.Resources.chkAnswerCheckFalse
        pbCh3.BackgroundImage = My.Resources.chkAnswerCheckFalse
        pbCh4.BackgroundImage = My.Resources.chkAnswerCheckFalse

        qDT.DefaultView.RowFilter = "question_no='" & QuestionNo & "'"
        If qDT.DefaultView.Count > 0 Then
            Dim tmpAnswer() As String = Split(qDT.DefaultView(0)("answer"), "##")
            Dim tmpChoice() As String = Split(qDT.DefaultView(0)("choice"), "##")

            If tmpChoice.Length = 4 And tmpAnswer.Length = 4 Then
                Dim scrWith As Integer = Screen.PrimaryScreen.Bounds.Width

                lblQuestionNo.Text = "ข้อ " & QuestionNo & " / " & qDT.Rows.Count
                _CurrentQuestionNo = QuestionNo

                lblQuestionDescription.Text = qDT.DefaultView(0)("question")
                lblChoice1.Text = tmpChoice(0)
                lblChoice2.Text = tmpChoice(1)
                lblChoice3.Text = tmpChoice(2)
                lblChoice4.Text = tmpChoice(3)

                lblCorrect1.Text = IIf(tmpAnswer(0) = "True", "Y", "N")
                lblCorrect2.Text = IIf(tmpAnswer(1) = "True", "Y", "N")
                lblCorrect3.Text = IIf(tmpAnswer(2) = "True", "Y", "N")
                lblCorrect4.Text = IIf(tmpAnswer(3) = "True", "Y", "N")

                If Convert.IsDBNull(qDT.DefaultView(0)("icon_file")) = False Then
                    pbQuestionCoverImage.Visible = True
                    pbQuestionCoverImage.BackgroundImage = GetImageFromURL(qDT.DefaultView(0)("icon_file"))
                Else
                    pbQuestionCoverImage.Visible = False
                    pbQuestionCoverImage.BackgroundImage = Nothing
                End If
            End If

            StartTime = DateTime.Now
        Else
            'To Summary Screen
            Dim _TargetPercent As Integer = 0
            DT_TEST.DefaultView.RowFilter = "id=" & _TestID
            If DT_TEST.DefaultView.Count > 0 Then
                _TargetPercent = DT_TEST.DefaultView(0)("target_percentage")
            End If
            DT_TEST.DefaultView.RowFilter = ""

            Dim _ScorePercent As Integer = (_CurrentScore * 100) / qDT.Rows.Count
            lblCurrentScore.Text = _CurrentScore
            lblQuestionQty.Text = qDT.Rows.Count

            Dim c As Color
            If _ScorePercent >= _TargetPercent Then
                lblResultHeader.Text = "ยินดีด้วยคุณสอบผ่านวิชานี้แล้ว"
                lblTargetPercent.Text = "ผ่านเกณฑ์คะแนน " & _TargetPercent & "%"
                c = Color.FromArgb(3, 148, 119)
            Else
                lblResultHeader.Text = "เสียใจด้วยคุณยังสอบไม่ผ่านวิชานี้"
                lblTargetPercent.Text = "ไม่ผ่านเกณฑ์คะแนน " & _TargetPercent & "%"
                c = Color.Red
            End If

            lblResultHeader.ForeColor = c
            lblCurrentScore.ForeColor = c
            lblQuestionQty.ForeColor = c

            lblTestingName.Text = "สรุปผลการทดสอบ"

            pnlSummary.Visible = True


            '    'บันทึก Log
            Dim AnswerData As String = ""
            AnswerData = "{" & Chr(34) & "testing_id" & Chr(34) & ":" & _TestID & ","
            AnswerData += Chr(34) & "user_id" & Chr(34) & ":" & myUser.UserID & ","
            AnswerData += Chr(34) & "course_id" & Chr(34) & ":" & _CourseID & ", "
            AnswerData += Chr(34) & "is_success" & Chr(34) & ":" & Chr(34) & "true" & Chr(34) & ", "
            AnswerData += Chr(34) & "target_percentage" & Chr(34) & ":" & _TargetPercent & ", "
            AnswerData += Chr(34) + "answer_data" + Chr(34) & ":[" & Environment.NewLine
            For Each qDr As DataRow In qDT.Rows
                If qDr("question_no") > 1 Then
                    AnswerData += ", "
                End If
                AnswerData += "{"
                AnswerData += Chr(34) + "question_id" + Chr(34) & ":" & qDr("id") & ", " & Environment.NewLine
                AnswerData += Chr(34) + "answer_id" + Chr(34) & ":" & qDr("answer_id") & ", " & Environment.NewLine
                AnswerData += Chr(34) + "is_correct" + Chr(34) & ":" & Chr(34) & qDr("answer_result") & Chr(34) & ", " & Environment.NewLine
                AnswerData += Chr(34) + "time_spent" + Chr(34) & ":" & qDr("time_spent") & Environment.NewLine
                AnswerData += "}"
            Next
            AnswerData += "]"
            AnswerData += "}"

            CallAPIUpdateLog(Token, myUser.ClientID, "complete", "testing", AnswerData)
        End If
        qDT.DefaultView.RowFilter = ""
        Application.DoEvents()
    End Sub

    Private Sub btnAnswer_Click(sender As System.Object, e As System.EventArgs) Handles btnAnswer.Click
        Dim isCorrect As Boolean = True
        Dim isCheck As Boolean = False
        Dim AnswerID As Int16 = 0

        If lblCheck1.Text = "Y" Then
            If lblCorrect1.Text = "N" Then
                isCorrect = False
            End If
            isCheck = True
            AnswerID = 0
        End If

        If isCorrect = True Then
            If lblCheck2.Text = "Y" Then
                If lblCorrect2.Text = "N" Then
                    isCorrect = False
                End If
                isCheck = True
                AnswerID = 1
            End If
        End If

        If isCorrect = True Then
            If lblCheck3.Text = "Y" Then
                If lblCorrect3.Text = "N" Then
                    isCorrect = False
                End If
                isCheck = True
                AnswerID = 2
            End If
        End If

        If lblCheck4.Text = "Y" Then
            If lblCorrect4.Text = "N" Then
                isCorrect = False
            End If
            isCheck = True
            AnswerID = 3
        End If

        If isCheck = False Then
            MessageBox.Show("กรุณาเลือกคำตอบ", "ผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        Dim ResultAnswer As String = ""
        If lblCorrect1.Text = "Y" Then
            ResultAnswer = lblChoice1.Text
        ElseIf lblCorrect2.Text = "Y" Then
            ResultAnswer = lblChoice2.Text
        ElseIf lblCorrect3.Text = "Y" Then
            ResultAnswer = lblChoice3.Text
        ElseIf lblCorrect4.Text = "Y" Then
            ResultAnswer = lblChoice4.Text
        End If


        'Dim nform As New frmQuestionDialog
        'If isCorrect = True Then
        '    nform.lblCorrect.Visible = True
        '    nform.pbIcon.BackgroundImage = My.Resources.icoCorrect
        'Else
        '    nform.pbHeader.Image = My.Resources.bgTestingDialogWrong
        '    nform.lblCorrect.Visible = False
        '    nform.pbIcon.Visible = False
        '    nform.lblIncorrect.Visible = True
        '    nform.lblResultAnswer.Visible = True
        '    nform.lblResultAnswer.Text = ResultAnswer
        'End If

        'For i As Integer = 0 To qDT.Rows.Count - 1
        '    If Convert.ToInt32(qDT.Rows(i)("question_no")) = _CurrentQuestionNo Then
        '        qDT.Rows(i)("answer_id") = AnswerID
        '        qDT.Rows(i)("answer_result") = isCorrect.ToString.ToLower
        '        qDT.Rows(i)("time_spent") = DateDiff(DateInterval.Second, StartTime, DateTime.Now)
        '    End If
        'Next
        'Plexiglass(nform)

        If isCorrect = True Then
            _CurrentScore += 1
        End If

        ShowQuestion(_CurrentQuestionNo + 1)
    End Sub

    Private Sub CheckChoice(lblCheck As Label, pbCh As PictureBox)
        If lblCheck.Text = "N" Then
            lblCheck1.Text = "N"
            pbCh1.BackgroundImage = My.Resources.chkAnswerCheckFalse
            lblCheck2.Text = "N"
            pbCh2.BackgroundImage = My.Resources.chkAnswerCheckFalse
            lblCheck3.Text = "N"
            pbCh3.BackgroundImage = My.Resources.chkAnswerCheckFalse
            lblCheck4.Text = "N"
            pbCh4.BackgroundImage = My.Resources.chkAnswerCheckFalse

            pbCh.BackgroundImage = My.Resources.chkAnswerCheckTrue
            lblCheck.Text = "Y"
        Else
            pbCh.BackgroundImage = My.Resources.chkAnswerCheckFalse
            lblCheck.Text = "N"
        End If
    End Sub

    Private Sub pbCh1_Click(sender As System.Object, e As System.EventArgs) Handles pbCh1.Click
        CheckChoice(lblCheck1, pbCh1)
    End Sub

    Private Sub pbCh2_Click(sender As System.Object, e As System.EventArgs) Handles pbCh2.Click
        CheckChoice(lblCheck2, pbCh2)
    End Sub

    Private Sub pbCh3_Click(sender As System.Object, e As System.EventArgs) Handles pbCh3.Click
        CheckChoice(lblCheck3, pbCh3)
    End Sub

    Private Sub pbCh4_Click(sender As System.Object, e As System.EventArgs) Handles pbCh4.Click
        CheckChoice(lblCheck4, pbCh4)
    End Sub

    Private Sub lblBack_Click(sender As System.Object, e As System.EventArgs) Handles lblBack.Click

        If GetDatableTableFromTesting() = True Then
            frmMain.CloseAllChildForm()

            Dim f As New frmSelectTestCourse
            f.MdiParent = frmMain
            f.Show()
        End If
    End Sub
End Class
