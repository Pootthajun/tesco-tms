﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmQuestionDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmQuestionDialog))
        Me.lblCorrect = New System.Windows.Forms.Label()
        Me.lblIncorrect = New System.Windows.Forms.Label()
        Me.lblResultAnswer = New System.Windows.Forms.Label()
        Me.lblNext = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnClose = New System.Windows.Forms.PictureBox()
        Me.pbIcon = New System.Windows.Forms.PictureBox()
        Me.pbHeader = New System.Windows.Forms.PictureBox()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblCorrect
        '
        Me.lblCorrect.AutoSize = True
        Me.lblCorrect.BackColor = System.Drawing.Color.White
        Me.lblCorrect.Font = New System.Drawing.Font("Kittithada Medium 65 P", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCorrect.ForeColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(119, Byte), Integer))
        Me.lblCorrect.Location = New System.Drawing.Point(197, 186)
        Me.lblCorrect.Name = "lblCorrect"
        Me.lblCorrect.Size = New System.Drawing.Size(108, 34)
        Me.lblCorrect.TabIndex = 1
        Me.lblCorrect.Text = "คุณตอบถูก"
        '
        'lblIncorrect
        '
        Me.lblIncorrect.AutoSize = True
        Me.lblIncorrect.BackColor = System.Drawing.Color.White
        Me.lblIncorrect.Font = New System.Drawing.Font("Kittithada Medium 65 P", 20.25!, System.Drawing.FontStyle.Bold)
        Me.lblIncorrect.ForeColor = System.Drawing.Color.DarkGray
        Me.lblIncorrect.Location = New System.Drawing.Point(168, 108)
        Me.lblIncorrect.Name = "lblIncorrect"
        Me.lblIncorrect.Size = New System.Drawing.Size(172, 34)
        Me.lblIncorrect.TabIndex = 4
        Me.lblIncorrect.Text = "คำตอบที่ถูกต้องคือ"
        Me.lblIncorrect.Visible = False
        '
        'lblResultAnswer
        '
        Me.lblResultAnswer.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblResultAnswer.BackColor = System.Drawing.Color.White
        Me.lblResultAnswer.Font = New System.Drawing.Font("Kittithada Medium 65 P", 20.25!, System.Drawing.FontStyle.Bold)
        Me.lblResultAnswer.ForeColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(119, Byte), Integer))
        Me.lblResultAnswer.Location = New System.Drawing.Point(15, 150)
        Me.lblResultAnswer.Name = "lblResultAnswer"
        Me.lblResultAnswer.Size = New System.Drawing.Size(483, 32)
        Me.lblResultAnswer.TabIndex = 52
        Me.lblResultAnswer.Text = "เฉลย"
        Me.lblResultAnswer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblResultAnswer.Visible = False
        '
        'lblNext
        '
        Me.lblNext.BackColor = System.Drawing.Color.FromArgb(CType(CType(2, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(121, Byte), Integer))
        Me.lblNext.Font = New System.Drawing.Font("Kittithada Medium 65 F", 22.0!, System.Drawing.FontStyle.Bold)
        Me.lblNext.ForeColor = System.Drawing.Color.White
        Me.lblNext.Location = New System.Drawing.Point(181, 234)
        Me.lblNext.Name = "lblNext"
        Me.lblNext.Size = New System.Drawing.Size(148, 45)
        Me.lblNext.TabIndex = 54
        Me.lblNext.Text = "ต่อไป"
        Me.lblNext.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Location = New System.Drawing.Point(15, 92)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(485, 220)
        Me.Panel1.TabIndex = 55
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.Transparent
        Me.btnClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.Location = New System.Drawing.Point(478, 1)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(35, 33)
        Me.btnClose.TabIndex = 56
        Me.btnClose.TabStop = False
        '
        'pbIcon
        '
        Me.pbIcon.BackColor = System.Drawing.Color.White
        Me.pbIcon.BackgroundImage = Global.TESCO_TMS.My.Resources.Resources.icoCorrect
        Me.pbIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbIcon.Location = New System.Drawing.Point(218, 108)
        Me.pbIcon.Name = "pbIcon"
        Me.pbIcon.Size = New System.Drawing.Size(74, 73)
        Me.pbIcon.TabIndex = 2
        Me.pbIcon.TabStop = False
        '
        'pbHeader
        '
        Me.pbHeader.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.pbHeader.Image = Global.TESCO_TMS.My.Resources.Resources.bgTestingDialogCorrect
        Me.pbHeader.Location = New System.Drawing.Point(15, 15)
        Me.pbHeader.Name = "pbHeader"
        Me.pbHeader.Size = New System.Drawing.Size(485, 87)
        Me.pbHeader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbHeader.TabIndex = 53
        Me.pbHeader.TabStop = False
        '
        'frmQuestionDialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ClientSize = New System.Drawing.Size(515, 312)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.pbIcon)
        Me.Controls.Add(Me.pbHeader)
        Me.Controls.Add(Me.lblResultAnswer)
        Me.Controls.Add(Me.lblCorrect)
        Me.Controls.Add(Me.lblNext)
        Me.Controls.Add(Me.lblIncorrect)
        Me.Controls.Add(Me.Panel1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmQuestionDialog"
        Me.ShowInTaskbar = False
        Me.TransparencyKey = System.Drawing.Color.FromArgb(CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer))
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbIcon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblCorrect As System.Windows.Forms.Label
    Friend WithEvents pbIcon As System.Windows.Forms.PictureBox
    Friend WithEvents lblIncorrect As System.Windows.Forms.Label
    Friend WithEvents lblResultAnswer As System.Windows.Forms.Label
    Friend WithEvents pbHeader As System.Windows.Forms.PictureBox
    Friend WithEvents lblNext As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnClose As System.Windows.Forms.PictureBox
End Class
