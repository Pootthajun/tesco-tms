﻿Imports System.Data
Imports System.Data.OleDb
Public Class frmSelectCourse

    Public CoverColorName As Color
    Private Sub frmSelectCourse_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.WindowState = FormWindowState.Maximized
        Me.ControlBox = False
        AdjudeObject()
        DisplayCourseList()
    End Sub
    Private Sub AdjudeObject()
        If flpCourseList.Left + flpCourseList.Width > Me.Width Then
            flpCourseList.Width = Me.Width - flpCourseList.Left - 20
        End If
    End Sub

    Private Sub DisplayCourseList()
        Dim sql As String = "select id, title, description, icon_file "
        sql += " from tb_course "
        sql += " where 1=1 and department_id=" & CurrentDepartmentID
        sql += " order by title"
        Dim dt As DataTable = GetSqlDataTable(sql)
        If dt.Rows.Count > 0 Then
            flpCourseList.Controls.Clear()

            For Each dr As DataRow In dt.Rows
                Dim uc As New ucFunctionBox
                uc.DisplayImage(dr("id"), dr("title"), My.Resources.icon_course_book)
                AddHandler uc.FunctionBox_Click, AddressOf FunctionBox_Click

                flpCourseList.Controls.Add(uc)
                Application.DoEvents()
            Next
        End If
        dt.Dispose()
    End Sub

    Private Sub FunctionBox_Click(sender As ucFunctionBox, e As System.EventArgs)
        frmMain.CloseAllChildForm(Me)

        Dim f As New frmSelectCourseDetailDialog
        f.CourseID = sender.lblID.Text
        Plexiglass(f, Me.ParentForm)

    End Sub

    Private Sub UcButtonBack1_Click(sender As Object, e As System.EventArgs) Handles UcButtonBack1.Click
        frmMain.CloseAllChildForm(Me)

        Dim f As New frmSelectDepartment
        f.MdiParent = frmMain
        f.WindowState = FormWindowState.Maximized
        f.lblFormatName.Text = Me.lblFormatName.Text.Replace(">", "").Trim & " > "
        f.CoverColorName = CoverColorName
        f.lblFunctionName.Left = (f.lblFormatName.Left + f.lblFormatName.Width)
        f.lblFunctionName.Text = Me.lblFunctionName.Text.Replace(">", "").Trim
        f.Show()

        Me.Close()
    End Sub

    Private Sub lblFormatName_Click(sender As System.Object, e As System.EventArgs) Handles lblFormatName.Click
        frmMain.CloseAllChildForm(Me)

        Dim f As New frmSelectFunction
        f.MdiParent = frmMain
        f.WindowState = FormWindowState.Maximized
        'f.lblFormatName.Text = myUser.UserFormat.ToUpper
        f.Show()

        Me.Close()
    End Sub

    Private Sub lblFunctionName_Click(sender As System.Object, e As System.EventArgs) Handles lblFunctionName.Click
        UcButtonBack1_Click(Nothing, Nothing)
    End Sub
End Class