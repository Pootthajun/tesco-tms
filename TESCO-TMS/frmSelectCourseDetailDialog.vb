﻿Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.Data

Public Class frmSelectCourseDetailDialog

    Public CourseID As Int32
    'Dim DT_USER As New DataTable

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs)
        Me.Close()
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    

    Private Sub frmSelectCourseDetailDialog_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        
        Dim sql As String = "select id,title, description,icon_file,cover_file"
        sql += " from tb_course "
        sql += " where id=" & CourseID
        sql += " order by sort"
        Dim Temp As DataTable = GetSqlDataTable(sql)
        If Temp.Rows.Count > 0 Then
            lblTitle1.Text = Temp.Rows(0).Item("title").ToString
            'HtmlRichTextBox1.AddHTML(Temp.Rows(0).Item("description").ToString)
            'WebBrowser1.DocumentText = BuiltHtmlDocumentText(Temp.Rows(0).Item("description").ToString)
            WebBrowser1.DocumentText = Temp.Rows(0).Item("description").ToString
        End If
        Temp.Dispose()

    End Sub

    Private Sub btnRegister_Click(sender As System.Object, e As System.EventArgs) Handles btnRegister.Click
        btnRegister.Enabled = False
        pb1.Visible = True
        BindDocumentData(CourseID, pb1)
        Dim nform As New frmSelectCourseStudyDialog
        nform.CourseID = CourseID
        Plexiglass(nform, Me.ParentForm)
        btnRegister.Enabled = True
        pb1.Visible = False
        Me.Close()
        Me.DialogResult = Windows.Forms.DialogResult.OK

    End Sub

    Private Sub btnClose_Click_1(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub WebBrowser1_DocumentCompleted(sender As Object, e As System.Windows.Forms.WebBrowserDocumentCompletedEventArgs) Handles WebBrowser1.DocumentCompleted
        If WebBrowser1.Document.Body.ScrollRectangle.Height > WebBrowser1.Height Then
            WebBrowser1.ScrollBarsEnabled = True
        End If
    End Sub
End Class