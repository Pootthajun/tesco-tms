﻿Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.Data

Public Class frmSelectCourseStudyDialog

    Public CourseID As Int32
    Dim DT_USER As New DataTable

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Function CreateClass(stdID As String) As Long
        Dim ret As Long = 0
        Try
            Dim info As String = ""
            info = GetStringDataFromURL(WebServiceURL & "api/class/create", Token & "&client_id=" & myUser.ClientID & "&course_id=" & CourseID & "&user_id=" & myUser.UserID & "&student_id_list=" & stdID)

            Dim json As String = info
            Dim ser As JObject = JObject.Parse(json)
            Dim data As List(Of JToken) = ser.Children().ToList

            If data.Count = 3 Then
                If DirectCast(data(0), JProperty).Value.ToString.ToLower = "true" Then
                    ret = DirectCast(data(2), JProperty).Value
                End If
            End If
        Catch ex As Exception
            ret = 0
        End Try
        Return ret
    End Function

    

    Private Sub ListUser_RemoveListUser(RowIndex As Integer)
        DT_USER.Rows.RemoveAt(RowIndex)
        flpUser.Controls.Clear()
        If DT_USER.Rows.Count > 0 Then
            For i As Integer = DT_USER.Rows.Count - 1 To 0 Step -1
                'Dim UserID As String = DT_USER.Rows(i)("id")
                Dim FirstName As String = DT_USER.Rows(i)("firstname")
                Dim LastName As String = DT_USER.Rows(i)("lastname")
                Dim CompanyID As String = DT_USER.Rows(i)("company_id")

                Dim US As New ucStudyUser
                'US.Display(User(3), User(1), User(2))
                US.Display(CompanyID, FirstName, LastName, i)
                flpUser.Controls.Add(US)
                AddHandler US.RemoveListUser, AddressOf ListUser_RemoveListUser
            Next
        End If
    End Sub


    Private Sub btnCheckUser_Click(sender As System.Object, e As System.EventArgs) Handles btnCheckUser.Click
        If txtID.Text = "" Then
            txtID.Focus()
            MessageBox.Show("กรุณาใส่รหัสประจำตัว", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        Dim User(3) As String
        User = AddUser(txtID.Text, CourseID)
        If User(0) = "" Then
            txtID.Focus()
            MessageBox.Show("ไม่มีรหัสประจำตัวนี้", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        DT_USER.DefaultView.RowFilter = "id=" & User(0)
        If DT_USER.DefaultView.Count > 0 Then
            MessageBox.Show("รหัสประจำตัวนี้ได้มีการเพิ่มเข้าไปแล้ว", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        DT_USER.DefaultView.RowFilter = ""
        Dim DR = DT_USER.NewRow
        DR("id") = User(0)
        DR("firstname") = User(1)
        DR("lastname") = User(2)
        DR("company_id") = User(3)
        DR("cb") = "T"
        DT_USER.Rows.Add(DR)

        If DT_USER.Rows.Count > 0 Then
            flpUser.Controls.Clear()
            For i As Integer = DT_USER.Rows.Count - 1 To 0 Step -1
                'Dim UserID As String = DT_USER.Rows(i)("id")
                Dim FirstName As String = DT_USER.Rows(i)("firstname")
                Dim LastName As String = DT_USER.Rows(i)("lastname")
                Dim CompanyID As String = DT_USER.Rows(i)("company_id")

                Dim US As New ucStudyUser
                US.Display(CompanyID, FirstName, LastName, i)
                flpUser.Controls.Add(US)
                AddHandler US.RemoveListUser, AddressOf ListUser_RemoveListUser
            Next
        End If

        txtID.Text = ""
        txtID.Focus()
    End Sub

    Private Sub btnStudy_Click(sender As System.Object, e As System.EventArgs) Handles btnStudy.Click
        btnStudy.Enabled = False
        btnStudy.Cursor = Cursors.WaitCursor
        Dim stdID As String = ""
        If DT_USER.Rows.Count > 0 Then
            For Each drUser As DataRow In DT_USER.Rows
                If stdID = "" Then
                    stdID = drUser("id")
                Else
                    stdID += "," & drUser("id")
                End If
            Next
            'stdID = stdID 
        End If

        If stdID.Trim = "" Then
            btnStudy.Enabled = True
            btnStudy.Cursor = Cursors.Default
            MessageBox.Show("กรุณาเช็คชื่อผู้เข้าเรียน", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If


        Dim ClassID As Long = CreateClass(stdID)

        DT_DOCUMENT_ALL_FILE = New DataTable
        DT_DOCUMENT_ALL_FILE.Columns.Add("id")
        DT_DOCUMENT_ALL_FILE.Columns.Add("tb_course_id")
        DT_DOCUMENT_ALL_FILE.Columns.Add("document_id")
        DT_DOCUMENT_ALL_FILE.Columns.Add("file")
        DT_DOCUMENT_ALL_FILE.Columns.Add("file_path")
        DT_DOCUMENT_ALL_FILE.Columns.Add("sort")

        Dim sql As String = "select cdf.id, cdf.file_url, cdf.sort, cd.tb_course_id, cdf.file_path, cd.id as document_id "
        sql += " from tb_course_document_file cdf"
        sql += " inner join tb_course_document cd on cd.id=cdf.tb_course_document_id"
        sql += " where cd.tb_course_id=" & CourseID
        sql += " order by cd.sort,cdf.sort"
        Dim dt As DataTable = GetSqlDataTable(sql)
        If dt.Rows.Count > 0 Then
            FolderCourseDocumentFile = TempPath & "\CourseDocumentFile"
            pb1.Visible = True
            pb1.Maximum = dt.Rows.Count
            pb1.Value = 0
            Application.DoEvents()
            For i As Int32 = 0 To dt.Rows.Count - 1
                Dim drv As DataRow = dt.Rows(i)
                If drv("file_url").ToString <> "" Then
                    Dim FileNo As Integer = DT_DOCUMENT_ALL_FILE.Rows.Count + 1

                    Dim DR As DataRow
                    DR = DT_DOCUMENT_ALL_FILE.NewRow
                    DR("id") = drv("id").ToString
                    DR("file") = drv("file_url").ToString
                    If Convert.IsDBNull(drv("file_path")) = False Then DR("file_path") = drv("file_path").ToString
                    DR("sort") = FileNo
                    DR("document_id") = drv("document_id").ToString
                    DR("tb_course_id") = drv("tb_course_id").ToString
                    DT_DOCUMENT_ALL_FILE.Rows.Add(DR)

                    sql = "insert into TB_USER_STUDY_FILE (user_id,tb_course_document_file_id, file_no, file_url, file_path, tb_course_id, class_id,client_id,is_study)"
                    If myUser.IsLogIn = True Then
                        sql += " values('" & myUser.UserID & "'"
                    Else
                        sql += " values('1'"
                    End If
                    sql += ", '" & drv("id").ToString & "'"
                    sql += ", '" & FileNo & "'"
                    sql += ", '" & drv("file_url").ToString & "'"
                    sql += ", '" & drv("file_path").ToString & "'"
                    sql += ", '" & drv("tb_course_id").ToString & "'"
                    sql += ", '" & ClassID.ToString & "'"
                    sql += ", '" & myUser.ClientID & "'"
                    sql += ", 'N'"
                    sql += ")"

                    If ExecuteSqlNoneQuery(sql) = False Then
                        btnStudy.Enabled = True
                        btnStudy.Cursor = Cursors.Default
                        MessageBox.Show("Cannot access Database please try agen later", "Connection Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Exit Sub
                    End If
                End If
                pb1.Value += 1
            Next

            If DT_DOCUMENT_ALL_FILE.Rows.Count = 0 Then
                btnStudy.Enabled = True
                btnStudy.Cursor = Cursors.Default
                MessageBox.Show("File not found", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If

            'Me.Close()
            'frmMain.CloseAllChildForm()
            ShowFormDocumentAllFile(dt.Rows(0).Item("sort"), ClassID)
            pb1.Value = 0
            pb1.Visible = False
            btnStudy.Enabled = True
            btnStudy.Cursor = Cursors.Default
        Else
            btnStudy.Enabled = True
            btnStudy.Cursor = Cursors.Default
            MessageBox.Show("File not found", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
        dt.Dispose()

    End Sub

    Private Sub frmSelectCourseStudyDialog_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim sql As String = "select id,title, description,icon_file,cover_file"
        sql += " from tb_course "
        sql += " where id=" & CourseID
        sql += " order by sort"
        Dim Temp As DataTable = GetSqlDataTable(sql)
        If Temp.Rows.Count > 0 Then
            lblTitle1.Text = Temp.Rows(0).Item("title").ToString


            'แสดงข้อมูลแท็บ ผู้เข้าเรียน
            Dim ImageIcon As Image = Nothing
            If Temp.Rows(0)("icon_file").ToString.Trim <> "" Then
                ImageIcon = Image.FromFile(Temp.Rows(0)("icon_file"))
            End If

            Dim ImageCover As Image = Nothing
            If Temp.Rows(0)("cover_file").ToString.Trim <> "" Then
                ImageCover = Image.FromFile(Temp.Rows(0)("cover_file"))
            End If
            DT_USER.Columns.Add("id", System.Type.GetType("System.Int32"))
            DT_USER.Columns.Add("firstname")
            DT_USER.Columns.Add("lastname")
            DT_USER.Columns.Add("company_id")
            DT_USER.Columns.Add("cb")
        End If
        Temp.Dispose()

    End Sub

    Private Sub txtID_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtID.KeyPress
        If Asc(e.KeyChar) = 13 Then
            btnCheckUser_Click(Nothing, Nothing)
        End If
    End Sub


    

End Class