﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSelectDepartment
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSelectDepartment))
        Me.lblFormatName = New System.Windows.Forms.Label()
        Me.flpFunctionList = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblFunctionName = New System.Windows.Forms.Label()
        Me.UcButtonBack1 = New TESCO_TMS.ucButtonBack()
        Me.SuspendLayout()
        '
        'lblFormatName
        '
        Me.lblFormatName.AutoSize = True
        Me.lblFormatName.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lblFormatName.Font = New System.Drawing.Font("Kittithada Medium 65 P", 26.0!, System.Drawing.FontStyle.Bold)
        Me.lblFormatName.ForeColor = System.Drawing.Color.FromArgb(CType(CType(90, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(143, Byte), Integer))
        Me.lblFormatName.Location = New System.Drawing.Point(41, 47)
        Me.lblFormatName.Name = "lblFormatName"
        Me.lblFormatName.Size = New System.Drawing.Size(99, 43)
        Me.lblFormatName.TabIndex = 27
        Me.lblFormatName.Text = "EXTRA"
        Me.lblFormatName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'flpFunctionList
        '
        Me.flpFunctionList.AutoScroll = True
        Me.flpFunctionList.Location = New System.Drawing.Point(145, 117)
        Me.flpFunctionList.Name = "flpFunctionList"
        Me.flpFunctionList.Size = New System.Drawing.Size(1103, 616)
        Me.flpFunctionList.TabIndex = 28
        '
        'lblFunctionName
        '
        Me.lblFunctionName.AutoSize = True
        Me.lblFunctionName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFunctionName.Font = New System.Drawing.Font("Kittithada Medium 65 P", 26.0!, System.Drawing.FontStyle.Bold)
        Me.lblFunctionName.ForeColor = System.Drawing.Color.White
        Me.lblFunctionName.Location = New System.Drawing.Point(146, 47)
        Me.lblFunctionName.Name = "lblFunctionName"
        Me.lblFunctionName.Size = New System.Drawing.Size(99, 43)
        Me.lblFunctionName.TabIndex = 29
        Me.lblFunctionName.Text = "EXTRA"
        Me.lblFunctionName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UcButtonBack1
        '
        Me.UcButtonBack1.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.UcButtonBack1.BackColor = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(121, Byte), Integer))
        Me.UcButtonBack1.BackgroundImage = CType(resources.GetObject("UcButtonBack1.BackgroundImage"), System.Drawing.Image)
        Me.UcButtonBack1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.UcButtonBack1.Location = New System.Drawing.Point(3, 377)
        Me.UcButtonBack1.Name = "UcButtonBack1"
        Me.UcButtonBack1.Size = New System.Drawing.Size(35, 110)
        Me.UcButtonBack1.TabIndex = 0
        '
        'frmSelectDepartment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(1292, 768)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblFunctionName)
        Me.Controls.Add(Me.UcButtonBack1)
        Me.Controls.Add(Me.flpFunctionList)
        Me.Controls.Add(Me.lblFormatName)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmSelectDepartment"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblFormatName As System.Windows.Forms.Label
    Friend WithEvents flpFunctionList As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents UcButtonBack1 As TESCO_TMS.ucButtonBack
    Friend WithEvents lblFunctionName As System.Windows.Forms.Label
End Class
