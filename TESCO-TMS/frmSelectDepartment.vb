﻿Imports System.Data
Imports System.Data.OleDb
Public Class frmSelectDepartment
    Public CoverColorName As Color
    Private Sub frmSelectFormat_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.WindowState = FormWindowState.Maximized
        Me.ControlBox = False
        CurrentDepartmentID = 0
        AdjudeObject()
        DisplayDepartmentList()
    End Sub

    Private Sub AdjudeObject()
        If flpFunctionList.Left + flpFunctionList.Width > Me.Width Then
            flpFunctionList.Width = Me.Width - flpFunctionList.Left - 20
        End If
    End Sub

    Private Sub DisplayDepartmentList()
        If myUser.UserDepartment.Rows.Count > 0 Then
            myUser.UserDepartment.DefaultView.RowFilter = "function_id=" & CurrentFunctionID

            Dim dt As New DataTable
            dt = myUser.UserDepartment.DefaultView.ToTable().Copy
            For Each dr As DataRow In dt.Rows
                Dim uc As New ucFunctionBox
                Dim bgColor As Color = CoverColorName
                Dim sql As String = "select id from TB_COURSE where department_id=" & dr("department_id")
                Dim cdt As DataTable = GetSqlDataTable(sql)
                If cdt.Rows.Count = 0 Then
                    bgColor = ColorTranslator.FromHtml("Gray")
                    uc.Cursor = Cursors.Default
                Else
                    AddHandler uc.FunctionBox_Click, AddressOf FunctionBox_Click
                End If

                Dim dTitle As String = dr("department_title") & " (" & cdt.Rows.Count & ")"
                uc.Display(dr("department_id"), dTitle, dr("department_cover"), bgColor, dr("format_title"))
                flpFunctionList.Controls.Add(uc)

                cdt.Dispose()
                Application.DoEvents()
            Next
        End If
    End Sub

    Private Sub FunctionBox_Click(sender As ucFunctionBox, e As System.EventArgs)
        Dim FuncationName As String = Me.lblFunctionName.Text.Trim
        Dim DepartmentName As String = Split(sender.lblTitle.Text.ToUpper, "(")(0)

        frmMain.CloseAllChildForm()
        CurrentDepartmentID = sender.lblID.Text

        Dim f As New frmSelectCourse
        f.MdiParent = frmMain
        f.WindowState = FormWindowState.Maximized
        f.lblFormatName.Text = myUser.UserDefaultFormat & " > "
        f.CoverColorName = CoverColorName

        f.lblFunctionName.Left = (f.lblFormatName.Left + f.lblFormatName.Width)
        f.lblFunctionName.Text = FuncationName & " > "

        f.lblDepartment.Left = (f.lblFunctionName.Left + f.lblFunctionName.Width)
        f.lblDepartment.Text = DepartmentName
        f.Show()

        'Me.Close()
    End Sub



    Private Sub UcButtonBack1_Click(sender As Object, e As System.EventArgs) Handles UcButtonBack1.Click
        frmMain.CloseAllChildForm()

        Dim f As New frmSelectFunction
        f.MdiParent = frmMain
        f.WindowState = FormWindowState.Maximized
        f.lblFormatName.Text = myUser.UserDefaultFormat
        f.Show()

        'Me.Close()
    End Sub

    Private Sub lblFormatName_Click(sender As System.Object, e As System.EventArgs) Handles lblFormatName.Click
        UcButtonBack1_Click(Nothing, Nothing)
    End Sub
End Class