﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSelectFormat
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCourse = New System.Windows.Forms.Label()
        Me.flp = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblWelcome4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.pbExtra = New System.Windows.Forms.PictureBox()
        Me.pbTalad = New System.Windows.Forms.PictureBox()
        Me.pbExpress = New System.Windows.Forms.PictureBox()
        Me.pbDC = New System.Windows.Forms.PictureBox()
        Me.pbHeadOffice = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbExtra, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbTalad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbExpress, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbDC, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbHeadOffice, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCourse
        '
        Me.btnCourse.BackColor = System.Drawing.Color.FromArgb(CType(CType(2, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(121, Byte), Integer))
        Me.btnCourse.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnCourse.Font = New System.Drawing.Font("Kittithada Bold 75 P", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnCourse.ForeColor = System.Drawing.Color.White
        Me.btnCourse.Location = New System.Drawing.Point(410, 644)
        Me.btnCourse.Name = "btnCourse"
        Me.btnCourse.Size = New System.Drawing.Size(213, 55)
        Me.btnCourse.TabIndex = 32
        Me.btnCourse.Text = "ถัดไป"
        Me.btnCourse.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'flp
        '
        Me.flp.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.flp.AutoScroll = True
        Me.flp.BackColor = System.Drawing.Color.FromArgb(CType(CType(43, Byte), Integer), CType(CType(67, Byte), Integer), CType(CType(84, Byte), Integer))
        Me.flp.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.flp.Location = New System.Drawing.Point(589, 231)
        Me.flp.Name = "flp"
        Me.flp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.flp.Size = New System.Drawing.Size(436, 394)
        Me.flp.TabIndex = 20
        '
        'lblWelcome4
        '
        Me.lblWelcome4.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblWelcome4.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblWelcome4.Font = New System.Drawing.Font("Kittithada Roman 55 F", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblWelcome4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(219, Byte), Integer))
        Me.lblWelcome4.Location = New System.Drawing.Point(589, 188)
        Me.lblWelcome4.Name = "lblWelcome4"
        Me.lblWelcome4.Size = New System.Drawing.Size(434, 41)
        Me.lblWelcome4.TabIndex = 16
        Me.lblWelcome4.Text = "ความเคลื่อนไหววันนี้..."
        Me.lblWelcome4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Kittithada Medium 65 F", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(93, 190)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(358, 39)
        Me.Label2.TabIndex = 22
        Me.Label2.Text = "เลือกฟอร์แมท (FORMAT) ของคุณ"
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.Image = Global.TESCO_TMS.My.Resources.Resources.bgSelectFormatSplitVertical
        Me.PictureBox1.Location = New System.Drawing.Point(510, 197)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(12, 428)
        Me.PictureBox1.TabIndex = 31
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.TESCO_TMS.My.Resources.Resources.bgSelectFormatHeader
        Me.PictureBox2.Location = New System.Drawing.Point(294, 75)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(440, 85)
        Me.PictureBox2.TabIndex = 25
        Me.PictureBox2.TabStop = False
        '
        'pbExtra
        '
        Me.pbExtra.BackgroundImage = Global.TESCO_TMS.My.Resources.Resources.extra_green
        Me.pbExtra.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbExtra.Location = New System.Drawing.Point(174, 259)
        Me.pbExtra.Name = "pbExtra"
        Me.pbExtra.Size = New System.Drawing.Size(177, 55)
        Me.pbExtra.TabIndex = 33
        Me.pbExtra.TabStop = False
        '
        'pbTalad
        '
        Me.pbTalad.BackgroundImage = Global.TESCO_TMS.My.Resources.Resources.talad_gray
        Me.pbTalad.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbTalad.Location = New System.Drawing.Point(174, 335)
        Me.pbTalad.Name = "pbTalad"
        Me.pbTalad.Size = New System.Drawing.Size(177, 55)
        Me.pbTalad.TabIndex = 34
        Me.pbTalad.TabStop = False
        '
        'pbExpress
        '
        Me.pbExpress.BackgroundImage = Global.TESCO_TMS.My.Resources.Resources.express_gray
        Me.pbExpress.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbExpress.Location = New System.Drawing.Point(174, 412)
        Me.pbExpress.Name = "pbExpress"
        Me.pbExpress.Size = New System.Drawing.Size(177, 55)
        Me.pbExpress.TabIndex = 35
        Me.pbExpress.TabStop = False
        '
        'pbDC
        '
        Me.pbDC.BackgroundImage = Global.TESCO_TMS.My.Resources.Resources.dc_gray
        Me.pbDC.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbDC.Location = New System.Drawing.Point(174, 489)
        Me.pbDC.Name = "pbDC"
        Me.pbDC.Size = New System.Drawing.Size(177, 55)
        Me.pbDC.TabIndex = 36
        Me.pbDC.TabStop = False
        '
        'pbHeadOffice
        '
        Me.pbHeadOffice.BackgroundImage = Global.TESCO_TMS.My.Resources.Resources.ho_gray
        Me.pbHeadOffice.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbHeadOffice.Location = New System.Drawing.Point(174, 565)
        Me.pbHeadOffice.Name = "pbHeadOffice"
        Me.pbHeadOffice.Size = New System.Drawing.Size(177, 55)
        Me.pbHeadOffice.TabIndex = 37
        Me.pbHeadOffice.TabStop = False
        '
        'frmSelectFormat
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(1036, 768)
        Me.ControlBox = False
        Me.Controls.Add(Me.pbHeadOffice)
        Me.Controls.Add(Me.pbDC)
        Me.Controls.Add(Me.pbExpress)
        Me.Controls.Add(Me.pbTalad)
        Me.Controls.Add(Me.pbExtra)
        Me.Controls.Add(Me.btnCourse)
        Me.Controls.Add(Me.flp)
        Me.Controls.Add(Me.lblWelcome4)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.Label2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmSelectFormat"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbExtra, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbTalad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbExpress, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbDC, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbHeadOffice, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCourse As System.Windows.Forms.Label
    Friend WithEvents flp As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblWelcome4 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents pbExtra As System.Windows.Forms.PictureBox
    Friend WithEvents pbTalad As System.Windows.Forms.PictureBox
    Friend WithEvents pbExpress As System.Windows.Forms.PictureBox
    Friend WithEvents pbDC As System.Windows.Forms.PictureBox
    Friend WithEvents pbHeadOffice As System.Windows.Forms.PictureBox
End Class
