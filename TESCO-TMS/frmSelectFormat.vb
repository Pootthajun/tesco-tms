﻿Imports System.Data
Imports System.Data.OleDb
Public Class frmSelectFormat

    Private Sub frmSelectFormat_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
 
        SetFormatButton()
        SetWelcomeMessage()

        Me.WindowState = FormWindowState.Maximized
        Me.ControlBox = False
        AdjudeObject()
        Application.DoEvents()
    End Sub

    Private Sub AdjudeObject()
        PictureBox1.Left = (Me.Width / 2) - (PictureBox1.Width / 2)   'เสา
        PictureBox2.Left = (Me.Width / 2) - (PictureBox2.Width / 2)   'ยินดีต้อนรับสู่
        btnCourse.Left = (Me.Width / 2) - (btnCourse.Width / 2)  'ปุ่มถัดไป

        Label2.Left = PictureBox1.Left - (Label2.Width + 60)
        pbExtra.Left = PictureBox1.Left - (pbExtra.Width + 160)
        pbTalad.Left = PictureBox1.Left - (pbTalad.Width + 160)
        pbExpress.Left = PictureBox1.Left - (pbExpress.Width + 160)
        pbDC.Left = PictureBox1.Left - (pbDC.Width + 160)
        pbHeadOffice.Left = PictureBox1.Left - (pbHeadOffice.Width + 160)


        lblWelcome4.Left = PictureBox1.Left + PictureBox1.Width + 70
        flp.Left = lblWelcome4.Left
    End Sub

    Private Sub SetWelcomeMessage()
        Try
            If myUser.UserMessage.Rows.Count > 0 Then
                For i As Int32 = 0 To myUser.UserMessage.Rows.Count - 1
                    Dim dr As DataRow = myUser.UserMessage.Rows(i)
                    Dim Form As New WelcomeMessage
                    Form.Display(dr("name").ToString, dr("description").ToString)
                    flp.Controls.Add(Form)
                    Application.DoEvents()
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SetFormatButton()
        'pbExtra.BackgroundImage = My.Resources.extra_gray
        'pbTalad.BackgroundImage = My.Resources.talad_gray
        'pbExpress.BackgroundImage = My.Resources.express_gray
        'pbDC.BackgroundImage = My.Resources.dc_gray
        'pbHeadOffice.BackgroundImage = My.Resources.ho_gray
        ClearStatusFormatButton()

        If myUser.UserFormat.Rows.Count > 0 Then
            myUser.UserFormat.DefaultView.RowFilter = "format_title like '%EXTRA%'"
            If myUser.UserFormat.DefaultView.Count > 0 Then
                pbExtra.BackgroundImage = My.Resources.extra_green
                myUser.UserDefaultFormat = myUser.UserFormat.DefaultView(0)("format_title").ToString.ToUpper
                Exit Sub
            End If
            myUser.UserFormat.DefaultView.RowFilter = ""

            myUser.UserFormat.DefaultView.RowFilter = "format_title like '%TALAD%'"
            If myUser.UserFormat.DefaultView.Count > 0 Then
                pbTalad.BackgroundImage = My.Resources.talad_green
                myUser.UserDefaultFormat = myUser.UserFormat.DefaultView(0)("format_title").ToString.ToUpper
                Exit Sub
            End If
            myUser.UserFormat.DefaultView.RowFilter = ""

            myUser.UserFormat.DefaultView.RowFilter = "format_title like '%EXPRESS%'"
            If myUser.UserFormat.DefaultView.Count > 0 Then
                pbExpress.BackgroundImage = My.Resources.express_green
                myUser.UserDefaultFormat = myUser.UserFormat.DefaultView(0)("format_title").ToString.ToUpper
                Exit Sub
            End If
            myUser.UserFormat.DefaultView.RowFilter = ""

            myUser.UserFormat.DefaultView.RowFilter = "format_title like '%DC%'"
            If myUser.UserFormat.DefaultView.Count > 0 Then
                pbDC.BackgroundImage = My.Resources.dc_green
                myUser.UserDefaultFormat = myUser.UserFormat.DefaultView(0)("format_title").ToString.ToUpper
                Exit Sub
            End If
            myUser.UserFormat.DefaultView.RowFilter = ""

            myUser.UserFormat.DefaultView.RowFilter = "format_title like '%HEAD%'"
            If myUser.UserFormat.DefaultView.Count > 0 Then
                pbHeadOffice.BackgroundImage = My.Resources.ho_green
                myUser.UserDefaultFormat = myUser.UserFormat.DefaultView(0)("format_title").ToString.ToUpper
                Exit Sub
            End If
            myUser.UserFormat.DefaultView.RowFilter = ""


            'If myUser.UserFormat.Rows(0)("format_title").ToString.ToUpper.IndexOf("EXTRA") > -1 Then
            '    pbExtra.BackgroundImage = My.Resources.extra_green
            'ElseIf myUser.UserFormat.Rows(0)("format_title").ToString.ToUpper.IndexOf("TALAD") > -1 Then
            '    pbTalad.BackgroundImage = My.Resources.talad_green
            'ElseIf myUser.UserFormat.Rows(0)("format_title").ToString.ToUpper.IndexOf("EXPRESS") > -1 Then
            '    pbExpress.BackgroundImage = My.Resources.express_green
            'ElseIf myUser.UserFormat.Rows(0)("format_title").ToString.ToUpper.IndexOf("DC") > -1 Then
            '    pbDC.BackgroundImage = My.Resources.dc_green
            'ElseIf myUser.UserFormat.Rows(0)("format_title").ToString.ToUpper.IndexOf("HEAD") > -1 Then
            '    pbHeadOffice.BackgroundImage = My.Resources.ho_green
            'End If
        End If
    End Sub

    Private Sub btnCourse_Click(sender As System.Object, e As System.EventArgs) Handles btnCourse.Click
        frmMain.CloseAllChildForm()
        Me.Close()

        Dim f As New frmSelectFunction
        f.MdiParent = frmMain
        f.WindowState = FormWindowState.Maximized
        f.lblFormatName.Text = myUser.UserDefaultFormat
        f.Show()
    End Sub

    Private Sub ClearStatusFormatButton()
        pbExtra.BackgroundImage = My.Resources.extra_gray
        pbTalad.BackgroundImage = My.Resources.talad_gray
        pbExpress.BackgroundImage = My.Resources.express_gray
        pbDC.BackgroundImage = My.Resources.dc_gray
        pbHeadOffice.BackgroundImage = My.Resources.ho_gray
    End Sub

    Private Sub pbExtra_Click(sender As Object, e As System.EventArgs) Handles pbExtra.Click
        If pbExtra.Cursor = Cursors.Hand Then
            ClearStatusFormatButton()
            pbExtra.BackgroundImage = My.Resources.extra_green

            myUser.UserFormat.DefaultView.RowFilter = "format_title like '%EXTRA%'"
            myUser.UserDefaultFormat = myUser.UserFormat.DefaultView(0)("format_title")
            myUser.UserFormat.DefaultView.RowFilter = ""
        End If
    End Sub

    Private Sub pbExtra_MouseHover(sender As Object, e As System.EventArgs) Handles pbExtra.MouseHover
        myUser.UserFormat.DefaultView.RowFilter = "format_title like '%EXTRA%'"
        If myUser.UserFormat.DefaultView.Count > 0 Then
            pbExtra.Cursor = Cursors.Hand
        End If
        myUser.UserFormat.DefaultView.RowFilter = ""
    End Sub

    Private Sub pbDC_Click(sender As Object, e As System.EventArgs) Handles pbDC.Click
        If pbDC.Cursor = Cursors.Hand Then
            ClearStatusFormatButton()
            pbDC.BackgroundImage = My.Resources.dc_green

            myUser.UserFormat.DefaultView.RowFilter = "format_title like '%DC%'"
            myUser.UserDefaultFormat = myUser.UserFormat.DefaultView(0)("format_title")
            myUser.UserFormat.DefaultView.RowFilter = ""
        End If
    End Sub

    Private Sub pbDC_MouseHover(sender As Object, e As System.EventArgs) Handles pbDC.MouseHover
        myUser.UserFormat.DefaultView.RowFilter = "format_title like '%DC%'"
        If myUser.UserFormat.DefaultView.Count > 0 Then
            pbDC.Cursor = Cursors.Hand
        End If
        myUser.UserFormat.DefaultView.RowFilter = ""
    End Sub

    Private Sub pbExpress_Click(sender As Object, e As System.EventArgs) Handles pbExpress.Click
        If pbExpress.Cursor = Cursors.Hand Then
            ClearStatusFormatButton()
            pbExpress.BackgroundImage = My.Resources.express_green

            myUser.UserFormat.DefaultView.RowFilter = "format_title like '%EXPRESS%'"
            myUser.UserDefaultFormat = myUser.UserFormat.DefaultView(0)("format_title")
            myUser.UserFormat.DefaultView.RowFilter = ""
        End If
    End Sub

    Private Sub pbExpress_MouseHover(sender As Object, e As System.EventArgs) Handles pbExpress.MouseHover
        myUser.UserFormat.DefaultView.RowFilter = "format_title like '%EXPRESS%'"
        If myUser.UserFormat.DefaultView.Count > 0 Then
            pbExpress.Cursor = Cursors.Hand
        End If
        myUser.UserFormat.DefaultView.RowFilter = ""
    End Sub

    Private Sub pbHeadOffice_Click(sender As Object, e As System.EventArgs) Handles pbHeadOffice.Click
        If pbHeadOffice.Cursor = Cursors.Hand Then
            ClearStatusFormatButton()
            pbHeadOffice.BackgroundImage = My.Resources.ho_green

            myUser.UserFormat.DefaultView.RowFilter = "format_title like '%HEAD%'"
            myUser.UserDefaultFormat = myUser.UserFormat.DefaultView(0)("format_title")
            myUser.UserFormat.DefaultView.RowFilter = ""
        End If
    End Sub

    Private Sub pbHeadOffice_MouseHover(sender As Object, e As System.EventArgs) Handles pbHeadOffice.MouseHover
        myUser.UserFormat.DefaultView.RowFilter = "format_title like '%HEAD%'"
        If myUser.UserFormat.DefaultView.Count > 0 Then
            pbHeadOffice.Cursor = Cursors.Hand
            myUser.UserDefaultFormat = myUser.UserFormat.DefaultView(0)("format_title")
        End If
        myUser.UserFormat.DefaultView.RowFilter = ""
    End Sub

    Private Sub pbTalad_Click(sender As Object, e As System.EventArgs) Handles pbTalad.Click
        If pbTalad.Cursor = Cursors.Hand Then
            ClearStatusFormatButton()
            pbTalad.BackgroundImage = My.Resources.talad_green

            myUser.UserFormat.DefaultView.RowFilter = "format_title like '%TALAD%'"
            myUser.UserDefaultFormat = myUser.UserFormat.DefaultView(0)("format_title")
            myUser.UserFormat.DefaultView.RowFilter = ""
        End If
    End Sub

    Private Sub pbTalad_MouseHover(sender As Object, e As System.EventArgs) Handles pbTalad.MouseHover
        myUser.UserFormat.DefaultView.RowFilter = "format_title like '%TALAD%'"
        If myUser.UserFormat.DefaultView.Count > 0 Then
            pbTalad.Cursor = Cursors.Hand
        End If
        myUser.UserFormat.DefaultView.RowFilter = ""
    End Sub
End Class