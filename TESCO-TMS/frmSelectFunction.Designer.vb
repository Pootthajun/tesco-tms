﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSelectFunction
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSelectFunction))
        Me.lblFormatName = New System.Windows.Forms.Label()
        Me.flpFunctionList = New System.Windows.Forms.FlowLayoutPanel()
        Me.flpAdditionalList = New System.Windows.Forms.FlowLayoutPanel()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LineShape2 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.UcButtonBack1 = New TESCO_TMS.ucButtonBack()
        Me.SuspendLayout()
        '
        'lblFormatName
        '
        Me.lblFormatName.AutoSize = True
        Me.lblFormatName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFormatName.Font = New System.Drawing.Font("Kittithada Medium 65 P", 26.0!, System.Drawing.FontStyle.Bold)
        Me.lblFormatName.ForeColor = System.Drawing.Color.FromArgb(CType(CType(90, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(143, Byte), Integer))
        Me.lblFormatName.Location = New System.Drawing.Point(41, 46)
        Me.lblFormatName.Name = "lblFormatName"
        Me.lblFormatName.Size = New System.Drawing.Size(205, 43)
        Me.lblFormatName.TabIndex = 27
        Me.lblFormatName.Text = "Select Function"
        Me.lblFormatName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'flpFunctionList
        '
        Me.flpFunctionList.AutoScroll = True
        Me.flpFunctionList.Location = New System.Drawing.Point(145, 106)
        Me.flpFunctionList.Name = "flpFunctionList"
        Me.flpFunctionList.Size = New System.Drawing.Size(1103, 457)
        Me.flpFunctionList.TabIndex = 28
        '
        'flpAdditionalList
        '
        Me.flpAdditionalList.AutoScroll = True
        Me.flpAdditionalList.Location = New System.Drawing.Point(145, 597)
        Me.flpAdditionalList.Name = "flpAdditionalList"
        Me.flpAdditionalList.Size = New System.Drawing.Size(1103, 159)
        Me.flpAdditionalList.TabIndex = 29
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape2})
        Me.ShapeContainer1.Size = New System.Drawing.Size(1036, 768)
        Me.ShapeContainer1.TabIndex = 30
        Me.ShapeContainer1.TabStop = False
        '
        'LineShape2
        '
        Me.LineShape2.BorderColor = System.Drawing.SystemColors.ButtonFace
        Me.LineShape2.BorderWidth = 5
        Me.LineShape2.Name = "LineShape2"
        Me.LineShape2.X1 = 144
        Me.LineShape2.X2 = 1247
        Me.LineShape2.Y1 = 572
        Me.LineShape2.Y2 = 572
        '
        'UcButtonBack1
        '
        Me.UcButtonBack1.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.UcButtonBack1.BackColor = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(121, Byte), Integer))
        Me.UcButtonBack1.BackgroundImage = CType(resources.GetObject("UcButtonBack1.BackgroundImage"), System.Drawing.Image)
        Me.UcButtonBack1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.UcButtonBack1.Location = New System.Drawing.Point(3, 377)
        Me.UcButtonBack1.Name = "UcButtonBack1"
        Me.UcButtonBack1.Size = New System.Drawing.Size(35, 110)
        Me.UcButtonBack1.TabIndex = 0
        '
        'frmSelectFunction
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(1036, 768)
        Me.ControlBox = False
        Me.Controls.Add(Me.flpAdditionalList)
        Me.Controls.Add(Me.UcButtonBack1)
        Me.Controls.Add(Me.lblFormatName)
        Me.Controls.Add(Me.flpFunctionList)
        Me.Controls.Add(Me.ShapeContainer1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmSelectFunction"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblFormatName As System.Windows.Forms.Label
    Friend WithEvents flpFunctionList As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents UcButtonBack1 As TESCO_TMS.ucButtonBack
    Friend WithEvents flpAdditionalList As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents LineShape2 As Microsoft.VisualBasic.PowerPacks.LineShape
End Class
