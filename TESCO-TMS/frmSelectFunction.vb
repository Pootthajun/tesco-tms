﻿Imports System.Data
Imports System.Data.OleDb
Public Class frmSelectFunction

    Private Sub frmSelectFunction_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.WindowState = FormWindowState.Maximized
        Me.ControlBox = False
        CurrentFunctionID = 0


        AdjudeObject()
        DisplayFunctionList()
    End Sub

    Private Sub AdjudeObject()
        If flpFunctionList.Left + flpFunctionList.Width > Me.Width Then
            flpFunctionList.Width = Me.Width - flpFunctionList.Left - 20
        End If

        If flpAdditionalList.Left + flpAdditionalList.Width > Me.Width Then
            flpAdditionalList.Width = Me.Width - flpAdditionalList.Left - 20
        End If
    End Sub

    Private Sub DisplayFunctionList()
        If myUser.UserFunction.Rows.Count > 0 Then
            myUser.UserFunction.DefaultView.RowFilter = "format_title='" & myUser.UserDefaultFormat & "'"
            For Each dr As DataRowView In myUser.UserFunction.DefaultView
                Dim uc As New ucFunctionBox

                Dim col As Color = Color.Transparent
                If dr("function_cover_color") <> "" Then
                    col = ColorTranslator.FromHtml(dr("function_cover_color"))
                End If

                uc.Display(dr("function_id"), dr("function_title"), dr("function_cover"), col, dr("format_title"))
                AddHandler uc.FunctionBox_Click, AddressOf FunctionBox_Click

                If dr("function_subject") = "main subject" Then
                    flpFunctionList.Controls.Add(uc)
                ElseIf dr("function_subject") = "additional subject" Then
                    flpAdditionalList.Controls.Add(uc)
                End If

                Application.DoEvents()
            Next
            myUser.UserFunction.DefaultView.RowFilter = ""
        End If
    End Sub

    Private Sub FunctionBox_Click(sender As ucFunctionBox, e As System.EventArgs)
        frmMain.CloseAllChildForm(Me)
        CurrentFunctionID = sender.lblID.Text

        Dim f As New frmSelectDepartment
        f.MdiParent = frmMain
        f.CoverColorName = sender.BackColor
        f.lblFormatName.Text = sender.lblFormatName.Text.Trim & " > "

        f.lblFunctionName.Left = (f.lblFormatName.Left + f.lblFormatName.Width)
        f.lblFunctionName.Text = sender.lblTitle.Text.ToUpper

        f.Show()

        'Me.Close()
    End Sub

    'Sub CloseAllChildForm()
    '    For i As Integer = My.Application.OpenForms.Count - 1 To 0 Step -1
    '        If My.Application.OpenForms.Item(i) IsNot Me Then
    '            My.Application.OpenForms.Item(i).Close()
    '        End If
    '    Next i
    'End Sub

    Private Sub UcButtonBack1_Click(sender As Object, e As System.EventArgs) Handles UcButtonBack1.Click
        frmMain.CloseAllChildForm()
        Dim f As New frmSelectFormat
        f.MdiParent = frmMain
        f.Show()
        buttonClick = True
        frmMain.lbl_H_Username.Text = myUser.Name
        'Me.Close()
    End Sub

End Class