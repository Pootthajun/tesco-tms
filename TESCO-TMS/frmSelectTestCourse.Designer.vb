﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSelectTestCourse
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblWelcome4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.flpTestCourse = New System.Windows.Forms.FlowLayoutPanel()
        Me.pnlStatistict = New System.Windows.Forms.Panel()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.lblTestPassTotal = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.lblTestedComplete = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lblTestTotal = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.lblTestedAttempt = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblCourseTotal = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblCourseComplete = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlStatistict.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblWelcome4
        '
        Me.lblWelcome4.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblWelcome4.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblWelcome4.Font = New System.Drawing.Font("Kittithada Roman 55 F", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblWelcome4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(219, Byte), Integer))
        Me.lblWelcome4.Location = New System.Drawing.Point(616, 200)
        Me.lblWelcome4.Name = "lblWelcome4"
        Me.lblWelcome4.Size = New System.Drawing.Size(396, 41)
        Me.lblWelcome4.TabIndex = 16
        Me.lblWelcome4.Text = "สถิติเบื้องต้นของคุณ"
        Me.lblWelcome4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Kittithada Medium 65 F", 24.0!)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(159, 190)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(213, 39)
        Me.Label2.TabIndex = 22
        Me.Label2.Text = "กรุณาเลือกบททดสอบ"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.TESCO_TMS.My.Resources.Resources.bgSelectFormatSplitVertical
        Me.PictureBox1.Location = New System.Drawing.Point(512, 197)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(12, 428)
        Me.PictureBox1.TabIndex = 31
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.TESCO_TMS.My.Resources.Resources.bgSelectTestCourseHeader
        Me.PictureBox2.Location = New System.Drawing.Point(355, 78)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(322, 77)
        Me.PictureBox2.TabIndex = 25
        Me.PictureBox2.TabStop = False
        '
        'flpTestCourse
        '
        Me.flpTestCourse.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.flpTestCourse.AutoScroll = True
        Me.flpTestCourse.BackColor = System.Drawing.Color.Transparent
        Me.flpTestCourse.Location = New System.Drawing.Point(115, 232)
        Me.flpTestCourse.Margin = New System.Windows.Forms.Padding(10)
        Me.flpTestCourse.Name = "flpTestCourse"
        Me.flpTestCourse.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.flpTestCourse.Size = New System.Drawing.Size(297, 472)
        Me.flpTestCourse.TabIndex = 21
        '
        'pnlStatistict
        '
        Me.pnlStatistict.Controls.Add(Me.Label13)
        Me.pnlStatistict.Controls.Add(Me.lblTestPassTotal)
        Me.pnlStatistict.Controls.Add(Me.Label15)
        Me.pnlStatistict.Controls.Add(Me.Label16)
        Me.pnlStatistict.Controls.Add(Me.lblTestedComplete)
        Me.pnlStatistict.Controls.Add(Me.Label18)
        Me.pnlStatistict.Controls.Add(Me.Label7)
        Me.pnlStatistict.Controls.Add(Me.lblTestTotal)
        Me.pnlStatistict.Controls.Add(Me.Label9)
        Me.pnlStatistict.Controls.Add(Me.Label10)
        Me.pnlStatistict.Controls.Add(Me.lblTestedAttempt)
        Me.pnlStatistict.Controls.Add(Me.Label12)
        Me.pnlStatistict.Controls.Add(Me.Label3)
        Me.pnlStatistict.Controls.Add(Me.lblCourseTotal)
        Me.pnlStatistict.Controls.Add(Me.Label6)
        Me.pnlStatistict.Controls.Add(Me.Label4)
        Me.pnlStatistict.Controls.Add(Me.lblCourseComplete)
        Me.pnlStatistict.Controls.Add(Me.Label1)
        Me.pnlStatistict.Location = New System.Drawing.Point(594, 243)
        Me.pnlStatistict.Name = "pnlStatistict"
        Me.pnlStatistict.Size = New System.Drawing.Size(438, 270)
        Me.pnlStatistict.TabIndex = 32
        '
        'Label13
        '
        Me.Label13.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label13.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.Label13.Font = New System.Drawing.Font("Kittithada Roman 55 F", 16.0!)
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(130, 219)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(83, 32)
        Me.Label13.TabIndex = 50
        Me.Label13.Text = "บททดสอบ"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTestPassTotal
        '
        Me.lblTestPassTotal.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblTestPassTotal.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblTestPassTotal.Font = New System.Drawing.Font("Kittithada Roman 55 F", 16.0!, System.Drawing.FontStyle.Bold)
        Me.lblTestPassTotal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(219, Byte), Integer))
        Me.lblTestPassTotal.Location = New System.Drawing.Point(93, 219)
        Me.lblTestPassTotal.Name = "lblTestPassTotal"
        Me.lblTestPassTotal.Size = New System.Drawing.Size(37, 32)
        Me.lblTestPassTotal.TabIndex = 49
        Me.lblTestPassTotal.Text = "5"
        Me.lblTestPassTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label15
        '
        Me.Label15.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label15.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.Label15.Font = New System.Drawing.Font("Kittithada Roman 55 F", 16.0!)
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(17, 219)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(82, 32)
        Me.Label15.TabIndex = 48
        Me.Label15.Text = "จากทั้งหมด"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label16
        '
        Me.Label16.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label16.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.Label16.Font = New System.Drawing.Font("Kittithada Roman 55 F", 20.0!)
        Me.Label16.ForeColor = System.Drawing.Color.White
        Me.Label16.Location = New System.Drawing.Point(326, 188)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(97, 32)
        Me.Label16.TabIndex = 47
        Me.Label16.Text = "บททดสอบ"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTestedComplete
        '
        Me.lblTestedComplete.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblTestedComplete.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblTestedComplete.Font = New System.Drawing.Font("Kittithada Roman 55 F", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTestedComplete.ForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(219, Byte), Integer))
        Me.lblTestedComplete.Location = New System.Drawing.Point(284, 188)
        Me.lblTestedComplete.Name = "lblTestedComplete"
        Me.lblTestedComplete.Size = New System.Drawing.Size(37, 32)
        Me.lblTestedComplete.TabIndex = 46
        Me.lblTestedComplete.Text = "5"
        Me.lblTestedComplete.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label18
        '
        Me.Label18.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label18.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.Label18.Font = New System.Drawing.Font("Kittithada Roman 55 F", 20.0!)
        Me.Label18.ForeColor = System.Drawing.Color.White
        Me.Label18.Location = New System.Drawing.Point(17, 188)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(272, 32)
        Me.Label18.TabIndex = 45
        Me.Label18.Text = "คุณได้ทำบททดสอบผ่านแล้วทั้งหมด"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label7.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.Label7.Font = New System.Drawing.Font("Kittithada Roman 55 F", 16.0!)
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(131, 128)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(83, 32)
        Me.Label7.TabIndex = 44
        Me.Label7.Text = "บททดสอบ"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTestTotal
        '
        Me.lblTestTotal.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblTestTotal.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblTestTotal.Font = New System.Drawing.Font("Kittithada Roman 55 F", 16.0!, System.Drawing.FontStyle.Bold)
        Me.lblTestTotal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(219, Byte), Integer))
        Me.lblTestTotal.Location = New System.Drawing.Point(94, 128)
        Me.lblTestTotal.Name = "lblTestTotal"
        Me.lblTestTotal.Size = New System.Drawing.Size(37, 32)
        Me.lblTestTotal.TabIndex = 43
        Me.lblTestTotal.Text = "5"
        Me.lblTestTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label9
        '
        Me.Label9.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label9.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.Label9.Font = New System.Drawing.Font("Kittithada Roman 55 F", 16.0!)
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(18, 128)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(82, 32)
        Me.Label9.TabIndex = 42
        Me.Label9.Text = "จากทั้งหมด"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label10.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.Label10.Font = New System.Drawing.Font("Kittithada Roman 55 F", 20.0!)
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(296, 97)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(97, 32)
        Me.Label10.TabIndex = 41
        Me.Label10.Text = "บททดสอบ"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTestedAttempt
        '
        Me.lblTestedAttempt.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblTestedAttempt.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblTestedAttempt.Font = New System.Drawing.Font("Kittithada Roman 55 F", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTestedAttempt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(219, Byte), Integer))
        Me.lblTestedAttempt.Location = New System.Drawing.Point(254, 97)
        Me.lblTestedAttempt.Name = "lblTestedAttempt"
        Me.lblTestedAttempt.Size = New System.Drawing.Size(37, 32)
        Me.lblTestedAttempt.TabIndex = 40
        Me.lblTestedAttempt.Text = "5"
        Me.lblTestedAttempt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label12
        '
        Me.Label12.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label12.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.Label12.Font = New System.Drawing.Font("Kittithada Roman 55 F", 20.0!)
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(18, 97)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(243, 32)
        Me.Label12.TabIndex = 39
        Me.Label12.Text = "คุณได้ทำบททดสอบแล้วทั้งหมด"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label3.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.Label3.Font = New System.Drawing.Font("Kittithada Roman 55 F", 16.0!)
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(131, 42)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(83, 32)
        Me.Label3.TabIndex = 38
        Me.Label3.Text = "หลักสูตร"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCourseTotal
        '
        Me.lblCourseTotal.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblCourseTotal.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblCourseTotal.Font = New System.Drawing.Font("Kittithada Roman 55 F", 16.0!, System.Drawing.FontStyle.Bold)
        Me.lblCourseTotal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(219, Byte), Integer))
        Me.lblCourseTotal.Location = New System.Drawing.Point(94, 42)
        Me.lblCourseTotal.Name = "lblCourseTotal"
        Me.lblCourseTotal.Size = New System.Drawing.Size(37, 32)
        Me.lblCourseTotal.TabIndex = 37
        Me.lblCourseTotal.Text = "5"
        Me.lblCourseTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label6
        '
        Me.Label6.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label6.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.Label6.Font = New System.Drawing.Font("Kittithada Roman 55 F", 16.0!)
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(18, 42)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(82, 32)
        Me.Label6.TabIndex = 36
        Me.Label6.Text = "จากทั้งหมด"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label4.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.Label4.Font = New System.Drawing.Font("Kittithada Roman 55 F", 20.0!)
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(296, 11)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(83, 32)
        Me.Label4.TabIndex = 35
        Me.Label4.Text = "หลักสูตร"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCourseComplete
        '
        Me.lblCourseComplete.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblCourseComplete.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblCourseComplete.Font = New System.Drawing.Font("Kittithada Roman 55 F", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCourseComplete.ForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(219, Byte), Integer))
        Me.lblCourseComplete.Location = New System.Drawing.Point(261, 11)
        Me.lblCourseComplete.Name = "lblCourseComplete"
        Me.lblCourseComplete.Size = New System.Drawing.Size(37, 32)
        Me.lblCourseComplete.TabIndex = 34
        Me.lblCourseComplete.Text = "5"
        Me.lblCourseComplete.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.Label1.Font = New System.Drawing.Font("Kittithada Roman 55 F", 20.0!)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(18, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(257, 32)
        Me.Label1.TabIndex = 33
        Me.Label1.Text = "คุณเรียนจบหลักสูตรแล้วทั้งหมด"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmSelectTestCourse
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(1036, 768)
        Me.ControlBox = False
        Me.Controls.Add(Me.pnlStatistict)
        Me.Controls.Add(Me.flpTestCourse)
        Me.Controls.Add(Me.lblWelcome4)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.Label2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmSelectTestCourse"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlStatistict.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblWelcome4 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents flpTestCourse As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents pnlStatistict As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblCourseTotal As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblCourseComplete As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lblTestTotal As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lblTestedAttempt As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lblTestPassTotal As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents lblTestedComplete As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
End Class
