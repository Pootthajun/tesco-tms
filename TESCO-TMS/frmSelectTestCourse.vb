﻿Imports System.Data
Imports System.Data.OleDb
Public Class frmSelectTestCourse

    Private Sub frmSelectFormat_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        SetTestingCourse()
        SetStatistict()


        Me.WindowState = FormWindowState.Maximized
        Me.ControlBox = False

        AdjudeObject()
    End Sub

    Private Sub AdjudeObject()
        PictureBox1.Left = (Me.Width / 2) - (PictureBox1.Width / 2)   'เสา
        PictureBox2.Left = (Me.Width / 2) - (PictureBox2.Width / 2)   'คุณกำลังเข้าสู่ แบบทดสอบ

        Label2.Left = PictureBox1.Left - (Label2.Width + 140)
        flpTestCourse.Left = PictureBox1.Left - (flpTestCourse.Width + 100)

        lblWelcome4.Left = PictureBox1.Left + PictureBox1.Width + 92
        pnlStatistict.Left = PictureBox1.Left + PictureBox1.Width + 70
    End Sub

    Private Sub SetTestingCourse()
        If DT_TEST.Rows.Count > 0 Then
            For Each dr As DataRow In DT_TEST.Rows
                Dim lbl As New Label
                lbl.Text = dr("title")
                lbl.Tag = dr("id")
                lbl.Size = New Size(250, 55)
                lbl.Font = New Font("Kittithada Medium 65 F", 16, FontStyle.Bold, GraphicsUnit.Point)
                lbl.ForeColor = Color.White
                lbl.BackColor = Color.FromArgb(149, 149, 149)
                lbl.TextAlign = ContentAlignment.MiddleCenter
                lbl.Cursor = Cursors.Hand
                lbl.Margin = New Padding(5)

                'AddHandler lbl.MouseHover, AddressOf btnTestingCourse_MouseHover
                AddHandler lbl.MouseMove, AddressOf btnTestingCourse_MouseMove
                AddHandler lbl.MouseLeave, AddressOf btnTestingCourse_MouseLeave
                AddHandler lbl.Click, AddressOf btnTestingCourse_Click

                flpTestCourse.Controls.Add(lbl)
            Next
        End If
    End Sub

    Private Sub btnTestingCourse_Click(sender As System.Object, e As System.EventArgs)
        frmMain.CloseAllChildForm(Me)

        Dim f As New frmSelectTestDetailDialog
        f.TestingID = DirectCast(sender, Label).Tag
        Plexiglass(f, Me.ParentForm)
    End Sub

    Private Sub btnTestingCourse_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs)
        DirectCast(sender, Label).BackColor = Color.FromArgb(16, 77, 86)
        Application.DoEvents()
    End Sub
    Private Sub btnTestingCourse_MouseLeave(sender As Object, e As System.EventArgs)
        DirectCast(sender, Label).BackColor = Color.FromArgb(149, 149, 149)
        Application.DoEvents()
    End Sub


    Private Sub SetStatistict()
        Try
            lblCourseComplete.Text = myUser.CourseComplete
            lblCourseTotal.Text = myUser.CourseTotal

            lblTestedAttempt.Text = myUser.TestingAttempt
            lblTestedComplete.Text = myUser.TestingComplete
            lblTestTotal.Text = myUser.TestingTotal
            lblTestPassTotal.Text = myUser.TestingTotal
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Label1_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Label1.MouseMove

    End Sub
End Class