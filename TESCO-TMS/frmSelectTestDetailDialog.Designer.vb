﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSelectTestDetailDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSelectTestDetailDialog))
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblPassPercent = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblQuestionQty = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblDesc = New System.Windows.Forms.WebBrowser()
        Me.btnStartTest = New System.Windows.Forms.Label()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.btnClose = New System.Windows.Forms.PictureBox()
        Me.Panel2.SuspendLayout()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.Color.White
        Me.Panel2.Controls.Add(Me.lblPassPercent)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.lblQuestionQty)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.lblDesc)
        Me.Panel2.Controls.Add(Me.btnStartTest)
        Me.Panel2.Location = New System.Drawing.Point(12, 75)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(532, 253)
        Me.Panel2.TabIndex = 3
        '
        'lblPassPercent
        '
        Me.lblPassPercent.AutoSize = True
        Me.lblPassPercent.Font = New System.Drawing.Font("Kittithada Medium 65 P", 20.0!)
        Me.lblPassPercent.ForeColor = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(121, Byte), Integer))
        Me.lblPassPercent.Location = New System.Drawing.Point(415, 149)
        Me.lblPassPercent.Name = "lblPassPercent"
        Me.lblPassPercent.Size = New System.Drawing.Size(53, 34)
        Me.lblPassPercent.TabIndex = 43
        Me.lblPassPercent.Text = "50%"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Kittithada Medium 65 P", 15.75!)
        Me.Label5.Location = New System.Drawing.Point(329, 154)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(86, 27)
        Me.Label5.TabIndex = 42
        Me.Label5.Text = "เกณฑ์คะแนน"
        '
        'lblQuestionQty
        '
        Me.lblQuestionQty.AutoSize = True
        Me.lblQuestionQty.Font = New System.Drawing.Font("Kittithada Medium 65 P", 20.0!)
        Me.lblQuestionQty.ForeColor = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(121, Byte), Integer))
        Me.lblQuestionQty.Location = New System.Drawing.Point(123, 149)
        Me.lblQuestionQty.Name = "lblQuestionQty"
        Me.lblQuestionQty.Size = New System.Drawing.Size(35, 34)
        Me.lblQuestionQty.TabIndex = 41
        Me.lblQuestionQty.Text = "20"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Kittithada Medium 65 P", 15.75!)
        Me.Label2.Location = New System.Drawing.Point(160, 154)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(29, 27)
        Me.Label2.TabIndex = 40
        Me.Label2.Text = "ข้อ"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Kittithada Medium 65 P", 15.75!)
        Me.Label1.Location = New System.Drawing.Point(35, 154)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(91, 27)
        Me.Label1.TabIndex = 39
        Me.Label1.Text = "จำนวนคำถาม"
        '
        'lblDesc
        '
        Me.lblDesc.Location = New System.Drawing.Point(28, 16)
        Me.lblDesc.Name = "lblDesc"
        Me.lblDesc.Size = New System.Drawing.Size(476, 132)
        Me.lblDesc.TabIndex = 38
        '
        'btnStartTest
        '
        Me.btnStartTest.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnStartTest.BackColor = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(121, Byte), Integer))
        Me.btnStartTest.Font = New System.Drawing.Font("Kittithada Medium 65 P", 26.0!)
        Me.btnStartTest.ForeColor = System.Drawing.Color.White
        Me.btnStartTest.Location = New System.Drawing.Point(190, 188)
        Me.btnStartTest.Name = "btnStartTest"
        Me.btnStartTest.Size = New System.Drawing.Size(135, 43)
        Me.btnStartTest.TabIndex = 36
        Me.btnStartTest.Text = "เริ่มทดสอบ"
        Me.btnStartTest.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblTitle
        '
        Me.lblTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTitle.BackColor = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(121, Byte), Integer))
        Me.lblTitle.Font = New System.Drawing.Font("Kittithada Medium 65 P", 26.0!)
        Me.lblTitle.ForeColor = System.Drawing.Color.White
        Me.lblTitle.Location = New System.Drawing.Point(12, 12)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(532, 63)
        Me.lblTitle.TabIndex = 24
        Me.lblTitle.Text = "เครื่องแคชเชียร์"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.Transparent
        Me.btnClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.Location = New System.Drawing.Point(519, 2)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(35, 33)
        Me.btnClose.TabIndex = 37
        Me.btnClose.TabStop = False
        '
        'frmSelectTestDetailDialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(556, 340)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.lblTitle)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmSelectTestDetailDialog"
        Me.ShowInTaskbar = False
        Me.TransparencyKey = System.Drawing.Color.FromArgb(CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents btnStartTest As System.Windows.Forms.Label
    Friend WithEvents btnClose As System.Windows.Forms.PictureBox
    Friend WithEvents lblDesc As System.Windows.Forms.WebBrowser
    Friend WithEvents lblPassPercent As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblQuestionQty As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
