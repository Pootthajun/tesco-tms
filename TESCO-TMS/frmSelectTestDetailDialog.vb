﻿Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.Data

Public Class frmSelectTestDetailDialog

    Public TestingID As Int32
    'Dim DT_USER As New DataTable

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs)
        Me.Close()
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub



    Private Sub frmSelectTestDetailDialog_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If DT_TEST.Rows.Count > 0 Then
            DT_TEST.DefaultView.RowFilter = "id=" & TestingID
            lblTitle.Text = DT_TEST.DefaultView(0)("title")
            lblDesc.DocumentText = DT_TEST.DefaultView(0)("description")
            lblQuestionQty.Text = DT_TEST.DefaultView(0)("question_qty")
            lblPassPercent.Text = DT_TEST.DefaultView(0)("target_percentage") & " %"

            DT_TEST.DefaultView.RowFilter = ""
        End If
    End Sub

    Private Sub btnStartTest_Click(sender As System.Object, e As System.EventArgs) Handles btnStartTest.Click
        Dim f As New frmQuestion
        f.MdiParent = frmMain
        f.TestID = TestingID
        f.QuestionQty = Convert.ToInt16(lblQuestionQty.Text.Trim)
        f.Show()

        frmMain.CloseAllChildForm(f)

    End Sub

    Private Sub btnClose_Click_1(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class