﻿Imports TESCO_TMS.Org.Mentalis.Files

Public Class frmSetProxy


    Dim ini As New IniReader(INIFile)

    Private Sub btnApply_Click(sender As System.Object, e As System.EventArgs) Handles btnApply.Click

        ini.Section = "PROXY"
        ini.Write("Address", txtAddress.Text)
        ini.Write("Port", txtPort.Text)
        ini.Write("UserName", txtUserName.Text)
        ini.Write("Password", txtPassword.Text)
        'ini.Write("Bypass", chkBypass.Checked.ToString)

        MessageBox.Show("Save Success", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Me.Close()
    End Sub

    Private Sub frmSetProxy_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        ini.Section = "PROXY"

        Dim Address As String = ini.ReadString("Address")
        Dim Port As String = ini.ReadString("Port")
        Dim UserName As String = ini.ReadString("UserName")
        Dim Password As String = ini.ReadString("Password")
        'Dim Bypass As String = ini.ReadString("Bypass")

        txtAddress.Text = Address
        txtPort.Text = Port
        txtUserName.Text = UserName
        txtPassword.Text = Password
        'Try
        '    chkBypass.Checked = CBool(Bypass)
        'Catch ex As Exception
        '    chkBypass.Checked = False
        '    ini.Write("Bypass", "False")
        'End Try
    End Sub
End Class