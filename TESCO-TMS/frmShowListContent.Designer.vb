﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmShowListContent
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pbClose = New System.Windows.Forms.PictureBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblCourseTitle = New System.Windows.Forms.Label()
        Me.flp = New System.Windows.Forms.FlowLayoutPanel()
        CType(Me.pbClose, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pbClose
        '
        Me.pbClose.Image = Global.TESCO_TMS.My.Resources.Resources.btnClose
        Me.pbClose.Location = New System.Drawing.Point(240, 0)
        Me.pbClose.Name = "pbClose"
        Me.pbClose.Size = New System.Drawing.Size(42, 37)
        Me.pbClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pbClose.TabIndex = 0
        Me.pbClose.TabStop = False
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(1, 2)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 24)
        Me.Label2.TabIndex = 27
        Me.Label2.Text = "หลักสูตร"
        '
        'lblCourseTitle
        '
        Me.lblCourseTitle.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblCourseTitle.BackColor = System.Drawing.Color.White
        Me.lblCourseTitle.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCourseTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(119, Byte), Integer))
        Me.lblCourseTitle.Location = New System.Drawing.Point(1, 45)
        Me.lblCourseTitle.Name = "lblCourseTitle"
        Me.lblCourseTitle.Size = New System.Drawing.Size(281, 59)
        Me.lblCourseTitle.TabIndex = 28
        Me.lblCourseTitle.Text = "LIVE TRAINING บทเรียนพิเศษ"
        '
        'flp
        '
        Me.flp.AutoScroll = True
        Me.flp.Location = New System.Drawing.Point(0, 108)
        Me.flp.Name = "flp"
        Me.flp.Size = New System.Drawing.Size(282, 645)
        Me.flp.TabIndex = 29
        '
        'frmShowListContent
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(284, 754)
        Me.ControlBox = False
        Me.Controls.Add(Me.flp)
        Me.Controls.Add(Me.lblCourseTitle)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.pbClose)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmShowListContent"
        Me.ShowInTaskbar = False
        Me.Text = "frmDisplayContent"
        Me.TopMost = True
        CType(Me.pbClose, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pbClose As System.Windows.Forms.PictureBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblCourseTitle As System.Windows.Forms.Label
    Friend WithEvents flp As System.Windows.Forms.FlowLayoutPanel
End Class
