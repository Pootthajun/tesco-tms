﻿Imports System.Data
Public Class frmShowListContent

    Public CourseID As Integer = 0

    Private Sub frmShowListContent_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Me.Top = 0
        Me.Left = 0
        Me.Height = Screen.PrimaryScreen.Bounds.Height

        ShowDocumentList()
    End Sub


    Private Sub ShowDocumentList()
        'หาชื่อเอกสารทั้งหมดที่มีในบทเรียน
        Dim cDt As DataTable = GetCourseDocumentList()
        If cDt.Rows.Count > 0 Then
            lblCourseTitle.Text = cDt.Rows(0)("course_title").ToString

            Dim i As Integer = 1
            For Each cDr As DataRow In cDt.Rows
                Dim IsDocLock As Boolean = False
                If Convert.IsDBNull(cDr("is_document_lock")) = False Then
                    IsDocLock = IIf(cDr("is_document_lock").ToString = "Y", True, False)
                End If

                If IsDocLock = True Then
                    Dim Dt As DataTable = CallAPIGetLog(Token, myUser.ClientID, cDr("tb_course_id"))
                    If Dt.Rows.Count > 0 Then
                        For Each dr As DataRow In Dt.Rows
                            If Convert.IsDBNull(dr("document_finished_list")) = False Then
                                'ถ้ามี List ของเอกสารที่เรียนจบแล้ว
                                For Each DocListID As String In Split(dr("document_finished_list"), ",")
                                    If DocListID <> "" Then
                                        If DocListID = cDr("id") Then
                                            IsDocLock = False 'IIf(dr("is_document_lock").ToString.ToLower = "true", True, False)
                                            Exit For
                                        End If
                                    End If
                                Next
                            End If
                            If IsDocLock = False Then
                                Exit For
                            End If
                        Next

                        Dim Form As New ContentIndexList
                        Form.Display(IsDocLock, cDr("id"), cDr("title").ToString, i)
                        AddHandler Form.ChangeDocument, AddressOf Form_ChangeDocument
                        flp.Controls.Add(Form)
                    End If
                    Dt.Dispose()
                Else
                    Dim Form As New ContentIndexList
                    Form.Display(IsDocLock, cDr("id"), cDr("title").ToString, i)
                    AddHandler Form.ChangeDocument, AddressOf Form_ChangeDocument
                    flp.Controls.Add(Form)
                End If

                i += 1
            Next
        Else
            MessageBox.Show("Document not found", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'Me.Close()
        End If
        cDt.Dispose()
    End Sub

    Private Sub Form_ChangeDocument()
        

        Me.Owner.Close()
        Me.Close()
        Threading.Thread.Sleep(5000)

    End Sub

    Private Function GetCourseDocumentList() As DataTable
        Dim sql As String = "select cd.id, cd.title, c.title as course_title, c.is_document_lock, cd.tb_course_id"
        sql += " from tb_course_document cd "
        sql += " inner join tb_course c on c.id=cd.tb_course_id "
        sql += " where cd.tb_course_id=" & CourseID
        sql += " order by cd.sort"
        Dim dt As DataTable
        Try
            dt = GetSqlDataTable(sql)
        Catch ex As Exception
            dt = New DataTable
        End Try
        Return dt
    End Function



    Private Sub pbClose_Click(sender As System.Object, e As System.EventArgs) Handles pbClose.Click
        Me.Close()
    End Sub
End Class