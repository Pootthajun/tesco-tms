﻿Public Class frmWait

    Private Sub frmWait_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        Me.WindowState = FormWindowState.Maximized
        Me.ControlBox = False

        Label1.Top = (Me.Height / 2) - (Label1.Height / 2)
        Label1.Left = (Me.Width / 2) - (Label1.Width / 2)
        Application.DoEvents()
    End Sub

    Private Sub pbClose_Click(sender As Object, e As System.EventArgs) Handles pbClose.Click
        Me.Close()
    End Sub
End Class