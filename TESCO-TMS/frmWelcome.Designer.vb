﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmWelcome
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmWelcome))
        Me.lblWelcome1 = New System.Windows.Forms.Label()
        Me.lblVersion = New System.Windows.Forms.Label()
        Me.lblWelcome02 = New System.Windows.Forms.Label()
        Me.lblShowSelectFormat = New System.Windows.Forms.Label()
        Me.pnlLogin = New System.Windows.Forms.Panel()
        Me.lblWait = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.txtUser = New System.Windows.Forms.TextBox()
        Me.pbLoginLogo = New System.Windows.Forms.PictureBox()
        Me.pbTextPassword = New System.Windows.Forms.PictureBox()
        Me.pbTextUsername = New System.Windows.Forms.PictureBox()
        Me.lblLogin = New System.Windows.Forms.Label()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.pbClose = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.btnExit = New System.Windows.Forms.PictureBox()
        Me.pnlLogin.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.pbLoginLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbTextPassword, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbTextUsername, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbClose, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnExit, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblWelcome1
        '
        Me.lblWelcome1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWelcome1.Font = New System.Drawing.Font("Tahoma", 34.0!)
        Me.lblWelcome1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(22, Byte), Integer), CType(CType(137, Byte), Integer))
        Me.lblWelcome1.Location = New System.Drawing.Point(0, 88)
        Me.lblWelcome1.Name = "lblWelcome1"
        Me.lblWelcome1.Size = New System.Drawing.Size(1690, 71)
        Me.lblWelcome1.TabIndex = 13
        Me.lblWelcome1.Text = "ยินดีต้อนรับเข้าสู่"
        Me.lblWelcome1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblVersion
        '
        Me.lblVersion.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblVersion.AutoSize = True
        Me.lblVersion.Location = New System.Drawing.Point(1628, 742)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(41, 13)
        Me.lblVersion.TabIndex = 23
        Me.lblVersion.Text = "V 0.0.0"
        '
        'lblWelcome02
        '
        Me.lblWelcome02.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWelcome02.Font = New System.Drawing.Font("Tahoma", 34.0!)
        Me.lblWelcome02.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(22, Byte), Integer), CType(CType(137, Byte), Integer))
        Me.lblWelcome02.Location = New System.Drawing.Point(0, 157)
        Me.lblWelcome02.Name = "lblWelcome02"
        Me.lblWelcome02.Size = New System.Drawing.Size(1690, 71)
        Me.lblWelcome02.TabIndex = 13
        Me.lblWelcome02.Text = "ศูนย์การเรียนรู้ ออนไลน์ เทสโก้โลตัส"
        Me.lblWelcome02.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblShowSelectFormat
        '
        Me.lblShowSelectFormat.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblShowSelectFormat.Font = New System.Drawing.Font("Tahoma", 34.0!)
        Me.lblShowSelectFormat.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(22, Byte), Integer), CType(CType(137, Byte), Integer))
        Me.lblShowSelectFormat.Location = New System.Drawing.Point(0, 220)
        Me.lblShowSelectFormat.Name = "lblShowSelectFormat"
        Me.lblShowSelectFormat.Size = New System.Drawing.Size(1690, 71)
        Me.lblShowSelectFormat.TabIndex = 13
        Me.lblShowSelectFormat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlLogin
        '
        Me.pnlLogin.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.pnlLogin.Controls.Add(Me.lblWait)
        Me.pnlLogin.Controls.Add(Me.Panel1)
        Me.pnlLogin.Controls.Add(Me.pbClose)
        Me.pnlLogin.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlLogin.Location = New System.Drawing.Point(0, 0)
        Me.pnlLogin.Name = "pnlLogin"
        Me.pnlLogin.Size = New System.Drawing.Size(1366, 768)
        Me.pnlLogin.TabIndex = 21
        '
        'lblWait
        '
        Me.lblWait.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblWait.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblWait.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblWait.ForeColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(119, Byte), Integer))
        Me.lblWait.Location = New System.Drawing.Point(1184, 677)
        Me.lblWait.Name = "lblWait"
        Me.lblWait.Size = New System.Drawing.Size(1267, 423)
        Me.lblWait.TabIndex = 43
        Me.lblWait.Text = "Please Wait..."
        Me.lblWait.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblWait.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.Controls.Add(Me.txtPassword)
        Me.Panel1.Controls.Add(Me.txtUser)
        Me.Panel1.Controls.Add(Me.pbLoginLogo)
        Me.Panel1.Controls.Add(Me.pbTextPassword)
        Me.Panel1.Controls.Add(Me.pbTextUsername)
        Me.Panel1.Controls.Add(Me.lblLogin)
        Me.Panel1.Controls.Add(Me.PictureBox3)
        Me.Panel1.Location = New System.Drawing.Point(63, 220)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1230, 308)
        Me.Panel1.TabIndex = 52
        '
        'txtPassword
        '
        Me.txtPassword.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPassword.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPassword.Font = New System.Drawing.Font("Kittithada Medium 65 F", 24.0!)
        Me.txtPassword.ForeColor = System.Drawing.Color.White
        Me.txtPassword.Location = New System.Drawing.Point(756, 128)
        Me.txtPassword.MaxLength = 20
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(274, 39)
        Me.txtPassword.TabIndex = 29
        Me.txtPassword.Text = "PASSWORD"
        '
        'txtUser
        '
        Me.txtUser.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtUser.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtUser.Font = New System.Drawing.Font("Kittithada Medium 65 F", 24.0!)
        Me.txtUser.ForeColor = System.Drawing.Color.White
        Me.txtUser.Location = New System.Drawing.Point(753, 58)
        Me.txtUser.MaxLength = 20
        Me.txtUser.Name = "txtUser"
        Me.txtUser.Size = New System.Drawing.Size(277, 39)
        Me.txtUser.TabIndex = 28
        Me.txtUser.Text = "USER LOGIN"
        '
        'pbLoginLogo
        '
        Me.pbLoginLogo.BackColor = System.Drawing.Color.Transparent
        Me.pbLoginLogo.Cursor = System.Windows.Forms.Cursors.Default
        Me.pbLoginLogo.Image = Global.TESCO_TMS.My.Resources.Resources.Login_Logo
        Me.pbLoginLogo.Location = New System.Drawing.Point(200, 47)
        Me.pbLoginLogo.Name = "pbLoginLogo"
        Me.pbLoginLogo.Size = New System.Drawing.Size(330, 213)
        Me.pbLoginLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbLoginLogo.TabIndex = 33
        Me.pbLoginLogo.TabStop = False
        '
        'pbTextPassword
        '
        Me.pbTextPassword.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pbTextPassword.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pbTextPassword.Cursor = System.Windows.Forms.Cursors.Hand
        Me.pbTextPassword.Location = New System.Drawing.Point(723, 118)
        Me.pbTextPassword.Name = "pbTextPassword"
        Me.pbTextPassword.Size = New System.Drawing.Size(319, 59)
        Me.pbTextPassword.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbTextPassword.TabIndex = 36
        Me.pbTextPassword.TabStop = False
        '
        'pbTextUsername
        '
        Me.pbTextUsername.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pbTextUsername.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pbTextUsername.Cursor = System.Windows.Forms.Cursors.Hand
        Me.pbTextUsername.Location = New System.Drawing.Point(723, 47)
        Me.pbTextUsername.Name = "pbTextUsername"
        Me.pbTextUsername.Size = New System.Drawing.Size(319, 60)
        Me.pbTextUsername.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbTextUsername.TabIndex = 34
        Me.pbTextUsername.TabStop = False
        '
        'lblLogin
        '
        Me.lblLogin.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblLogin.BackColor = System.Drawing.Color.FromArgb(CType(CType(2, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(121, Byte), Integer))
        Me.lblLogin.Font = New System.Drawing.Font("Kittithada Medium 65 F", 36.0!, System.Drawing.FontStyle.Bold)
        Me.lblLogin.ForeColor = System.Drawing.Color.White
        Me.lblLogin.Location = New System.Drawing.Point(723, 195)
        Me.lblLogin.Name = "lblLogin"
        Me.lblLogin.Size = New System.Drawing.Size(319, 68)
        Me.lblLogin.TabIndex = 49
        Me.lblLogin.Text = "เข้าสู่ระบบ"
        Me.lblLogin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox3
        '
        Me.PictureBox3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.PictureBox3.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox3.Image = Global.TESCO_TMS.My.Resources.Resources.Login_Line
        Me.PictureBox3.Location = New System.Drawing.Point(615, 19)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(13, 266)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 45
        Me.PictureBox3.TabStop = False
        '
        'pbClose
        '
        Me.pbClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pbClose.Image = Global.TESCO_TMS.My.Resources.Resources.btnClose
        Me.pbClose.Location = New System.Drawing.Point(1336, 0)
        Me.pbClose.Name = "pbClose"
        Me.pbClose.Size = New System.Drawing.Size(30, 30)
        Me.pbClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbClose.TabIndex = 51
        Me.pbClose.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox1.Image = Global.TESCO_TMS.My.Resources.Resources.mascot
        Me.PictureBox1.Location = New System.Drawing.Point(198, 395)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(219, 361)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnExit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnExit.Image = Global.TESCO_TMS.My.Resources.Resources.btnExit1
        Me.btnExit.Location = New System.Drawing.Point(1552, 98)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(117, 46)
        Me.btnExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnExit.TabIndex = 14
        Me.btnExit.TabStop = False
        '
        'frmWelcome
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1366, 768)
        Me.ControlBox = False
        Me.Controls.Add(Me.pnlLogin)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.lblVersion)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.lblShowSelectFormat)
        Me.Controls.Add(Me.lblWelcome02)
        Me.Controls.Add(Me.lblWelcome1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmWelcome"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.pnlLogin.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.pbLoginLogo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbTextPassword, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbTextUsername, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbClose, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnExit, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblWelcome1 As System.Windows.Forms.Label
    Friend WithEvents btnExit As System.Windows.Forms.PictureBox
    Friend WithEvents lblVersion As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblWelcome02 As System.Windows.Forms.Label
    Friend WithEvents lblShowSelectFormat As System.Windows.Forms.Label
    Friend WithEvents pnlLogin As System.Windows.Forms.Panel
    Friend WithEvents lblWait As System.Windows.Forms.Label
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents txtUser As System.Windows.Forms.TextBox
    Friend WithEvents pbTextUsername As System.Windows.Forms.PictureBox
    Friend WithEvents pbLoginLogo As System.Windows.Forms.PictureBox
    Friend WithEvents pbTextPassword As System.Windows.Forms.PictureBox
    Friend WithEvents lblLogin As System.Windows.Forms.Label
    Friend WithEvents pbClose As System.Windows.Forms.PictureBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    'Friend WithEvents CustomScrollbar1 As CustomControls.CustomScrollbar
End Class
