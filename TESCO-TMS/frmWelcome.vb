﻿Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports TESCO_TMS.Org.Mentalis.Files

Public Class frmWelcome

    Dim buttonClick As Boolean = False

    Private Sub frmWelcome_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If buttonClick = False Then
            e.Cancel = True
            Exit Sub
        End If
    End Sub

    Private Sub frmWelcome_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        AdjudeObject()
    End Sub

    Private Sub AdjudeObject()
        PictureBox3.Left = (Panel1.Width / 2) - (PictureBox3.Width / 2)
        pbLoginLogo.Left = PictureBox3.Left - (pbLoginLogo.Width + 85)

        pbTextUsername.Left = PictureBox3.Left + PictureBox3.Width + 85
        txtUser.Left = pbTextUsername.Left + 30

        pbTextPassword.Left = pbTextUsername.Left
        txtPassword.Left = txtUser.Left

        lblLogin.Left = pbTextUsername.Left
    End Sub

    Private Sub frmWelcome_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        Me.WindowState = FormWindowState.Maximized
        Me.ControlBox = False

        lblVersion.Text = "V " & getMyVersion()
        'TempPath
        If (Not Directory.Exists(TempPath)) Then
            Try
                Directory.CreateDirectory(TempPath)
            Catch ex As Exception
                MessageBox.Show("Could not fine Drive " & GetTempPath(), "Attention", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                btnExit_Click(Nothing, Nothing)
                Exit Sub
            End Try

        End If

        If (Not File.Exists(TempPath & "/" & dbFile)) Then
            If File.Exists(Application.StartupPath & "\" & dbFile) Then
                File.Copy(Application.StartupPath & "\" & dbFile, TempPath & "\" & dbFile)
            End If
        End If

        'สำหรับกรณี Update เป็น Version 1.1.6
        Dim Usql As String = "select * from TB_COURSE"
        Dim Udt As DataTable = GetSqlDataTable(Usql)
        If Udt.Columns.Contains("is_document_lock") = False Then
            Usql = " alter table TB_COURSE add is_document_lock Text(1) "
            ExecuteSqlNoneQuery(Usql)
        End If
        If Udt.Columns.Contains("document_detail") = False Then
            Usql = " alter table TB_COURSE add document_detail Memo "
            ExecuteSqlNoneQuery(Usql)
        End If
        If Udt.Columns.Contains("bind_document") = False Then
            Usql = " alter table TB_COURSE add bind_document Text(1) "
            ExecuteSqlNoneQuery(Usql)
        End If
        Udt.Dispose()
        '################### V1.1.6 ##############

        'สำหรับกรณี Update เป็น Version 2.4.0
        If Udt.Columns.Contains("department_id") = False Then
            Usql = " alter table TB_COURSE add department_id Integer "
            ExecuteSqlNoneQuery(Usql)
        End If
        '################### V2.4.0 ##############

        'สำหรับกรณี Update เป็น Version 2.12.0
        Dim cdDT As DataTable = GetAccessSchema("TB_COURSE_DOCUMENT_FILE")
        cdDT.DefaultView.RowFilter = "column_name='file_url'"
        If cdDT.DefaultView.Count > 0 Then
            If cdDT.DefaultView(0)("column_flags") <> 234 Then
                Usql = " alter table TB_COURSE_DOCUMENT_FILE alter column title longtext "
                ExecuteSqlNoneQuery(Usql)

                Usql = " alter table TB_COURSE_DOCUMENT_FILE alter column file_url longtext "
                ExecuteSqlNoneQuery(Usql)
            End If
        End If
        cdDT.Dispose()
        '################### V2.12.0

        Dim sql As String = ""
        Dim dt As New DataTable
        Dim da As OleDbDataAdapter
        Try
            sql = "select Activate_code,client_id,client_token from [activate] where 1=1"
            dt = New DataTable
            da = New OleDbDataAdapter(sql, ConnStr)
            da.Fill(dt)
            If dt.Rows.Count = 0 Then
                Dim F As New frmActivation
                If F.ShowDialog(Me) = DialogResult.Cancel Then
                    btnExit_Click(Nothing, Nothing)
                End If
            Else
                If IsDBNull(dt.Rows(0)("client_token")) = True Then
                    Dim f As New frmActivation
                    If f.ShowDialog(Me) = DialogResult.Cancel Then
                        btnExit_Click(Nothing, Nothing)
                    End If
                Else
                    'Check for Activate with client_token  'V1.0.9
                    Dim ClientID As Integer = Convert.ToInt64(dt.Rows(0)("client_id"))
                    Dim ClientToken As String = dt.Rows(0)("client_token")
                    If TescoCheckActivateWithClientToken(ClientID, ClientToken) = True Then
                        myUser.ClientID = ClientID
                    Else
                        Dim f As New frmActivation
                        If f.ShowDialog(Me) = DialogResult.Cancel Then
                            btnExit_Click(Nothing, Nothing)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        buttonClick = True
        frmMain.buttonClick = True
        Application.Exit()
    End Sub


#Region "Login"
    Private Const wmUsername As String = "USER LOGIN"
    Private Const wmPassword As String = "PASSWORD"


    Private Sub txtUser_GotFocus(sender As Object, e As System.EventArgs) Handles txtUser.GotFocus
        If txtUser.Text = wmUsername Then
            txtUser.Text = ""
        End If
    End Sub

    Private Sub txtUser_LostFocus(sender As Object, e As System.EventArgs) Handles txtUser.LostFocus
        If txtUser.Text.Trim = "" Then
            txtUser.Text = wmUsername
        End If
    End Sub

    Private Sub txtUser_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtUser.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtPassword.Focus()
        End If
    End Sub

    Private Sub txtPassword_GotFocus(sender As Object, e As System.EventArgs) Handles txtPassword.GotFocus
        txtPassword.PasswordChar = "*"
        If txtPassword.Text.Trim = wmPassword Then
            txtPassword.Text = ""
        End If
    End Sub

    Private Sub txtPassword_LostFocus(sender As Object, e As System.EventArgs) Handles txtPassword.LostFocus
        If txtPassword.Text = "" Then
            txtPassword.Text = wmPassword
            txtPassword.PasswordChar = ""
        End If
    End Sub

    Private Sub txtPassword_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtPassword.KeyPress
        If Asc(e.KeyChar) = 13 Then
            btnLogin_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub btnLogin_Click(sender As System.Object, e As System.EventArgs) Handles lblLogin.Click
        lblWait.Location = New Point(0, 0)
        lblWait.Dock = DockStyle.Fill
        lblWait.Visible = True

        Application.DoEvents()
        If Login(txtUser.Text, txtPassword.Text) = True Then
            Dim v As System.Version = System.Reflection.Assembly.GetExecutingAssembly.GetName().Version
            Dim ClientVersion As String = v.Minor & "." & v.Build & "." & v.Revision
            SaveClientLogonInfo(myUser.UserID, myUser.UserID, myUser.ClientID, ClientVersion)

            frmMain.CloseAllChildForm()
            Dim f As New frmSelectFormat
            f.MdiParent = frmMain
            f.Show()
            buttonClick = True
            frmMain.lbl_H_Username.Text = myUser.Name
            Me.Close()
        Else
            lblWait.Visible = False
            MessageBox.Show("Invalid Username or Password", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

#End Region

    Private Sub pbClose_Click(sender As System.Object, e As System.EventArgs) Handles pbClose.Click
        End
    End Sub
End Class