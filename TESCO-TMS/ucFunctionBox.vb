﻿Public Class ucFunctionBox

    Public Event FunctionBox_Click(sender As ucFunctionBox, e As System.EventArgs)

    Public Sub Display(_ID As String, ByVal Title As String, ByVal iconURL As String, col As Color, formatName As String)
        lblID.Text = _ID
        lblTitle.Text = Title.ToUpper
        lblFormatName.Text = formatName
        If iconURL.Trim <> "" Then
            Dim Image As Image = GetImageFromURL(iconURL)
            If Not Image Is Nothing Then
                PictureBox1.Image = Image
                PictureBox1.SizeMode = PictureBoxSizeMode.StretchImage
            End If
        End If

        Me.BackColor = col
        Panel1.BackColor = col
    End Sub

    Public Sub DisplayImage(_ID As String, ByVal Title As String, ByVal iconImage As Image)
        lblID.Text = _ID
        lblTitle.Text = Title.ToUpper

        Dim x As Integer = 100 - ((Me.Width * 100) / 278)
        Dim h As Integer = 100 - ((Me.Height * 100) / 218)

        lblTitle.Location = New Point(x, 0)
        lblTitle.Size = New Size(Me.Width - x, Me.Height - h)
        lblTitle.TextAlign = ContentAlignment.MiddleCenter
        PictureBox1.Visible = False
        
        Me.BackgroundImage = iconImage
        Me.BackgroundImageLayout = ImageLayout.Stretch
    End Sub

    Private Sub ucFunctionBox_Click(sender As Object, e As System.EventArgs) Handles Me.Click, lblTitle.Click, Panel1.Click, PictureBox1.Click
        RaiseEvent FunctionBox_Click(Me, e)
    End Sub

End Class
