﻿Public Class ucStudyUser

    Public Event RemoveListUser(RowIndex As Integer)


    Sub Display(ByVal CompanyID As String, Name As String, ByVal Sername As String, RowIndex As Int16)
        lblCompanyID.Text = CompanyID
        lblName.Text = Name & " " & Sername
        lblRowIndex.Text = RowIndex
    End Sub


    Private Sub pbRemove_Click(sender As System.Object, e As System.EventArgs) Handles pbRemove.Click
        RaiseEvent RemoveListUser(lblRowIndex.Text)
    End Sub
End Class
